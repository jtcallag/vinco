export default class campaignInfluencerModel {

    constructor() {}

    uniqID;
    campaignUniqID;
    influencerUniqID;
    fullName;
    firstName;
    lastName;
    dateOfBirth;
    age;
    clickCount;
    campaignInfluencerClickUrl;
    isEnabled;

    campaignName;
    productName;
    productDescription;
    productUrl;
    startDate;
    endDate;
    campaignIsEnabled;
    influencerNameAlias;
    influencerDateOfBirth;

}