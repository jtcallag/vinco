export default class advertiserRegistrationModel {

    constructor() {}

    companyName;
    nameAlias;
    description;
    streetAddress1;
    streetAddress2;
    city;
    state;
    postalCode;
    
    email;
    password;
    passwordVerify;
    firstName;
    lastName;
    phone;
    userTypeID;
}