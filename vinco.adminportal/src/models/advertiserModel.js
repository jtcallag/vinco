export default class advertiserModel {

    constructor() {}

    name;
    nameAlias;
    description;
    streetAddress1;
    streetAddress2;
    city;
    state;
    postalCode;
    createdByName;
    createdDate;
    modifiedByName;
    modifiedDate;
    isEnabled;
}