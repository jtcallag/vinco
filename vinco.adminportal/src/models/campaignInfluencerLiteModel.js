export default class campaignInfluencerLiteModel {

    constructor() {}

    uniqID;
    campaignUniqID;
    influencerUniqID;
    fullName;
    firstName;
    lastName;
    dateOfBirth;
    age;
    clickCount;
    campaignInfluencerClickUrl;
    isEnabled;
}