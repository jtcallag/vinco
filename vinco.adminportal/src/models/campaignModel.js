export default class campaignModel {

    constructor() {}

    uniqID;
    campaignName;
    productName;
    productDescription;
    productUrl;
    startDate;
    endDate;
    isEnabled;
    createdByName;
    createdDate;
    modifiedByName;
    modifiedDate;
    advertiserUniqID;
    advertiserName;
}