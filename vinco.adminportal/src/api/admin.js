import axios from 'axios';
import store  from '../store'
import Vue from 'vue'


const baseUrl = process.env.VUE_APP_API_BASE_URL;

// create an axios instance with default options
const http = axios.create({ baseURL: baseUrl });

http.interceptors.request.use(req => {
    // `req` is the Axios request config, so you can modify
    // the `headers`.
    var  token = store.getters.authToken;
    req.headers.authorization = `Bearer ${token}`;
    return req;
  });

  export default {
    save(_appSetting) {
        return http.put('/api/admin/setting', _appSetting);
    },
    get() {
      return http.get(`/api/admin/settings`);
    }
}