import axios from 'axios';
import store  from '../store'
import Vue from 'vue'


const baseUrl = process.env.VUE_APP_API_BASE_URL;

// create an axios instance with default options
const http = axios.create({ baseURL: baseUrl });

http.interceptors.request.use(req => {
    // `req` is the Axios request config, so you can modify
    // the `headers`.
    var  token = store.getters.authToken;
    req.headers.authorization = `Bearer ${token}`;
    return req;
  });

  export default {
    search(_searchText) {
        return http.get(`/api/advertiser/search/${_searchText}`);
    },
    register(_registration) {
        return http.post(`/api/advertiser/register`, _registration)
    },
    save(_advertiser) {
        return http.put('/api/advertiser', _advertiser);
    },
    get(_uniqID) {
      return http.get(`/api/advertiser/${_uniqID}`);
    },
    getCampaigns(_uniqID) {
      return http.get(`/api/advertiser/${_uniqID}/campaigns`);
    },
    addCampaign(_uniqID, _campaign) {
      return http.post(`/api/advertiser/${_uniqID}/campaigns`, _campaign);
    },
    updateCampaign(_uniqID, _campaign) {
      return http.put(`/api/advertiser/${_uniqID}/campaigns`, _campaign)
    },
    uploadLogo(_uniqID, _logoFile) {
      let formData = new FormData();
      formData.append("logoFile", _logoFile);
      return http.post(`/api/advertiser/${_uniqID}/logo`, formData, { headers: {
        "Content-Type": "multipart/form-data" }
      });
    }


    

    
};