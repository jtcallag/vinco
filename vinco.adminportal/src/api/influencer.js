import axios from 'axios';
import store  from '../store'
import Vue from 'vue'


const baseUrl = process.env.VUE_APP_API_BASE_URL;

// create an axios instance with default options
const http = axios.create({ baseURL: baseUrl });

http.interceptors.request.use(req => {
    // `req` is the Axios request config, so you can modify
    // the `headers`.
    var  token = store.getters.authToken;
    req.headers.authorization = `Bearer ${token}`;
    return req;
  });

  export default {
    search(_searchText) {
        return http.get(`/api/influencer/search/${_searchText}`);
    },
    register(_registration) {
        return http.post(`/api/influencer/register`, _registration)
    },
    save(_influencer) {
        return http.put('/api/influencer', _influencer);
    },
    get(_uniqID) {
      return http.get(`/api/influencer/${_uniqID}`);
    },
    getCampaigns(_uniqID) {
      return http.get(`/api/influencer/${_uniqID}/campaigns`);
    },
    addCampaign(_uniqID, _campaignUniqID, _campaign) {
      return http.post(`/api/influencer/${_uniqID}/campaign/${_campaignUniqID}`);
    },
    updateCampaign(_uniqID, _campaignUniqID, _campaign) {
      return http.put(`/api/influencer/${_uniqID}/campaign/${_campaignUniqID}`, _campaign)
    },
    uploadAvatar(_uniqID, _avatarFile) {
      let formData = new FormData();
      formData.append("avatarFile", _avatarFile);
      return http.post(`/api/influencer/${_uniqID}/avatar`, formData, { headers: {
        "Content-Type": "multipart/form-data" }
      });
    }


    

    
};