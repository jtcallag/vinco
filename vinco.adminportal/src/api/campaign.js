import axios from 'axios';
import store  from '../store'
import Vue from 'vue'


const baseUrl = process.env.VUE_APP_API_BASE_URL;

// create an axios instance with default options
const http = axios.create({ baseURL: baseUrl });

http.interceptors.request.use(req => {
    // `req` is the Axios request config, so you can modify
    // the `headers`.
    var  token = store.getters.authToken;
    req.headers.authorization = `Bearer ${token}`;
    return req;
  });

  export default {
    
    get(_uniqID) {
      return http.get(`/api/campaign/${_uniqID}`);
    },
    add(_model) {
        return http.post(`/api/campaign`, _model);
    },
    update(_model) {
        return http.put(`/api/campaign`, _model);
    },
    uploadImage(_uniqID, _imgNum, _imgFile) {
      let formData = new FormData();
      formData.append("imageFile", _imgFile);
      return http.post(`/api/campaign/${_uniqID}/image/${_imgNum}`, formData, { headers: {
        "Content-Type": "multipart/form-data" }
      });
    },
    getInfluencers(_uniqID) {
      return http.get(`/api/campaign/${_uniqID}/influencers`);
    },
    search(_model) {  //campaignSearchModel
      return http.post(`/api/campaign/search`, _model);
    }


}
