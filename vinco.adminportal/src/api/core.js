import axios from 'axios';

const baseUrl = process.env.VUE_APP_API_BASE_URL;

// create an axios instance with default options
const http = axios.create({ baseURL: baseUrl });

export default {
    login(_email, _password) {
        // then return the promise of the axios instance
        return http.post('/api/admin/login',
        {
            email: _email,
            password: _password
        })
    },
    anotherEndpoint() {
        return http.get('other/endpoint');
    }
};