import Vue from 'vue'
import Vuex from 'vuex'
import core from './modules/core';
import advertisers from './modules/advertisers';
import influencers from './modules/influencers';
import campaigns from './modules/campaigns';
import admin from './modules/admin';

Vue.use(Vuex)

//const store = new Vuex.Store({
export default new Vuex.Store({
  modules: {
     core,
     advertisers,
     influencers,
     campaigns,
     admin
  }
});




