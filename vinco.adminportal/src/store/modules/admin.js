import admin from '../../api/admin';
import router from '@/router/index';

const state = {
    appSettings: []
};

const getters = {
    appSettings: state => state.appSettings,
    appSettings_DefaultLogoUrl: state => state.appSettings.find(n => n.name == 'DefaultLogoUrl').value,
    appSettings_DefaultCampaignUrl: state => state.appSettings.find(n => n.name == 'DefaultCampaignImageUrl').value,
    appSettings_DefaultInfluencerUrl: state => state.appSettings.find(n => n.name == 'DefaultInfluencerImageUrl').value,
    appSettings_MediaRootUrl: state => state.appSettings.find(n => n.name == 'MediaRootUrl').value,
    appSettings_ApiBaseUrl: state => process.env.VUE_APP_API_BASE_URL
    
};

const actions = {
    async getAppSettings({commit}) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await admin.get()
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setAppSettings', response.data.payload);
            }
            else {
                commit('setNotification', { notificationType: 'error', message: `Failed to retrieve Application Settings: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       

    },
    async saveAppSetting({commit, dispatch}, obj) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await admin.save(obj)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setNotification', { notificationType: 'success', message: `Application Setting saved.`}, { root: true })
                dispatch('getAppSettings');
             }
            else {
                commit('setNotification', { notificationType: 'error', message: `Application Setting save failed: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       
    }

        
};

const mutations = {
    setAppSettings: (state, result) => (state.appSettings = result)
};

export default {
    state,
    getters,
    actions,
    mutations
}
