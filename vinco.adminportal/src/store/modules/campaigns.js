import campaign from '../../api/campaign';
import router from '@/router/index';

const state = {
    currentCampaignSearchResult: [],
    currentCampaign: {},
    currentCampaignInfluencers: []
};

const getters = {
    currentCampaignSearchResult: state => state.currentCampaignSearchResult,
    currentCampaign: state => state.currentCampaign,
    currentCampaignInfluencers: state => state.currentCampaignInfluencers
};

const actions = {
    async setCurrentCampaign({ commit }, campaign) {
        commit('setCurrentCampaign', campaign);
    },
    async setCurrentCampaignInfluencers({ commit }, campaignInfluencers) {
        commit('setCurrentCampaignInfluencers', campaignInfluencers);
    },
    async getCampaign({ commit }, uniqID) {
        commit('setNotification', { notificationType: 'loading', message: `` }, { root: true })
        const response = await campaign.get(uniqID)
            .then((response) => {
                if (response.data.isSuccessful) {
                    commit('setCurrentCampaign', response.data.payload);
                }
                else {
                    commit('setNotification', { notificationType: 'error', message: `Failed to retrieve Campaign: ${response.data.message}` }, { root: true })
                }

            }).catch((e) => {
                commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
            }).finally(() => {
                commit('clearNotification', { notificationType: 'loading', message: `` }, { root: true })
            })

    },
    async addCampaign({ commit, dispatch }, obj) {
        commit('setNotification', { notificationType: 'loading', message: `` }, { root: true })
        const response = await campaign.add(obj)
            .then((response) => {
                if (response.data.isSuccessful) {
                    commit('setNotification', { notificationType: 'success', message: `Campaign created.` }, { root: true })
                    router.push(`/advertiser/${response.data.payload.advertiserUniqID}/campaign/${response.data.payload.uniqID}`)
                }
                else {
                    if (response.data.hasValidationErrors) {
                        commit('setNotification', { notificationType: 'error', message: response.data.message, hasValidationErrors: response.data.hasValidationErrors, validationErrors: response.data.validationErrors }, { root: true })
                    }
                    else {
                        commit('setNotification', { notificationType: 'error', message: `Save failed: ${response.data.message}` }, { root: true })
                    }
                }

            }).catch((e) => {
                commit('setNotification', { notificationType: 'error', message: e.message, hasValidationErrors: e.hasValidationErrors, validationErrors: e.validationErrors }, { root: true })
            }).finally(() => {
                commit('clearNotification', { notificationType: 'loading', message: `` }, { root: true })
            })
    },
    async updateCampaign({ commit, dispatch }, obj) {
        commit('setNotification', { notificationType: 'loading', message: `` }, { root: true })
        const response = await campaign.update(obj)
            .then((response) => {
                if (response.data.isSuccessful) {
                    commit('setNotification', { notificationType: 'success', message: `Campaign saved.` }, { root: true })
                    dispatch('getCampaign', obj.uniqID);
                }
                else {
                    if (response.data.hasValidationErrors) {
                        commit('setNotification', { notificationType: 'error', message: response.data.message, hasValidationErrors: response.data.hasValidationErrors, validationErrors: response.data.validationErrors }, { root: true })
                    }
                    else {
                        commit('setNotification', { notificationType: 'error', message: `Save failed: ${response.data.message}` }, { root: true })
                    }
                }

            }).catch((e) => {
                commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
            }).finally(() => {
                commit('clearNotification', { notificationType: 'loading', message: `` }, { root: true })
            })
    },
    async uploadImage({ commit }, obj) {
        commit('setNotification', { notificationType: 'loading', message: `` }, { root: true })
        const response = await campaign.uploadImage(obj.uniqID, obj.imageNum, obj.imageFile)
            .then((response) => {
                if (response.data.isSuccessful) {
                    commit('setNotification', { notificationType: 'success', message: `Image saved.` }, { root: true })
                }
                else {
                    commit('setNotification', { notificationType: 'error', message: `Image upload failed: ${response.data.message}` }, { root: true })
                }

            }).catch((e) => {
                commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
            }).finally(() => {
                commit('clearNotification', { notificationType: 'loading', message: `` }, { root: true })
            })
    },
    async getCampaignInfluencers({ commit }, uniqID) {
        const response = await campaign.getInfluencers(uniqID)
            .then((response) => {
                if (response.data.isSuccessful) {
                    commit('setCurrentCampaignInfluencers', response.data.payload);
                }
                else {
                    commit('setNotification', { notificationType: 'error', message: `Failed to retrieve Influencers for Campaign: ${response.data.message}` }, { root: true })
                }

            }).catch((e) => {
                commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
            }).finally(() => {

            })

    },
    async campaignSearch({ commit }, _model) {
        const response = await campaign.search(_model)
            .then((response) => {
                if (response.data.isSuccessful) {

                    //clear out the campaigns that the influencer is already subscribed to
                    var result  = response.data.payload.filter(csri => !this.getters.currentInfluencerCampaigns.find(cci => (cci.campaignUniqID === csri.uniqID) ))
                    commit('setCurrentCampaignSearchResult', result);
                }
                else {
                    commit('setNotification', { notificationType: 'error', message: `Failed to search Campaigns: ${response.data.message}` }, { root: true })
                }

            }).catch((e) => {
                commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
            }).finally(() => {

            })

    },
    async clearCampaignSearch({commit}) {
        commit('setCurrentCampaignSearchResult', []);
    },
    async removeCampaignSearchResultItem({ commit }, _uniqID) {
          commit('removeCampaignSearchResultItem', _uniqID);
    }

};

const mutations = {
    setCurrentCampaignSearchResult: (state, result) => (state.currentCampaignSearchResult = result),
    setCurrentCampaign: (state, campaign) => (state.currentCampaign = campaign),
    setCurrentCampaignInfluencers: (state, result) => (state.currentCampaignInfluencers = result),
    removeCampaignSearchResultItem: function (state, campaignUniqID) {
        var filteredResult = state.currentCampaignSearchResult.filter(function( obj ) {
            return obj.uniqID !== campaignUniqID;
          });
        state.currentCampaignSearchResult = filteredResult;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}
