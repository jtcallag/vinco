import advertiser from '../../api/advertiser';
import router from '@/router/index';

const state = {
    advertiserSearchResult: [],
    currentAdvertiser: {},
    currentAdvertiserCampaigns: []
};

const getters = {
    advertiserSearchResult: state => state.advertiserSearchResult,
    currentAdvertiser: state => state.currentAdvertiser,
    currentAdvertiserCampaigns: state => state.currentAdvertiserCampaigns
};

const actions = {
    async advertiserSearch({commit}, searchText) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await advertiser.search(searchText)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setAdvertiserSearchResult', response.data.payload);
            }
            else {
                commit('setNotification', { notificationType: 'error', message: `Search failed: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })
    },
    async clearAdvertiserSearch({commit}) {
        commit('setAdvertiserSearchResult', []);
    },
    async setCurrentAdvertiser({commit}, advertiser) {
        commit('setCurrentAdvertiser', advertiser);
    },
    async clearCurrentAdvertiser({commit}) {
        commit('setCurrentAdvertiser', {});
        commit('setCurrentAdvertiserCampaigns', []);
    },
    async getAdvertiser({commit}, uniqID) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await advertiser.get(uniqID)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setCurrentAdvertiser', response.data.payload);
            }
            else {
                commit('setNotification', { notificationType: 'error', message: `Failed to retrieve Advertiser: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       

    },
    async saveAdvertiser({commit, dispatch}, obj) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await advertiser.save(obj)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setNotification', { notificationType: 'success', message: `Advertiser saved.`}, { root: true })
                dispatch('getAdvertiser', obj.uniqID);
             }
            else {
                commit('setNotification', { notificationType: 'error', message: `Save failed: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       
    },
    async registerAdvertiser({commit, dispatch}, obj) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await advertiser.register(obj)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setNotification', { notificationType: 'success', message: `Advertiser registered.`}, { root: true })
                router.push(`/advertiser/${response.data.payload.uniqID}`)
             }
             else if (response.data.hasValidationErrors) {
                commit('setNotification', { notificationType: 'error', message: `<ul><li>msg 1</li><li>msg 2</li></ul>`}, { root: true })
             }
            else {
                commit('setNotification', { notificationType: 'error', message: `Registration failed: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       
    },
    async getCampaigns({commit}, uniqID) {
        const response = await advertiser.getCampaigns(uniqID)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setCurrentAdvertiserCampaigns', response.data.payload);
            }
            else {
                commit('setNotification', { notificationType: 'error', message: `Failed to retrieve Advertiser Campaigns: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        })       

    },
    async uploadLogo({commit}, obj) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await advertiser.uploadLogo(obj.uniqID, obj.logoFile)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setNotification', { notificationType: 'success', message: `Logo saved.`}, { root: true })
             }
            else {
                commit('setNotification', { notificationType: 'error', message: `Logo upload failed: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       
    }        
        
};

const mutations = {
    setAdvertiserSearchResult: (state, result) => (state.advertiserSearchResult = result),
    setCurrentAdvertiser: (state, advertiser) => (state.currentAdvertiser = advertiser),
    setCurrentAdvertiserCampaigns: (state, campaigns) => (state.currentAdvertiserCampaigns = campaigns)
};

export default {
    state,
    getters,
    actions,
    mutations
}
