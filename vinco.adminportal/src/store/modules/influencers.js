import influencer from '../../api/influencer';
import router from '@/router/index';

const state = {
    influencerSearchResult: [],
    currentInfluencer: {},
    currentInfluencerCampaigns: []
};

const getters = {
    influencerSearchResult: state => state.influencerSearchResult,
    currentInfluencer: state => state.currentInfluencer,
    currentInfluencerCampaigns: state => state.currentInfluencerCampaigns
};

const actions = {
    async influencerSearch({commit}, searchText) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await influencer.search(searchText)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setInfluencerSearchResult', response.data.payload);
            }
            else {
                commit('setNotification', { notificationType: 'error', message: `Search failed: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })
    }, 
    async clearInfluencerSearch({commit}) {
        commit('setInfluencerSearchResult', []);
    },
    async setCurrentInfluencer({commit}, influencer) {
        commit('setCurrentInfluencer', influencer);
    },
    async clearCurrentInfluencer({commit}) {
        commit('setCurrentInfluencer', {});
        commit('setCurrentInfluencerCampaigns', []);
    },
    async getInfluencer({commit}, uniqID) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await influencer.get(uniqID)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setCurrentInfluencer', response.data.payload);
            }
            else {
                commit('setNotification', { notificationType: 'error', message: `Failed to retrieve Influencer: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       

    },
    async saveInfluencer({commit, dispatch}, obj) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await influencer.save(obj)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setNotification', { notificationType: 'success', message: `Influencer saved.`}, { root: true })
                dispatch('getInfluencer', obj.uniqID);
             }
            else {
                commit('setNotification', { notificationType: 'error', message: `Save failed: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       
    },
    async registerInfluencer({commit, dispatch}, obj) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await influencer.register(obj)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setNotification', { notificationType: 'success', message: `Influencer registered.`}, { root: true })
                router.push(`/influencer/${response.data.payload.uniqID}`)
             }
             else if (response.data.hasValidationErrors) {
                commit('setNotification', { notificationType: 'error', message: response.data.message, hasValidationErrors: response.data.hasValidationErrors, validationErrors: response.data.validationErrors }, { root: true })
             }
            else {
                commit('setNotification', { notificationType: 'error', message: `Registration failed: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       
    },
    async getInfluencerCampaigns({commit}, uniqID) {
        const response = await influencer.getCampaigns(uniqID)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setCurrentInfluencerCampaigns', response.data.payload);
            }
            else {
                commit('setNotification', { notificationType: 'error', message: `Failed to retrieve Influencer Campaigns: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        })       

    },
    async addInfluencerCampaign({commit, dispatch}, obj) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await influencer.addCampaign(obj.influencerUniqID, obj.campaignUniqID)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setNotification', { notificationType: 'success', message: `Campaign added.`}, { root: true })
                dispatch('getInfluencerCampaigns', obj.influencerUniqID);
                dispatch('removeCampaignSearchResultItem', obj.campaignUniqID);
             }
            else {
                commit('setNotification', { notificationType: 'error', message: `Add Campaign failed: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       
    },
    async updateInfluencerCampaign({commit, dispatch}, obj) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await influencer.updateCampaign(obj.influencerUniqID, obj.campaignUniqID, obj)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setNotification', { notificationType: 'success', message: `Campaign saved.`}, { root: true })
                dispatch('getInfluencerCampaigns', obj.influencerUniqID);
             }
            else {
                commit('setNotification', { notificationType: 'error', message: `Save Campaign failed: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       
    },
    async uploadAvatar({commit}, obj) {
        commit('setNotification', { notificationType: 'loading', message: ``}, { root: true })
        const response = await influencer.uploadAvatar(obj.uniqID, obj.avatarFile)
        .then((response) => {
            if (response.data.isSuccessful) {
                commit('setNotification', { notificationType: 'success', message: `Avatar saved.`}, { root: true })
             }
            else {
                commit('setNotification', { notificationType: 'error', message: `Avatar upload failed: ${response.data.message}`}, { root: true })
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message }, { root: true })
        }).finally(() => {
            commit('clearNotification', { notificationType: 'loading', message: ``}, { root: true })
        })       
    }     
        
};

const mutations = {
    setInfluencerSearchResult: (state, result) => (state.influencerSearchResult = result),
    setCurrentInfluencer: (state, influencer) => (state.currentInfluencer = influencer),
    setCurrentInfluencerCampaigns: (state, campaigns) => (state.currentInfluencerCampaigns = campaigns)
};

export default {
    state,
    getters,
    actions,
    mutations
}

