import core from '../../api/core';

const baseUrl = process.env.VUE_APP_API_BASE_URL;

const state = {
    isAuthenticated: false,
    authToken: '',
    loggedInUser: [],
    notifyInfo: false,
    messageInfo: '',
    notifyWarning: false,
    messageWarning: '',
    notifySuccess: false,
    messageSuccess: '',
    notifyError: false,
    messageError: '',
    notifyLoading: false

};

const getters = {
    isAuthenticated: state => state.isAuthenticated,
    authToken: state => state.authToken,
    loggedInUser: state => state.loggedInUser,
    notifyInfo: state => state.notifyInfo,
    messageInfo: state => state.messageInfo,
    notifySuccess: state => state.notifySuccess,
    messageSuccess: state => state.messageSuccess,
    notifyWarning: state => state.notifyWarning,
    messageWarning: state => state.messageWarning,
    notifyError: state => state.notifyError,
    messageError: state => state.messageError,
    notifyLoading: state => state.notifyLoading

};


const actions = {
    async login({commit}, credentials) {
        const response = await core.login(credentials.email, credentials.password)
        .then((response) => {
            commit('setIsAuthenticated', response.data.isSuccessful);
            if (response.data.isSuccessful) {
                commit('setAuthToken', response.data.payload.authenticationToken);
                commit('setLoggedInUser', response.data.payload);
                commit('setNotification', { notificationType: 'success', message: 'Login successful.'})
            }
            else {
                commit('setNotification', { notificationType: 'error', message: `Login failed: ${response.data.message}`})
            }

        }).catch((e) => {
            commit('setNotification', { notificationType: 'error', message: e.message })
        })
    },
    async clearNotification( {commit}, notificationType) {
        commit('clearNotification', notificationType);
    },
    async setNotification( {commit}, notification) {
        commit('setNotification', notification);
    }

};

const mutations = {
    setIsAuthenticated: (state, isAuthenticated) => (state.isAuthenticated = isAuthenticated),
    setAuthToken: (state, authToken) => (state.authToken = authToken),
    setLoggedInUser: (state, usr) => (state.loggedInUser = usr),
    setNotification: (state, notification) => {
        switch(notification.notificationType) {
            case "info":
                state.notifyInfo = true;
                state.messageInfo = notification.message;
                break;
            case "warning":
                state.notifyWarning = true;
                state.messageWarning = notification.message;
                break;
            case "success":
                state.notifySuccess = true;
                state.messageSuccess = notification.message;
                break;
            case "error":
                state.notifyError = true;
                if (notification.hasValidationErrors) {
                    state.messageError = '<ul>';
                    notification.validationErrors.forEach(e => state.messageError += `<li>${e}</li>` );
                    state.messageError += '</ul>';
                }
                else {
                    state.messageError = notification.message;
                }
                break;
            case "loading":
                state.notifyLoading = true;
                break;
                }
    },
    clearNotification: (state, notification) => {
        switch(notification.notificationType) {
            case "info":
                state.notifyInfo = false;
                state.messageInfo = '';
                break;
            case "warning":
                state.notifyWarning = false;
                state.messageWarning = '';
                break;
            case "success":
                state.notifySuccess = false;
                state.messageSuccess = '';
                break;
            case "error":
                state.notifyError = false;
                state.messageError = '';
                break;
            case "loading":
                state.notifyLoading = false;
                break;
    
            }
    }

};

export default {
    state,
    getters,
    actions,
    mutations
}
