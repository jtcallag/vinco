import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Advertiser from '../views/Advertiser.vue'
import AdvertiserRegister from '../views/AdvertiserRegister.vue'
import Advertisers from '../views/Advertisers.vue'
import Campaign from '../views/Campaign.vue'
import Campaigns from '../views/Campaigns.vue'
import Influencer from '../views/Influencer.vue'
import Influencers from '../views/Influencers.vue'
import InfluencerRegister from '../views/InfluencerRegister.vue'
import ApplicationSettings from '../views/ApplicationSettings'
import Utilities from '../views/Utilities'

Vue.use(VueRouter)

function guardAuthenticated(to, from, next)
{
  var isAuthenticated= store.getters.isAuthenticated;

  if(isAuthenticated) 
  {
    next(); // allow to enter route
  } 
  else
  {
    next('/login'); // go to '/login';
  }
}

const routes = [
  {
    path: '/',
    name: 'Home',
    beforeEnter : guardAuthenticated,
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/settings',
    name: 'Settings',
    beforeEnter : guardAuthenticated,
    component: ApplicationSettings
  },
  {
    path: '/advertiser/new',
    name: 'AdvertiserRegister',
    beforeEnter : guardAuthenticated,
    component: AdvertiserRegister,
    props: true
  },
  {
    path: '/advertiser/:id',
    name: 'Advertiser',
    beforeEnter : guardAuthenticated,
    component: Advertiser,
    props: true
  },
  {
    path: '/advertiser/:advertiserid/campaign/:id',
    name: 'Campaign',
    beforeEnter : guardAuthenticated,
    component: Campaign,
    props: true
  },
  {
    path: '/advertisers',
    name: 'Advertisers',
    beforeEnter : guardAuthenticated,
    component: Advertisers
  },
/*   {
    path: '/campaign/:id',
    name: 'Campaign',
    beforeEnter : guardAuthenticated,
    component: Campaign
  }, */
  {
    path: '/campaigns',
    name: 'Campaigns',
    component: Campaigns
  },
  {
    path: '/influencer/new',
    name: 'InfluencerRegister',
    beforeEnter : guardAuthenticated,
    component: InfluencerRegister,
    props: true
  },
  {
    path: '/influencer/:id',
    name: 'Influencer',
    component: Influencer,
    props: true
  },
  {
    path: '/influencers',
    name: 'Influencers',
    component: Influencers
  },
  {
    path: '/utilities',
    name: 'Utilities',
    component: Utilities
  },


]

const router = new VueRouter({
  routes
})

export default router
