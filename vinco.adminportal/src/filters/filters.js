import Vue from 'vue';
import moment from 'moment';


Vue.filter('formatDate', function(value) {
    if (value) {
      var result = moment(String(value)).format('lll'); 
      return result;
    }
  });

  Vue.filter('formatDateTime', function(value) {
    if (value) {
      var result = moment(String(value)).format('MM/DD/YYYY HH:mm:ss'); 
      return result;
    }
  });

  Vue.filter('formatShortDate', function(value) {
    if (value) {
      var result = moment(String(value)).format('MM/DD/YYYY'); 
      return result;
    }
  });
