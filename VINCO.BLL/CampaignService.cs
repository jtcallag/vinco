﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VINCO.BLL.DAL;
using VINCO.BLL.Interfaces;
using VINCO.Model.ViewModel;
using VINCO.Model;

namespace VINCO.BLL
{
    public class CampaignService : BaseService, ICampaignService
    {

        public CampaignService(VincoDbContext context) : base(context)
        {
        }

        public async Task<List<CampaignViewModel>> Search(CampaignSearchViewModel searchModel)
        {
            var viewModel = new List<CampaignViewModel>();
            await Task.Run(() =>
            {
                var result = _context.Campaigns.Include(u => u.CreatedByUser)
                                               .Include(u => u.ModifiedByUser)
                                               .Include(a => a.Advertiser)
                                                     .Where(c => (c.CampaignName.Contains(searchModel.Keyword) ||
                                                                  c.Advertiser.Name.Contains(searchModel.Keyword) ||
                                                                  c.Advertiser.NameAlias.Contains(searchModel.Keyword) ||
                                                                  c.ProductName.Contains(searchModel.Keyword) ||
                                                                  c.ProductDescription.Contains(searchModel.Keyword)) &&
                                                                 (c.EndDate >= DateTime.Today &&
                                                                  c.IsEnabled))
                                                     .Distinct()
                                                     .OrderBy(c => c.EndDate);

                foreach (var campaign in result)
                {
                    var vm = new CampaignViewModel();
                    vm.From(campaign);
                    viewModel.Add(vm);
                }
            });
            return viewModel;
        }

        public async Task<CampaignViewModel> Add(CampaignViewModel model)
        {
            var advertiser = await _context.Advertisers.FirstOrDefaultAsync(a => a.UniqID == model.AdvertiserUniqID);

            var campaign = model.To();
            campaign.AdvertiserID = (int)advertiser.ID;
            campaign.UniqID = Guid.NewGuid().ToString();

            Decorate(campaign, _context.CurrentUser.ID, true);
            Validate(campaign);

            _context.Campaigns.Add(campaign);
            await _context.SaveChangesAsync();

            model.UniqID = campaign.UniqID;
            return model;
        }

        public async Task<bool> Update(CampaignViewModel model)
        {
            var orig = await _context.Campaigns.FirstOrDefaultAsync(a => a.UniqID == model.UniqID);

            //set the uniqID as it is a new campaign
            model.UniqID = Guid.NewGuid().ToString();

            var updated = model.To();
            orig.CampaignName = updated.CampaignName;
            orig.ProductName = updated.ProductName;
            orig.ProductDescription = updated.ProductDescription;
            orig.ProductUrl = updated.ProductUrl;
            orig.StartDate = updated.StartDate;
            orig.EndDate = updated.EndDate;
            orig.IsEnabled = updated.IsEnabled;

            Decorate(orig, _context.CurrentUser.ID);
            Validate(orig);

            await _context.SaveChangesAsync();

            return true;
        }


        public async Task<CampaignViewModel> Get(string uniqID)
        {
            var result = await _context.Campaigns.Include(a => a.CreatedByUser)
                                       .Include(b => b.ModifiedByUser)
                                       .Include(c => c.Advertiser)
                                       .FirstOrDefaultAsync<Campaign>(a => a.UniqID == uniqID);

            var viewModel = new CampaignViewModel();
            viewModel.From(result);
            return viewModel;
        }

        public async Task<List<CampaignInfluencerLiteViewModel>> GetInfluencers(string campaignUniqID)
        {
            return await Task.Run(() =>
            {
                var campaignInfluencers = new List<CampaignInfluencerLiteViewModel>();

                var result = _context.CampaignInfluencers.Include(a => a.Campaign)
                                                         .Include(b => b.Influencer)
                                                         .ThenInclude(c => c.User)
                                                         .Where(a => a.Campaign.UniqID == campaignUniqID);

                foreach (var ci in result)
                {
                    var vm = new CampaignInfluencerLiteViewModel();
                    vm.From(ci);
                    campaignInfluencers.Add(vm);
                }

                return campaignInfluencers;

            });
        }

    }
}
