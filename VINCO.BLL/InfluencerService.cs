﻿using System;
using VINCO.BLL.Interfaces;
using VINCO.BLL.DAL;
using VINCO.Common;
using VINCO.Model;
using VINCO.Model.ViewModel;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace VINCO.BLL
{
    public class InfluencerService : LoginBaseService, IInfluencerService
    {
        public InfluencerService(VincoDbContext context) : base(context)
        {
        }


        public async Task<InfluencerViewModel> Register(RegistrationViewModel model)
        {
            //set the UserType as Influencer.  will need for ValidationPre.
            model.UserTypeID = (int)VINCO.Model.Enums.UserType.Influencer;

            ValidatePre(model);

            var user = model.ToUser();

            //get a random salt and hash the password for the user
            user.PasswordSalt = Encryption.GenerateSalt();
            user.Password = Encryption.GetHash(model.Password, user.PasswordSalt);
            user.IsRegistrationComplete = false;
            user.IsActive = true;

            var influencer = model.ToInfluencer();

            user.UniqID = Guid.NewGuid().ToString();
            influencer.UniqID = Guid.NewGuid().ToString();

            Validate(new List<object>() { user, influencer });

            Decorate(user, _context.CurrentUser?.ID ?? Globals.SystemUserID, true);
            Decorate(influencer, _context.CurrentUser?.ID ?? Globals.SystemUserID, true);

            influencer.User = user;
            _context.Influencers.Add(influencer);
 
            await _context.SaveChangesAsync();

            //insert UserVerification here - send email to confirm enrollment.

            var i = _context.Influencers.Include(cb => cb.CreatedByUser)
                                        .Include(mb => mb.ModifiedByUser)
                                        .Include(u => u.User)
                                        .FirstOrDefault(a => a.ID == influencer.ID);

            var vm = new InfluencerViewModel();
            vm.From(i);


            return vm;

        }

        public async Task<InfluencerViewModel> Login(LoginViewModel model)
        {
            var result = await base.Login(model.Email, model.Password, Model.Enums.UserType.Influencer);

            var influencer = _context.Influencers.Include(a => a.CreatedByUser)
                                                 .Include(a => a.ModifiedByUser)
                                                 .Include(u => u.User)
                                                 .FirstOrDefault(v => v.UserID == result);

            var vm = new InfluencerViewModel();
            vm.From(influencer);

            //set the token to the viewmodel.
            vm.AuthenticationToken = GenerateJwtToken(influencer.User, influencer.UniqID);

            return vm;
        }

        public async Task<List<InfluencerViewModel>> Search(string searchText)
        {
            Authorize(Model.Enums.UserType.VincoAdmin);

            var viewModel = new List<InfluencerViewModel>();
            await Task.Run(() =>
            {
                var result = _context.Influencers.Include(a => a.CreatedByUser)
                                                 .Include(b => b.ModifiedByUser)
                                                 .Include(c => c.User)
                                                 .Where(a => a.User.Email.Contains(searchText) || 
                                                             a.User.MobilePhone.Contains(searchText) ||
                                                             a.User.LastName.Contains(searchText));

                foreach (var a in result)
                {
                    var vm = new InfluencerViewModel();
                    vm.From(a);
                    viewModel.Add(vm);
                }
            });

            return viewModel;

        }


        public async Task<InfluencerViewModel> Get(string influencerUniqID)
        {
            var result = await _context.Influencers.Include(a => a.CreatedByUser)
                                                   .Include(b => b.ModifiedByUser)
                                                   .Include(c => c.User)
                                                   .FirstOrDefaultAsync<Influencer>(a => a.UniqID == influencerUniqID);

            var viewModel = new InfluencerViewModel();
            viewModel.From(result);
            return viewModel;

        }

        public async Task<bool> Update(InfluencerViewModel model)
        {
            var orig = await _context.Influencers.Include(u => u.User)
                                                 .FirstOrDefaultAsync<Influencer>(a => a.UniqID == model.UniqID);

            orig.User.FirstName = model.FirstName;
            orig.User.LastName = model.LastName;
            orig.User.Email = model.Email;
            orig.User.MobilePhone = model.MobilePhone;
            orig.User.IsActive = model.IsActive;
            orig.DateOfBirth = model.DateOfBirth;
            orig.NameAlias = model.NameAlias;
            
            Decorate(orig, _context.CurrentUser.ID);
            Validate(new List<object>() { orig, orig.User });

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> AddCampaign(string campaignUniqID, string influencerUniqID)
        {
            var campaign = await _context.Campaigns.FirstOrDefaultAsync(a => a.UniqID == campaignUniqID);
            var influencer = await _context.Influencers.FirstOrDefaultAsync(b => b.UniqID == influencerUniqID);

            var campaignInfluencer = new CampaignInfluencer();
            campaignInfluencer.CampaignID = (int)campaign.ID;
            campaignInfluencer.InfluencerID = (int)influencer.ID;
            campaignInfluencer.UniqID = Guid.NewGuid().ToString();
            campaignInfluencer.IsEnabled = true;
            campaignInfluencer.Influencer = influencer;
            var settings = await _context.GetAppSettings();
            campaignInfluencer.CampaignInfluencerClickUrl = $"{settings.FirstOrDefault(s => s.Name == "BaseApiUrl")?.Value}/api/click/{campaignInfluencer.UniqID}";

            
            Validate(campaignInfluencer);

            _context.CampaignInfluencers.Add(campaignInfluencer);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateCampaign(CampaignInfluencerViewModel model)
        {
            var orig = await _context.CampaignInfluencers.FirstOrDefaultAsync<CampaignInfluencer>(a => a.UniqID == model.UniqID);

            orig.IsEnabled = model.IsEnabled;
            

            //Decorate(orig, _context.CurrentUser.ID);
            Validate(new List<object>() { orig });

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<List<CampaignInfluencerViewModel>> GetCampaigns(string influencerUniqID)
        {
            var result = await _context.Influencers.Include(a => a.CampaignInfluencers)
                                                   .ThenInclude(b => b.Campaign)
                                                   .ThenInclude(c => c.Advertiser)
                                                   .FirstOrDefaultAsync<Influencer>(a => a.UniqID == influencerUniqID);

            var viewModel = new List<CampaignInfluencerViewModel>();
            foreach (var campaignInfluencer in result.CampaignInfluencers)
            {
                var vm = new CampaignInfluencerViewModel();
                vm.From(campaignInfluencer);
                viewModel.Add(vm);
            }

            return viewModel;

        }





        public async Task<CampaignViewModel> GetCampaign(string campaignUniqID, string influencerUniqID)
        {
            return new CampaignViewModel();
        }
        public async Task<CampaignMetricsViewModel> GetCampaignMetrics(string campaignUniqID, string influencerUniqID)
        {
            return new CampaignMetricsViewModel();
        }
        
        public async Task<bool> SaveCampaignInfluencer(CampaignInfluencerViewModel model)
        {
            return true;
        }
        public async Task<bool> DisableCampaign(string campaignUniqID, string influencerUniqID)
        {
            var result = await _context.CampaignInfluencers.Include(c => c.Campaign)
                                                     .Include(i => i.Influencer)
                                                     .FirstOrDefaultAsync(ci => ci.Campaign.UniqID == campaignUniqID &&
                                                                                ci.Influencer.UniqID == influencerUniqID);
            result.IsEnabled = false;
            await _context.SaveChangesAsync();
            return true;
        }
        public async Task<PaymentProfileViewModel> GetPaymentProfile(string influencerUniqID)
        {
            return new PaymentProfileViewModel();
        }
        public async Task<bool> SavePaymentProfile(PaymentProfileViewModel model)
        {
            return true;
        }

    }
}
