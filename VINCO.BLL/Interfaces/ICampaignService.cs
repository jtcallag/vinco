﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VINCO.Model.ViewModel;

namespace VINCO.BLL.Interfaces
{
    public interface ICampaignService : IBaseService
    {
        Task<List<CampaignViewModel>> Search(CampaignSearchViewModel searchModel);
        Task<CampaignViewModel> Get(string uniqID);

        Task<List<CampaignInfluencerLiteViewModel>> GetInfluencers(string campaignUniqID);

        Task<CampaignViewModel> Add(CampaignViewModel model);
        Task<bool> Update(CampaignViewModel model);
    }
}
