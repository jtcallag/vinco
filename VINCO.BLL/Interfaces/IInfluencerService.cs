﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VINCO.Model.ViewModel;

namespace VINCO.BLL.Interfaces
{
    public interface IInfluencerService : IBaseService
    {
        Task<InfluencerViewModel> Register(RegistrationViewModel model);
        Task<InfluencerViewModel> Login(LoginViewModel model);
        Task<List<InfluencerViewModel>> Search(string searchText);

        Task<InfluencerViewModel> Get(string influencerUniqID);
        Task<bool> Update(InfluencerViewModel model);
        Task<List<CampaignInfluencerViewModel>> GetCampaigns(string influencerUniqID);
        Task<CampaignViewModel> GetCampaign(string campaignUniqID, string influencerUniqID);
        Task<CampaignMetricsViewModel> GetCampaignMetrics(string campaignUniqID, string influencerUniqID);
        Task<bool> AddCampaign(string campaignUniqID, string influencerUniqID);
        Task<bool> UpdateCampaign(CampaignInfluencerViewModel model);
        Task<bool> DisableCampaign(string campaignUniqID, string influencerUniqID);
        Task<PaymentProfileViewModel> GetPaymentProfile(string influencerUniqID);
        Task<bool> SavePaymentProfile(PaymentProfileViewModel model);

    }
}
