﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VINCO.Model.ViewModel;

namespace VINCO.BLL.Interfaces
{
    public interface IAdvertiserService : IBaseService
    {
        Task<AdvertiserViewModel> Register(RegistrationViewModel model);

        Task<AdvertiserViewModel> Login(LoginViewModel model);

        Task<AdvertiserViewModel> Get(string uniqID);

        Task<List<AdvertiserViewModel>> Search(string searchText);

        Task<bool> Update(AdvertiserViewModel model);

        
        Task<List<CampaignViewModel>> GetCampaigns(string advertiserUniqID);

    }
}
