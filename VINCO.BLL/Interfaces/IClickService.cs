﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VINCO.BLL.Interfaces
{
    public interface IClickService : IBaseService
    {
        Task<string> SaveClick(string campaignInfluencerUniqID);
    }
}
