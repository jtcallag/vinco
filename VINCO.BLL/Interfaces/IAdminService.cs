﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VINCO.Model.ViewModel;

namespace VINCO.BLL.Interfaces
{
    public interface IAdminService : IBaseService
    {
        Task<bool> ForgotPassword(string email);

        Task<AdminUserViewModel> Register(RegistrationViewModel model);

        Task<AdminUserViewModel> Login(LoginViewModel model);

        Task<bool> UpdatePassword(string email, string newPassword, string userUniqID);

        Task<List<ApplicationSettingViewModel>> GetApplicationSettings();
        Task<bool> UpdateApplicationSetting(ApplicationSettingViewModel model);

    }
}
