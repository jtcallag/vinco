﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using VINCO.Model.ViewModel;
using VINCO.Common;

namespace VINCO.BLL.Interfaces
{
    public interface IMediaService
    {
        Task<bool> SaveLogo(string uniqID, Stream logoFile);
        
        Task<bool> SaveAvatar(string uniqID, Stream logoFile);
        Task<bool> SaveImage(string uniqID, string imageNum, Stream imageFile, ImageType imageType);

    }
}
