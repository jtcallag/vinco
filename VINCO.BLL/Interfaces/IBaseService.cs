﻿using System;
using System.Collections.Generic;
using System.Text;
using VINCO.BLL.DAL;

namespace VINCO.BLL.Interfaces
{
    public interface IBaseService
    {
        void SetCurrentUser(CurrentUser user);
    }
}
