﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using VINCO.BLL.Interfaces;
using VINCO.BLL.DAL;
using VINCO.Common;
using VINCO.Model;
using VINCO.Model.ViewModel;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace VINCO.BLL
{
    public class AdvertiserService : LoginBaseService, IAdvertiserService
    {
        public AdvertiserService(VincoDbContext context) : base(context)
        {
        }

        public async Task<AdvertiserViewModel> Register(RegistrationViewModel model)
        {
            //set the UserType as Advertiser.  will need for ValidationPre.
            model.UserTypeID = (int)VINCO.Model.Enums.UserType.Advertiser;

            ValidatePre(model);
            
            var user = model.ToUser();

            //get a random salt and hash the password for the user
            user.PasswordSalt = Encryption.GenerateSalt();
            user.Password = Encryption.GetHash(model.Password, user.PasswordSalt);
            user.IsRegistrationComplete = false;
            user.IsActive = false;

            var advertiser = model.ToAdvertiser();

            user.UniqID = Guid.NewGuid().ToString();
            advertiser.UniqID = Guid.NewGuid().ToString();
            
            Validate(new List<object>() { user, advertiser });
            
            Decorate(user, _context.CurrentUser?.ID ?? Globals.SystemUserID, true);
            Decorate(advertiser, _context.CurrentUser?.ID ?? Globals.SystemUserID, true);

            advertiser.AdvertiserUsers = new List<AdvertiserUser>();
            var advertiserUser = new AdvertiserUser() { Advertiser = advertiser, User = user };
            advertiser.AdvertiserUsers.Add(advertiserUser);

            _context.Advertisers.Add(advertiser);

            await _context.SaveChangesAsync();

            //insert UserVerification here - send email to confirm enrollment.


            var a = _context.Advertisers.Include(cb => cb.CreatedByUser)
                                        .Include(mb => mb.ModifiedByUser)
                                        .Include(u => u.AdvertiserUsers)
                                        .ThenInclude(u => u.User)
                                        .FirstOrDefault(a => a.ID == advertiser.ID);

            var vm = new AdvertiserViewModel();
            vm.From(a);


            return vm;
        }

        public async Task<AdvertiserViewModel> Login(LoginViewModel model)
        {
            var result = await base.Login(model.Email, model.Password, Model.Enums.UserType.Advertiser);

            var advertiser = _context.Advertisers.Include(a => a.CreatedByUser)
                                                 .Include(a => a.ModifiedByUser)
                                                 .Include(au => au.AdvertiserUsers)
                                                 .ThenInclude(u => u.User)
                                                 .FirstOrDefault(v => v.AdvertiserUsers.Any(vu => vu.UserID == result));
                                     
            var vm = new AdvertiserViewModel();
            vm.From(advertiser);

            //set the token to the viewmodel.
            vm.AuthenticationToken = GenerateJwtToken(advertiser.AdvertiserUsers.First().User, advertiser.UniqID);
            return vm;
        }

        public async Task<AdvertiserViewModel> Get(string uniqID)
        {
            var result = await _context.Advertisers.Include(a => a.CreatedByUser)
                                                   .Include(b => b.ModifiedByUser)
                                                   .FirstOrDefaultAsync<Advertiser>(a => a.UniqID == uniqID);

            var viewModel = new AdvertiserViewModel();
            viewModel.From(result);
            return viewModel;
        }

        public async Task<List<AdvertiserViewModel>> Search(string searchText)
        {
            Authorize(Model.Enums.UserType.Influencer);

            var viewModel = new List<AdvertiserViewModel>();
            await Task.Run(() =>
            {
                var result = _context.Advertisers.Include(a => a.CreatedByUser)
                                                   .Include(b => b.ModifiedByUser)
                                                   .Where(a => a.Name.Contains(searchText) || a.NameAlias.Contains(searchText));

                foreach (var a in result)
                {
                    var vm = new AdvertiserViewModel();
                    if (_context.CurrentUser.UserType == Model.Enums.UserType.VincoAdmin)
                    {
                        vm.From(a);
                    }
                    else
                    {
                        vm.LiteFrom(a);
                    }
                    viewModel.Add(vm);
                }
            });
            
            return viewModel;
        }

        public async Task<bool> Update(AdvertiserViewModel model)
        {
            var orig = await _context.Advertisers.FirstOrDefaultAsync<Advertiser>(a => a.UniqID == model.UniqID);

            var updated = model.To();
            orig.Name = updated.Name;
            orig.NameAlias = updated.NameAlias;
            orig.StreetAddress1 = updated.StreetAddress1;
            orig.StreetAddress2 = updated.StreetAddress2;
            orig.City = updated.City;
            orig.State = updated.State;
            orig.PostalCode = updated.PostalCode;
            orig.Description = updated.Description;
            orig.IsEnabled = updated.IsEnabled;

            Decorate(orig, _context.CurrentUser.ID);
            Validate(orig);

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<List<CampaignViewModel>> GetCampaigns(string advertiserUniqID)
        {
            var result = await _context.Advertisers.Include(a => a.Campaigns)
                                                        .ThenInclude(cu => cu.CreatedByUser)
                                                   .Include(b => b.Campaigns) 
                                                        .ThenInclude(mu => mu.ModifiedByUser)
                                                   .FirstOrDefaultAsync<Advertiser>(a => a.UniqID == advertiserUniqID);

            var viewModel = new List<CampaignViewModel>();
            foreach (var campaign in result.Campaigns)
            {
                var vm = new CampaignViewModel();
                vm.From(campaign);
                viewModel.Add(vm);
            }
            
            return viewModel;

        }


    }
}
