﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using VINCO.BLL.DAL;
using VINCO.Common;
using VINCO.Model;

namespace VINCO.BLL
{
    public class LoginBaseService : BaseService
    {
        private readonly VincoDbContext _context; 
        public LoginBaseService(VincoDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<int> Login(string email, string password, Model.Enums.UserType userType)
        {
            var user = await _context.Users.FirstOrDefaultAsync<User>(u => u.Email == email &&
                                                                     u.UserTypeID == (int)userType);

            if (user == null)
            {
                throw new VincoUnauthorizedException(ValidationMessages.LOGIN_INVALID_CREDENTIALS);
            }

            user.LastAttemptedLogin = DateTime.UtcNow;
            await _context.SaveChangesAsync();

            var hashedPassword = Encryption.GetHash(password, user.PasswordSalt);

            if (hashedPassword == user.Password)
            {
                user.LastSuccessfulLogin = DateTime.UtcNow;
                await _context.SaveChangesAsync();
                return (int)user.ID;
            }
            else
            {
                throw new VincoUnauthorizedException(ValidationMessages.LOGIN_INVALID_CREDENTIALS);
            }
        }


        protected string GenerateJwtToken(User user, string uniqID)
        {
            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Globals.TokenSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("Email", user.Email),
                    new Claim("UserType", user.UserTypeID.ToString()),
                    new Claim("UniqID", uniqID),
                    new Claim("ID", user.ID.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(Globals.TokenExpirationDays),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }



    }
}
