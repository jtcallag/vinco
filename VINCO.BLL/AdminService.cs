﻿using System;
using System.Collections.Generic;
using System.Text;
using VINCO.BLL.Interfaces;
using VINCO.BLL.DAL;
using VINCO.Common;
using VINCO.Model;
using VINCO.Model.ViewModel;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace VINCO.BLL
{
    public class AdminService : LoginBaseService, IAdminService
    {
        
        public AdminService(VincoDbContext context) : base(context)
        {
        
        }

        public async Task<bool> ForgotPassword(string email)
        {
            return true;
        }

        public async Task<AdminUserViewModel> Register(RegistrationViewModel model)
        {
            //set the UserType as Admin.  will need for ValidationPre.
            model.UserTypeID = (int)VINCO.Model.Enums.UserType.VincoAdmin;

            ValidatePre(model);

            var user = model.ToUser();

            //get a random salt and hash the password for the user
            user.PasswordSalt = Encryption.GenerateSalt();
            user.Password = Encryption.GetHash(model.Password, user.PasswordSalt);
            user.IsRegistrationComplete = false;
            user.IsActive = true;

            
            user.UniqID = Guid.NewGuid().ToString();
            
            Validate(new List<object>() { user });

            Decorate(user, Globals.SystemUserID, true);
            
            _context.Users.Add(user);

            await _context.SaveChangesAsync();

            //insert UserVerification here - send email to confirm enrollment.s


            var vm = new AdminUserViewModel();
            vm.From(user);

            return vm;

        }


        public async Task<AdminUserViewModel> Login(LoginViewModel model)
        {
            var result = await base.Login(model.Email, model.Password, Model.Enums.UserType.VincoAdmin);

            var admin = _context.Users.Include(a => a.CreatedByUser)
                                                 .Include(a => a.ModifiedByUser)
                                                 .FirstOrDefault(v => v.ID == result);

            var vm = new AdminUserViewModel();
            vm.From(admin);

            //set the token to the viewmodel.
            vm.AuthenticationToken = GenerateJwtToken(admin, admin.UniqID);

            return vm;
        }

        public async Task<bool> UpdatePassword(string email, string newPassword, string userUniqID)
        {
            
            return true;
        }

        public async Task<List<ApplicationSettingViewModel>> GetApplicationSettings()
        {
            var result = new List<ApplicationSettingViewModel>();

            await Task.Run(() =>
            {
                var settings = _context.ApplicationSettings.Include(a => a.CreatedByUser)
                                                     .Include(a => a.ModifiedByUser);
                
                foreach (var setting in settings)
                {
                    var vm = new ApplicationSettingViewModel();
                    vm.From(setting);
                    result.Add(vm);
                }

            });

            return result;
        }
        public async Task<bool> UpdateApplicationSetting(ApplicationSettingViewModel model)
        {
            var newSetting = model.To();

            var origSetting = _context.ApplicationSettings.FirstOrDefault(v => v.Name == newSetting.Name);
            
            if (origSetting == null)
            {
                return false;
            }

            origSetting.Value = newSetting.Value;

            Validate(new List<object>() { newSetting });

            Decorate(origSetting, _context.CurrentUser.ID, false);

            await _context.SaveChangesAsync();

            return true;

        }

    }
}
