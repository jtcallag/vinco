﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VINCO.BLL.Interfaces;
using VINCO.BLL.DAL;
using VINCO.BLL.Storage;
using System.IO;
using VINCO.Model.ViewModel;
using VINCO.Model;
using VINCO.Model.Enums;
using VINCO.Common;

namespace VINCO.BLL
{
    public class MediaService : BaseService, IMediaService
    {
        private readonly IAdvertiserService _advertiserService;
        private readonly IStorageService _storageService;
        public MediaService(VincoDbContext context, IAdvertiserService advertiserService, IStorageService storageService) : base(context)
        {
            _advertiserService = advertiserService;
            _storageService = storageService;
        }
        public async Task<bool> SaveLogo(string uniqID, Stream logoFile)
        {
            return await Task.Run(async () =>
            {
                var result = true;
                var settings = await _context.GetAppSettings();
                var request = new MediaRequest();
                request.S3Key = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3ApiKey)).Value;
                request.S3Secret = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3ApiSecret)).Value;
                request.S3Bucket = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3BucketName)).Value;
                request.S3Url = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3Url)).Value;

                var imageUtil = new ImageUtilities();
                var imageSet = await imageUtil.GetImageSet(logoFile, ImageType.AdvertiserLogo);

                foreach(var item in imageSet)
                {
                    request.ImageStream = item.Value;
                    request.FileName = $"{uniqID}.logo.{Enum.GetName(typeof(ImageWidth), item.Key)}.jpg";
                    await _storageService.Upload(request);
                }

                return true;
            });
        }

        

        public async Task<bool> SaveAvatar(string uniqID, Stream logoFile)
        {
            return await Task.Run(async () =>
            {
                var result = true;
                var settings = await _context.GetAppSettings();
                var request = new MediaRequest();
                request.S3Key = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3ApiKey)).Value;
                request.S3Secret = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3ApiSecret)).Value;
                request.S3Bucket = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3BucketName)).Value;
                request.S3Url = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3Url)).Value;

                var imageUtil = new ImageUtilities();
                var imageSet = await imageUtil.GetImageSet(logoFile, ImageType.Avatar);

                foreach (var item in imageSet)
                {
                    request.ImageStream = item.Value;
                    request.FileName = $"{uniqID}.avatar.{Enum.GetName(typeof(ImageWidth), item.Key)}.jpg";
                    await _storageService.Upload(request);
                }

                return true;
            });
        }
        public async Task<bool> SaveImage(string uniqID, string imageNum, Stream imageFile, ImageType imageType)
        {
            return await Task.Run(async () =>
            {
                var result = true;
                var settings = await _context.GetAppSettings();
                var request = new MediaRequest();
                request.S3Key = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3ApiKey)).Value;
                request.S3Secret = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3ApiSecret)).Value;
                request.S3Bucket = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3BucketName)).Value;
                request.S3Url = settings.FirstOrDefault(n => n.Name == Enum.GetName(typeof(ApplicationSettingKey), ApplicationSettingKey.S3Url)).Value;

                var imageUtil = new ImageUtilities();
                var imageSet = await imageUtil.GetImageSet(imageFile, imageType);

                foreach (var item in imageSet)
                {
                    request.ImageStream = item.Value;
                    request.FileName = $"{uniqID}.image.{imageNum}.{Enum.GetName(typeof(ImageWidth), item.Key)}.jpg";
                    await _storageService.Upload(request);
                }

                return true;
            });

        }


    }
}
