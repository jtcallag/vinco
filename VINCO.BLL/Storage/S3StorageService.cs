﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using VINCO.Model.ViewModel;
using VINCO.Model;
using Amazon.S3;
using Amazon.S3.Transfer;

namespace VINCO.BLL.Storage
{
    public class S3StorageService : IStorageService
    {
        public async Task<bool> Upload(MediaRequest request)
        {
            await Task.Run(() =>
            {
                var s3ClientConfig = new AmazonS3Config
                {  
                    ServiceURL = request.S3Url
                };
                var credentials = new Amazon.Runtime.BasicAWSCredentials(request.S3Key, request.S3Secret);
                var s3Client = new AmazonS3Client(credentials, s3ClientConfig);
                
                
                    var transferUtility = new TransferUtility(s3Client);
                    var transferUtilityRequest = new TransferUtilityUploadRequest
                    {
                        BucketName = request.S3Bucket,
                        InputStream = request.ImageStream,
                        StorageClass = S3StorageClass.Standard,
                        PartSize = 6291456, // 6 MB
                        Key = request.FileName,
                        CannedACL = S3CannedACL.PublicRead
                    };
                    transferUtility.Upload(transferUtilityRequest);
                
            });

            return true;
        }
        public async Task<bool> Remove(MediaRequest request)
        {
            await Task.Run(() =>
            {

            });
            
            return true;

        }
    }
}
