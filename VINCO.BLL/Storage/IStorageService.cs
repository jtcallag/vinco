﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using VINCO.Model.ViewModel;
using VINCO.Model;

namespace VINCO.BLL.Storage
{
    public interface IStorageService
    {
        Task<bool> Upload(MediaRequest request);
        Task<bool> Remove(MediaRequest request);
    }
}
