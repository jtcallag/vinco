﻿using MySqlX.XDevAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using VINCO.BLL.DAL;
using VINCO.Common;
using VINCO.Model.Interfaces;
using VINCO.Model;
using VINCO.Model.ViewModel;
using Enums = VINCO.Model.Enums;

namespace VINCO.BLL
{
    public abstract class BaseService
    {
        protected readonly VincoDbContext _context;
        public BaseService(VincoDbContext context)
        {
            _context = context;
        }

        public void SetCurrentUser(CurrentUser currentUser)
        {
            _context.CurrentUser = currentUser;
        }

        protected void Authorize(params Enums.UserType[] userType)
        {
            if (_context.CurrentUser.UserType != Model.Enums.UserType.VincoAdmin &&
                !userType.Any(ut => ut == _context.CurrentUser.UserType))
            {
                throw new VincoUnauthorizedException("You are not authorized to perform this operation.");
            }
        }

        public void Decorate(IVincoModel model, int currentUserId, bool isNew = false)
        {
            if (isNew)
            {
                model.CreatedBy = currentUserId;
                model.CreatedDate = DateTime.UtcNow;
            }

            model.ModifiedBy = currentUserId;
            model.ModifiedDate = DateTime.UtcNow;
        }

        public void ValidatePre(object viewModel)
        {
            var ve = new List<string>();

            #region RegistrationViewModel

            if (viewModel is RegistrationViewModel)
            {
                var vm = (RegistrationViewModel)viewModel;
                var emailTester = new System.ComponentModel.DataAnnotations.EmailAddressAttribute();
                //.IsValid(input)
                //if (RegexUtilities.IsValidEmail(vm.Email))
                if (!emailTester.IsValid(vm.Email))
                {
                    ve.Add(ValidationMessages.INVALID_EMAIL_FORMAT);
                }
                if (vm.Password == null ||
                    vm.Password?.Length < 8 ||
                    vm.Password.Count(p => char.IsDigit(p)) == 0 ||
                    vm.Password.Count(p => char.IsLetter(p)) == 0 ||
                    vm.Password.Count(p => !char.IsLetterOrDigit(p)) == 0)
                {
                    ve.Add(ValidationMessages.INVALID_PASSWORD);
                }
                
               if (_context.Users.Any(u => u.Email == vm.Email && u.UserTypeID == vm.UserTypeID))
               {
                    ve.Add(ValidationMessages.REGISTRATION_EMAIL_EXISTS);
               }
            }
            #endregion
            #region LoginViewModel
            else if (viewModel is LoginViewModel)
            {

            }
            #endregion

            if (ve.Count > 0)
            {
                throw new VincoValidationException(ve);
            }

        }



        public void Validate(object model)
        {
            var schemaResult = ValidateSchema(model);
            var customResult = ValidateCustom(model);
            if (!schemaResult.IsValid || !customResult.IsValid)
            {
                var errors = new List<string>();
                errors.AddRange(schemaResult.ValidationErrors);
                errors.AddRange(customResult.ValidationErrors);
                throw new VincoValidationException(errors);
            }
        }

        public void Validate(List<object> models)
        {
            var ve = new List<string>();
            var schemaResults = new List<VincoValidationResult>();
            var customResults = new List<VincoValidationResult>();

            foreach (var model in models)
            {
                var schemaResult = ValidateSchema(model);
                schemaResults.Add(schemaResult);
                var customResult = ValidateCustom(model);
                customResults.Add(customResult);
            }

            if (schemaResults.Any(sr => !sr.IsValid) || customResults.Any(cr => !cr.IsValid))
            {
                foreach (var sr in schemaResults)
                {
                    ve.AddRange(sr.ValidationErrors);
                }
                foreach (var cr in customResults)
                {
                    ve.AddRange(cr.ValidationErrors);
                }
            }

            if (ve.Count > 0)
            {
                throw new VincoValidationException(ve);
            }

        }

        public VincoValidationResult ValidateSchema(object model)
        {
            var vincoResult = new VincoValidationResult();
            
            var v = new ValidationContext(model);
            var vr = new List<ValidationResult>();
            var result = Validator.TryValidateObject(model, v, vr, true);
            if (!result)
            {
                vincoResult.IsValid = false;
                
                foreach (var e in vr)
                {
                    vincoResult.ValidationErrors.Add(e.ErrorMessage);
                }
            }

            return vincoResult;
        }

        public VincoValidationResult ValidateCustom(object model)
        {
            var vincoResult = new VincoValidationResult();
            
            
            if (model is User)
            {

            }
            else if (model is Advertiser)
            {

            }
            else if (model is Influencer)
            {

            }
            else if (model is Campaign)
            {
                var campaign = (Campaign)model;
                if (campaign.StartDate >= campaign.EndDate)
                {
                    vincoResult.IsValid = false;
                    vincoResult.ValidationErrors.Add(ValidationMessages.CAMPAIGN_START_DATE_AFTER_END_DATE);
                }

            }
            else if (model is CampaignInfluencer)
            {
                var campaignInfluencer = (CampaignInfluencer)model;
                if (campaignInfluencer.ID == null) //only validate on Adds here
                {
                    if(_context.CampaignInfluencers.Any(ci => ci.InfluencerID == campaignInfluencer.InfluencerID && ci.CampaignID == campaignInfluencer.CampaignID))
                    {
                        vincoResult.IsValid = false;
                        vincoResult.ValidationErrors.Add(ValidationMessages.INFLUENCER_ALREADY_ADDED_TO_CAMPAIGN);
                    }
                }
            }
            //etc....

            return vincoResult;
        }



    }
}
