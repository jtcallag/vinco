﻿using System;
using System.Collections.Generic;
using System.Text;
using VINCO.BLL.Interfaces;
using VINCO.BLL.DAL;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace VINCO.BLL
{
    public class ClickService : BaseService, IClickService
    {
        public ClickService(VincoDbContext context) : base(context)
        {

        }

        public async Task<string> SaveClick(string campaignInfluencerUniqID)
        {
            var ci = await _context.CampaignInfluencers.Include(c => c.Campaign)
                                                       .FirstOrDefaultAsync(ci => ci.UniqID == campaignInfluencerUniqID);
            return $"{ci.Campaign.ProductUrl}{(ci.Campaign.ProductUrl.Contains('?') ? $"&vinco={ci.UniqID}" : $"?vinco={ci.UniqID}")}";
        }
    }
}
