﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Text;
using VINCO.Common;
using VINCO.Model;


namespace VINCO.BLL.DAL
{
    public static class MigrationSeeding
    {
        public static void RefreshSeeding(this MigrationBuilder builder)
        {
            //lookup tables

            //userTypes
            foreach (var userType in UserTypeSeeds())
            {
                builder.Sql($"INSERT INTO UserTypes (ID, Name, IsActive) " +
                            $"VALUES({userType.ID}, '{userType.Name}', {userType.IsActive}) " +
                            $"ON DUPLICATE KEY UPDATE " +
                            $"ID = {userType.ID}, Name = '{userType.Name}', IsActive = {userType.IsActive};");
            }


            //paymentMethods
            foreach (var paymentMethod in PaymentMethodSeeds())
            {
                builder.Sql($"INSERT INTO PaymentMethods (ID, Name, IsActive) " +
                            $"VALUES({paymentMethod.ID}, '{paymentMethod.Name}', {paymentMethod.IsActive}) " +
                            $"ON DUPLICATE KEY UPDATE " +
                            $"ID = {paymentMethod.ID}, Name = '{paymentMethod.Name}', IsActive = {paymentMethod.IsActive};");
            }

            //paymentTransactionStatuses
            foreach (var paymentTransactionStatus in PaymentTransactionStatusSeeds())
            {
                builder.Sql($"INSERT INTO PaymentTransactionStatuses (ID, Name, IsActive) " +
                            $"VALUES({paymentTransactionStatus.ID}, '{paymentTransactionStatus.Name}', {paymentTransactionStatus.IsActive}) " +
                            $"ON DUPLICATE KEY UPDATE " +
                            $"ID = {paymentTransactionStatus.ID}, Name = '{paymentTransactionStatus.Name}', IsActive = {paymentTransactionStatus.IsActive};");
            }

            //entity tables - the order upon which these are updated matters.  you have been warned. :)

            //users
            foreach (var user in UserSeeds())
            {
                builder.Sql($"INSERT INTO Users (ID, UniqID, FirstName, LastName, Email, IsActive, Password, PasswordSalt, IsRegistrationComplete, UserTypeID, CreatedBy, CreatedDate, ModifiedBy, ModifiedDate) " +
                            $"VALUES({user.ID}, '{user.UniqID}', '{user.FirstName}', '{user.LastName}', '{user.Email}', {user.IsActive}, '{user.Password}', '{user.PasswordSalt}', {user.IsRegistrationComplete}, {user.UserTypeID}, {Globals.SystemUserID}, '{DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss")}', {Globals.SystemUserID}, '{DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss")}') " +
                            $"ON DUPLICATE KEY UPDATE " +
                            $"ID = {user.ID}, UniqID = '{user.UniqID}', FirstName = '{user.FirstName}', LastName = '{user.LastName}', Email = '{user.Email}', IsActive = {user.IsActive}, Password = '{user.Password}', PasswordSalt = '{user.PasswordSalt}', IsRegistrationComplete = {user.IsRegistrationComplete}, UserTypeID = {user.UserTypeID}, ModifiedBy = {Globals.SystemUserID}, ModifiedDate = '{DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss")}';");
            }


        }

        private static List<User> UserSeeds()
        {
            return new List<User>()
            {
                new User {ID = -1, UniqID = "0D6D6033-F6CB-4C21-9E2D-A79BD98E2E79", FirstName = "System", LastName = "Internal", Email="system@vinco.com", IsActive = true, Password = string.Empty, PasswordSalt = string.Empty, IsRegistrationComplete = false, MobilePhone = string.Empty, UserTypeID = (int)VINCO.Model.Enums.UserType.VincoAdmin }
            };
        }

        private static List<UserType> UserTypeSeeds()
        {
            return new List<UserType>()
            {
                new UserType {ID = 1, Name = "VincoAdmin", IsActive = true },
                new UserType {ID = 2, Name = "Advertiser", IsActive = true },
                new UserType {ID = 3, Name = "Influencer", IsActive = true }
            };
        }

        private static List<PaymentMethod> PaymentMethodSeeds()
        {
            return new List<PaymentMethod>()
            {
                new PaymentMethod {ID = 1, Name = "PayPal", IsActive = true},
                new PaymentMethod {ID = 2, Name = "Venmo", IsActive = true},
            };
        }

        private static List<PaymentTransactionStatus> PaymentTransactionStatusSeeds()
        {
            return new List<PaymentTransactionStatus>()
            {
                new PaymentTransactionStatus {ID = 1, Name = "Pending", IsActive = true },
                new PaymentTransactionStatus {ID = 2, Name = "Processing", IsActive = true },
                new PaymentTransactionStatus {ID = 3, Name = "Completed", IsActive = true },
                new PaymentTransactionStatus {ID = 4, Name = "Error", IsActive = true }
            };
        }

    }
}
