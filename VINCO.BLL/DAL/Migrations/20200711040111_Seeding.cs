﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VINCO.BLL.DAL.Migrations
{
    public partial class Seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            MigrationSeeding.RefreshSeeding(migrationBuilder);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
