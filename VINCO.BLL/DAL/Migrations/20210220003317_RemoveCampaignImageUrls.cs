﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VINCO.BLL.DAL.Migrations
{
    public partial class RemoveCampaignImageUrls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductLargeImageUrl",
                table: "Campaigns");

            migrationBuilder.DropColumn(
                name: "ProductMediumImageUrl",
                table: "Campaigns");

            migrationBuilder.DropColumn(
                name: "ProductSmallImageUrl",
                table: "Campaigns");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProductLargeImageUrl",
                table: "Campaigns",
                type: "varchar(500)",
                maxLength: 500,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ProductMediumImageUrl",
                table: "Campaigns",
                type: "varchar(500)",
                maxLength: 500,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ProductSmallImageUrl",
                table: "Campaigns",
                type: "varchar(500)",
                maxLength: 500,
                nullable: false,
                defaultValue: "");
        }
    }
}
