﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace VINCO.BLL.DAL.Migrations
{
    public partial class AddedAdvertiserLogoFileName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LogoFileName",
                table: "Advertisers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LogoFileName",
                table: "Advertisers");
        }
    }
}
