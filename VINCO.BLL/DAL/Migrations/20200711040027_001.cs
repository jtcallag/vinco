﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using MySql.Data.EntityFrameworkCore.Metadata;

namespace VINCO.BLL.DAL.Migrations
{
    public partial class _001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PaymentMethods",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentMethods", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "PaymentTransactionStatuses",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 20, nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentTransactionStatuses", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "UserTypes",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTypes", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    UniqID = table.Column<string>(maxLength: 255, nullable: false),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: false),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    MobilePhone = table.Column<string>(nullable: true),
                    Password = table.Column<string>(maxLength: 255, nullable: false),
                    PasswordSalt = table.Column<string>(maxLength: 255, nullable: false),
                    IsRegistrationComplete = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    LastAttemptedLogin = table.Column<DateTime>(nullable: true),
                    LastSuccessfulLogin = table.Column<DateTime>(nullable: true),
                    UserTypeID = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Users_Users_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Users_Users_ModifiedBy",
                        column: x => x.ModifiedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Users_UserTypes_UserTypeID",
                        column: x => x.UserTypeID,
                        principalTable: "UserTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Advertisers",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    UniqID = table.Column<string>(maxLength: 50, nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    NameAlias = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    StreetAddress1 = table.Column<string>(maxLength: 100, nullable: false),
                    StreetAddress2 = table.Column<string>(maxLength: 100, nullable: true),
                    City = table.Column<string>(maxLength: 100, nullable: false),
                    State = table.Column<string>(maxLength: 10, nullable: true),
                    PostalCode = table.Column<string>(maxLength: 20, nullable: false),
                    IsEnabled = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Advertisers", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Advertisers_Users_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Advertisers_Users_ModifiedBy",
                        column: x => x.ModifiedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationSettings",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Value = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationSettings", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ApplicationSettings_Users_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplicationSettings_Users_ModifiedBy",
                        column: x => x.ModifiedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Influencers",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    UniqID = table.Column<string>(maxLength: 255, nullable: false),
                    NameAlias = table.Column<string>(maxLength: 50, nullable: true),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Influencers", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Influencers_Users_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Influencers_Users_ModifiedBy",
                        column: x => x.ModifiedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Influencers_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PaymentProfiles",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    AccountCode = table.Column<string>(nullable: true),
                    EffectiveStartDate = table.Column<DateTime>(nullable: false),
                    EffectiveEndDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    PaymentMethodId = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentProfiles", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PaymentProfiles_Users_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentProfiles_Users_ModifiedBy",
                        column: x => x.ModifiedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentProfiles_PaymentMethods_PaymentMethodId",
                        column: x => x.PaymentMethodId,
                        principalTable: "PaymentMethods",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentProfiles_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserVerifications",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Passcode = table.Column<string>(maxLength: 20, nullable: false),
                    ExpiryDate = table.Column<DateTime>(nullable: false),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserVerifications", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserVerifications_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdvertiserUsers",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    UniqID = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    AdvertiserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdvertiserUsers", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AdvertiserUsers_Advertisers_AdvertiserID",
                        column: x => x.AdvertiserID,
                        principalTable: "Advertisers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AdvertiserUsers_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Campaigns",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    UniqID = table.Column<string>(maxLength: 50, nullable: false),
                    CampaignName = table.Column<string>(maxLength: 50, nullable: false),
                    ProductName = table.Column<string>(maxLength: 50, nullable: false),
                    ProductDescription = table.Column<string>(maxLength: 1000, nullable: false),
                    ProductUrl = table.Column<string>(maxLength: 500, nullable: false),
                    ProductSmallImageUrl = table.Column<string>(maxLength: 500, nullable: false),
                    ProductMediumImageUrl = table.Column<string>(maxLength: 500, nullable: false),
                    ProductLargeImageUrl = table.Column<string>(maxLength: 500, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    IsEnabled = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<int>(nullable: false),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    AdvertiserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Campaigns", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Campaigns_Advertisers_AdvertiserID",
                        column: x => x.AdvertiserID,
                        principalTable: "Advertisers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Campaigns_Users_CreatedBy",
                        column: x => x.CreatedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Campaigns_Users_ModifiedBy",
                        column: x => x.ModifiedBy,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PaymentTransactions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    UniqID = table.Column<string>(nullable: true),
                    TransactionCode = table.Column<string>(nullable: true),
                    TransactionAmount = table.Column<decimal>(nullable: true),
                    ResponseMessage = table.Column<string>(maxLength: 500, nullable: true),
                    PaymentTransactionStatusID = table.Column<int>(nullable: false),
                    PaymentProfileID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentTransactions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_PaymentTransactions_PaymentProfiles_PaymentProfileID",
                        column: x => x.PaymentProfileID,
                        principalTable: "PaymentProfiles",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentTransactions_PaymentTransactionStatuses_PaymentTransa~",
                        column: x => x.PaymentTransactionStatusID,
                        principalTable: "PaymentTransactionStatuses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CampaignInfluencers",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    UniqID = table.Column<string>(nullable: true),
                    CampaignID = table.Column<int>(nullable: false),
                    InfluencerID = table.Column<int>(nullable: false),
                    CampaignInfluencerClickUrl = table.Column<string>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CampaignInfluencers", x => x.ID);
                    table.ForeignKey(
                        name: "FK_CampaignInfluencers_Campaigns_CampaignID",
                        column: x => x.CampaignID,
                        principalTable: "Campaigns",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CampaignInfluencers_Influencers_InfluencerID",
                        column: x => x.InfluencerID,
                        principalTable: "Influencers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClickActions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    CampaignUniqID = table.Column<string>(maxLength: 50, nullable: false),
                    InfluencerUniqID = table.Column<string>(maxLength: 50, nullable: false),
                    ClickDateTime = table.Column<DateTime>(nullable: false),
                    ResultedInPurchase = table.Column<bool>(nullable: false),
                    OrigIPAddress = table.Column<string>(maxLength: 20, nullable: true),
                    HttpReferrer = table.Column<string>(maxLength: 50, nullable: true),
                    CampaignInfluencerID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClickActions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ClickActions_CampaignInfluencers_CampaignInfluencerID",
                        column: x => x.CampaignInfluencerID,
                        principalTable: "CampaignInfluencers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Advertisers_CreatedBy",
                table: "Advertisers",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Advertisers_ModifiedBy",
                table: "Advertisers",
                column: "ModifiedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Advertisers_UniqID",
                table: "Advertisers",
                column: "UniqID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AdvertiserUsers_AdvertiserID",
                table: "AdvertiserUsers",
                column: "AdvertiserID");

            migrationBuilder.CreateIndex(
                name: "IX_AdvertiserUsers_UserID",
                table: "AdvertiserUsers",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationSettings_CreatedBy",
                table: "ApplicationSettings",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationSettings_ModifiedBy",
                table: "ApplicationSettings",
                column: "ModifiedBy");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignInfluencers_CampaignID",
                table: "CampaignInfluencers",
                column: "CampaignID");

            migrationBuilder.CreateIndex(
                name: "IX_CampaignInfluencers_InfluencerID",
                table: "CampaignInfluencers",
                column: "InfluencerID");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_AdvertiserID",
                table: "Campaigns",
                column: "AdvertiserID");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_CreatedBy",
                table: "Campaigns",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_ModifiedBy",
                table: "Campaigns",
                column: "ModifiedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Campaigns_UniqID",
                table: "Campaigns",
                column: "UniqID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ClickActions_CampaignInfluencerID",
                table: "ClickActions",
                column: "CampaignInfluencerID");

            migrationBuilder.CreateIndex(
                name: "IX_Influencers_CreatedBy",
                table: "Influencers",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Influencers_ModifiedBy",
                table: "Influencers",
                column: "ModifiedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Influencers_UniqID",
                table: "Influencers",
                column: "UniqID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Influencers_UserID",
                table: "Influencers",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentProfiles_CreatedBy",
                table: "PaymentProfiles",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentProfiles_ModifiedBy",
                table: "PaymentProfiles",
                column: "ModifiedBy");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentProfiles_PaymentMethodId",
                table: "PaymentProfiles",
                column: "PaymentMethodId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentProfiles_UserID",
                table: "PaymentProfiles",
                column: "UserID");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTransactions_PaymentProfileID",
                table: "PaymentTransactions",
                column: "PaymentProfileID");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentTransactions_PaymentTransactionStatusID",
                table: "PaymentTransactions",
                column: "PaymentTransactionStatusID");

            migrationBuilder.CreateIndex(
                name: "IX_Users_CreatedBy",
                table: "Users",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Users_ModifiedBy",
                table: "Users",
                column: "ModifiedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Users_UniqID",
                table: "Users",
                column: "UniqID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_UserTypeID",
                table: "Users",
                column: "UserTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_UserVerifications_UserID",
                table: "UserVerifications",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AdvertiserUsers");

            migrationBuilder.DropTable(
                name: "ApplicationSettings");

            migrationBuilder.DropTable(
                name: "ClickActions");

            migrationBuilder.DropTable(
                name: "PaymentTransactions");

            migrationBuilder.DropTable(
                name: "UserVerifications");

            migrationBuilder.DropTable(
                name: "CampaignInfluencers");

            migrationBuilder.DropTable(
                name: "PaymentProfiles");

            migrationBuilder.DropTable(
                name: "PaymentTransactionStatuses");

            migrationBuilder.DropTable(
                name: "Campaigns");

            migrationBuilder.DropTable(
                name: "Influencers");

            migrationBuilder.DropTable(
                name: "PaymentMethods");

            migrationBuilder.DropTable(
                name: "Advertisers");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "UserTypes");
        }
    }
}
