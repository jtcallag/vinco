﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VINCO.Model;

namespace VINCO.BLL.DAL
{
    public class VincoDbContext : DbContext
    {
        public VincoDbContext(DbContextOptions<VincoDbContext> options)
            : base(options) 
        { 
        }
        
        //use this when scaffolding the database with an EF migration
        //public VincoDbContext()
        //{

        //}

        //lookup entities
        public DbSet<UserType> UserTypes { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<PaymentTransactionStatus> PaymentTransactionStatuses { get; set; }

        //fact entities
        public DbSet<User> Users { get; set; }
        public DbSet<Influencer> Influencers { get; set; }
        public DbSet<Advertiser> Advertisers { get; set; }
        public DbSet<ApplicationSetting> ApplicationSettings { get; set; }
        public DbSet<PaymentProfile> PaymentProfiles { get; set; }
        public DbSet<PaymentTransaction> PaymentTransactions { get; set; }
        public DbSet<UserVerification> UserVerifications { get; set; }
        public DbSet<Campaign> Campaigns { get; set; }
        public DbSet<CampaignInfluencer> CampaignInfluencers { get; set; }
        public DbSet<ClickAction> ClickActions { get; set; }
        public DbSet<AdvertiserUser> AdvertiserUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseMySQL(@"server=157.245.227.211;port=3306;database=VINCO;uid=mamn;password=m@mn2020!");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>()
                .HasIndex(u => u.UniqID)
                .IsUnique();

            builder.Entity<Influencer>()
                .HasIndex(u => u.UniqID)
                .IsUnique();

            builder.Entity<Advertiser>()
                .HasIndex(u => u.UniqID)
                .IsUnique();

            builder.Entity<Campaign>()
                .HasIndex(u => u.UniqID)
                .IsUnique();

            builder.Entity<CampaignInfluencer>()
                        .HasOne(c => c.Campaign)
                        .WithMany(i => i.CampaignInfluencers)
                         .HasForeignKey(bc => bc.CampaignID);

            builder.Entity<CampaignInfluencer>()
                .HasOne(i => i.Influencer)
                .WithMany(c => c.CampaignInfluencers)
                .HasForeignKey(bc => bc.InfluencerID);

            builder.Entity<AdvertiserUser>()
                        .HasOne(c => c.Advertiser)
                        .WithMany(i => i.AdvertiserUsers)
                         .HasForeignKey(bc => bc.AdvertiserID);


        }

        #region VINCO specific stuff

        private List<ApplicationSetting> _AppSettings;
        public async Task<List<ApplicationSetting>> GetAppSettings()
        {
            if (_AppSettings == null)
            {
            _AppSettings = await this.ApplicationSettings.ToListAsync<ApplicationSetting>();
            }
            return _AppSettings;
            
        }

        public CurrentUser CurrentUser { get; set; }
        
        #endregion

    }

    public class CurrentUser
    {
        public string Email { get; set; }
        public Model.Enums.UserType UserType { get; set; }
        public string UniqID { get; set; }

        public int ID { get; set; }
    }


}