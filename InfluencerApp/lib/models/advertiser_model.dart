class AdvertiserModel {
  String uniqID;
  String name;
  String nameAlias;
  String description;
  String streetAddress1;
  String streetAddress2;
  String city;
  String state;
  String postalCode;
  bool isEnabled;
  DateTime createdDate;
  String createdByName;
  DateTime modifiedDate;
  String modifiedByName;
  String logoFileName;
}
