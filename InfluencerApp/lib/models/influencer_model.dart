class InfluencerModel {
  String uniqID;
  String nameAlias;
  DateTime dateOfBirth;
  String firstName;
  String lastName;
  String email;
  String mobilePhone;
  bool isRegistrationComplete;
  bool isActive;
  DateTime lastAttemptedLogin;
  DateTime lastSuccessfulLogin;

  String authenticationToken;

  InfluencerModel(
      {this.uniqID,
      this.nameAlias,
      this.dateOfBirth,
      this.firstName,
      this.lastName,
      this.email,
      this.mobilePhone,
      this.isRegistrationComplete,
      this.isActive,
      this.lastAttemptedLogin,
      this.lastSuccessfulLogin,
      this.authenticationToken});

  factory InfluencerModel.fromJson(Map<String, dynamic> parsedJson) {
    return InfluencerModel(
        uniqID: parsedJson['uniqID'].toString(),
        nameAlias: parsedJson['nameAlias'].toString(),
        dateOfBirth: DateTime.tryParse(['dateOfBirth'].toString()),
        firstName: parsedJson['firstName'].toString(),
        lastName: parsedJson['lastName'].toString(),
        email: parsedJson['email'].toString(),
        mobilePhone: parsedJson['mobilePhone'].toString(),
        isRegistrationComplete:
            parsedJson['isRegistrationComplete'].toString().toLowerCase() ==
                'true',
        isActive: parsedJson['isActive'].toString().toLowerCase() == 'true',
        lastAttemptedLogin:
            DateTime.tryParse(['lastAttemptedLogin'].toString()),
        lastSuccessfulLogin:
            DateTime.tryParse(['lastSuccessfulLogin'].toString()),
        authenticationToken: parsedJson['authenticationToken'].toString());
  }
}
