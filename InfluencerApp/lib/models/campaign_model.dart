class CampaignView {
  String uniqID;
  String advertiserUniqID;
  String campaignName;
  String productName;
  String productDescription;
  String productURL;
  String productSmallImageUrl;
  String productMediumImageUrl;
  String productLargeImageUrl;
  String startDate;
  String endDate;
  bool isEnabled;
  String createdByName;
  String createdDate;
  String modifiedByName;
  String modifiedDate;
}
