class RequestResult {
  bool isSuccessful;
  bool hasValidationErrors;
  String message;
  dynamic payload;
  List<String> validationErrors;
}
