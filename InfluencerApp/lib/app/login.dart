import 'package:InfluencerApp/services/userservice.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';

class Login extends StatelessWidget {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("VINCO Influencer"),
        elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(64.0),
        child: Column(children: [
          TextField(
            controller: usernameController,
            decoration: InputDecoration(
              hintText: 'Username',
              contentPadding: const EdgeInsets.all(16.0),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red),
                borderRadius: BorderRadius.all(Radius.circular(30)),
              ),
            ),
          ),
          SizedBox(height: 30),
          TextField(
            controller: passwordController,
            decoration: InputDecoration(
              hintText: 'Password',
              contentPadding: const EdgeInsets.all(16.0),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red),
                borderRadius: BorderRadius.all(Radius.circular(30)),
              ),
            ),
          ),
          SizedBox(height: 48),
          RaisedButton(
            child: Text("Enter"),
            onPressed: () async {
              try {
                await Provider.of<UserService>(context).login(
                    usernameController.value.toString(),
                    passwordController.value.toString());

                Scaffold.of(context).showSnackBar(new SnackBar(
                    content: Text('logged in'), backgroundColor: Colors.green));
              } catch (e) {
                Scaffold.of(context).showSnackBar(new SnackBar(
                    content: Text(e.toString()), backgroundColor: Colors.red));
              }
            },
            color: Colors.red[400],
            textColor: Colors.blueGrey,
            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
            splashColor: Colors.grey,
          )
        ]),
      ),
    );
  }
}
