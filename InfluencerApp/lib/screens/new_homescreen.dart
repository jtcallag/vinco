import 'package:InfluencerApp/billing/billing.dart';
import 'package:InfluencerApp/billing/pay_stubs.dart';
import 'package:InfluencerApp/billing/paystubs2.dart';
import 'package:InfluencerApp/billing/redeem_gifts.dart';
import 'package:InfluencerApp/campaigns/active_campaign.dart';
import 'package:InfluencerApp/campaigns/campaign.dart';
import 'package:InfluencerApp/campaigns/campaign_search.dart';
import 'package:InfluencerApp/campaigns/new_campaign.dart';
import 'package:InfluencerApp/screens/instructions.dart';
import 'package:InfluencerApp/screens/sign_in.dart';
import 'package:InfluencerApp/stats/stats.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


//import 'package:flutter/foundation.dart';
//import 'package:vinco/home_screen.dart';
//import './forgot_password.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    final urlImage = 'https://wallpaperaccess.com/full/882.jpg';
    final urlImage2 = 'https://cdn.shopify.com/s/files/1/0463/8487/3628/files/0F0221B2-DED5-41D2-9AF0-17CB4ECC79CB_300x300.jpg?v=1606678730';
    final urlImage3 = 'https://cdn.shopify.com/s/files/1/0510/3498/0531/products/RetroNationTrucker2_1024x1024.jpg?v=1625079668';
    return Scaffold(
    
    resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,

      body: Container(
        decoration: BoxDecoration(
          //image: DecorationImage(
            //fit: BoxFit.cover,
            //image: NetworkImage(urlImage)
            color: Colors.white
        ),
        margin: EdgeInsets.all(0),
        //color: Colors.white,
        /* decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              
              Colors.grey[400],
              Colors.grey[200],
              Colors.grey[200],
              Colors.grey[400],
            ],
          ),
        ), */
        child: Column(
          
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.08),
            Padding(
              padding: EdgeInsets.only(right: 0, left: 0, top: 20, bottom: 0),

              //const EdgeInsets.all(64.0), //64

              child: Column(children: [
                Align(
                    alignment: Alignment.center,
                    child: Container(
                        padding: EdgeInsets.only(
                            left: 0, right: 0, top: 0, bottom: 0),
                        child: Text(
                          'BRANDS',
                          //'S i g n   I n',
                          textAlign: TextAlign.center,
                          //alternative font: fredoka one, fugaz one
                          style: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                            color: Colors.lightGreenAccent[700],
                            fontSize: 48,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.italic
                          )),
                        )
                        ),

                        

                        



                        ),
                      
                        SizedBox(height: MediaQuery.of(context).size.height * 0.08),

                        



                        Container(
                          width: MediaQuery.of(context).size.width * 0.5,
                          height: MediaQuery.of(context).size.height * 0.2,
                          
                          decoration: BoxDecoration(
                            
                        borderRadius: BorderRadius.circular(10),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(urlImage2)
            )
        ),


                        ),
SizedBox(height: MediaQuery.of(context).size.height * 0.01),

RaisedButton(
                      child: Text(
                        "D E T A I L S",
                        style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        )),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4),
                        side: BorderSide(
                          color: Colors.green,
                          width: 0,
                        ),
                      ),
                      onPressed: () => {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomePage()),
                        ),
                      },
                      color: Colors.lightBlue,
                      padding: EdgeInsets.fromLTRB(48, 8, 48, 8),
                      splashColor: Colors.grey,
                    ),

SizedBox(height: MediaQuery.of(context).size.height * 0.06),

Container(
                          width: MediaQuery.of(context).size.width * 0.5,
                          height: MediaQuery.of(context).size.height * 0.2,
                          
                          decoration: BoxDecoration(
                            
                        borderRadius: BorderRadius.circular(10),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(urlImage3)
            )
        ),


                        ),

                        SizedBox(height: MediaQuery.of(context).size.height * 0.01),

                        RaisedButton(
                      child: Text(
                        "D E T A I L S",
                        style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        )),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(4),
                        side: BorderSide(
                          color: Colors.green,
                          width: 0,
                        ),
                      ),
                      onPressed: () => {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomePage()),
                        ),
                      },
                      color: Colors.lightBlue,
                      padding: EdgeInsets.fromLTRB(48, 8, 48, 8),
                      splashColor: Colors.grey,
                    ),

                    SizedBox(height: MediaQuery.of(context).size.height * 0.064),

                    Row(
                          children: [
                            SizedBox(width: MediaQuery.of(context).size.width * 0.22),
                    FlatButton(
                      child: Icon(Icons.home, size: 35, color: Colors.black),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomePage()),
                        );
                      },
                    ),
                    
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.14,
                    ),
                    FlatButton(
                      child: Icon(Icons.find_in_page,
                          size: 35, color: Colors.black),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Instructions()),
                        );
                      },
                    ),
                    
                          ],
                        ),

                        ]
                        )
                        )
                        ]
                        )
                        )
                        )
              ;}}