import 'package:InfluencerApp/screens/verify_code.dart';
import 'package:flutter/material.dart';

//import 'package:flutter/foundation.dart';

import './home_screen.dart';

import './create_account.dart';

import './forgot_password.dart';
import 'package:google_fonts/google_fonts.dart';

class ForgotPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();

    return Scaffold(
      //backgroundColor: Colors.white,

      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,

      /* body: Container(
        margin: EdgeInsets.all(0),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.blue[900],
              Colors.blue[500],
              Colors.blue[400],
              Colors.blue[500],
              Colors.blue[900],
            ],
          ),
        ), */

      //Container(

      // Center(child: Container(

      // margin: const EdgeInsets.all(10.0),

      // color: Colors.lightBlue,

      // width: 48,

      // height:48

      //),

      //),

      body: Container(
        margin: EdgeInsets.all(0),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.grey[50],
              Colors.grey[50],
              Colors.grey[50],
              Colors.grey[50],
            ],
          ),
        ),
        child: Column(
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.129),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2),
                  border: Border.all(width: 0, color: Colors.black),
                  color: Colors.white),
              //color: Colors.white,
              margin: EdgeInsets.only(right: 40, left: 40, top: 84, bottom: 50),
              child: Padding(
                padding: EdgeInsets.only(right: 0, left: 0, top: 20, bottom: 0),

                //const EdgeInsets.all(64.0), //64

                child: Column(children: [
                  Align(
                      alignment: Alignment.center,
                      child: Container(
                          padding: EdgeInsets.only(
                              left: 0, right: 0, top: 0, bottom: 0),
                          child: Text(
                            'F O R G O T   P A S S W O R D ?',
                            textAlign: TextAlign.left,
                            style: GoogleFonts.staatliches(
                                textStyle: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            )),
                          ))),

                  SizedBox(height: MediaQuery.of(context).size.height * 0.0258),

                  Container(
                      width: 280,
                      child: TextField(
                        onChanged: (String str) {
                          print(str);
                        },

                        //onTap: () {

                        //FocusScope.of(context).requestFocus();

                        //},

                        decoration: InputDecoration(
                          hintText: 'EMAIL',

                          fillColor: Colors.grey[50],

                          filled: true,

                          contentPadding: const EdgeInsets.all(16.0),

                          //child: padding(  EdgeInsets.only(

                          // left: 300, right: 300, bottom: 116),

                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 0,
                              ),

                              // borderRadius: BorderRadius.all(Radius.circular(30)),

                              borderRadius:
                                  BorderRadius.all(Radius.elliptical(2, 2))),
                        ),
                      )),

                  SizedBox(height: MediaQuery.of(context).size.height * 0.0258),

                  Container(
                      width: 280,
                      child: TextField(
                        onChanged: (String str) {
                          print(str);
                        },
                        decoration: InputDecoration(
                          hintText: 'CONFIRM EMAIL',
                          fillColor: Colors.grey[50],
                          filled: true,
                          contentPadding: const EdgeInsets.all(16.0),
                          enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black, width: 0),

                              // borderRadius: BorderRadius.all(Radius.circular(30)),

                              borderRadius:
                                  BorderRadius.all(Radius.elliptical(2, 2))),
                        ),
                      )),

                  //SizedBox(height: 1),

                  /* Align(
                      alignment: Alignment.centerRight,
                      child: FlatButton(
                        child: Text(
                          "FORGOT PASSWORD?",
                          style: GoogleFonts.yanoneKaffeesatz(
                              textStyle: TextStyle(fontSize: 20)),
                        ),

                        //shape: Border.all(width: 2, color: Colors.black),

                        onPressed: () => {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ForgotPassword()),
                          ),
                        },

                        color: Colors.white,

                        textColor: Colors.red,

                        padding: EdgeInsets.fromLTRB(1, 0, 30, 0),

                        splashColor: Colors.grey,
                      )), */

                  SizedBox(height: MediaQuery.of(context).size.height * 0.04532),

                  Align(
                      alignment: Alignment.center,
                      child: RaisedButton(
                        child: Text(
                          "S E N D   C O D E",
                          style: GoogleFonts.staatliches(
                              textStyle: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          )),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(2),
                          side: BorderSide(
                            color: Colors.black,
                            width: 0,
                          ),
                        ),
                        onPressed: () => {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => VerifyCode()),
                          ),
                        },
                        color: Colors.green,
                        padding: EdgeInsets.fromLTRB(100, 10, 100, 10),
                        splashColor: Colors.grey,
                      )),

                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.0515,
                  ),

                  /* Container(
                    child: Row(
                      children: <Widget>[
                        Text('Dont have an account?   ',
                            style: GoogleFonts.encodeSans(
                                textStyle: TextStyle(fontSize: 15))),
                        FlatButton(
                          padding: EdgeInsets.fromLTRB(1, 0, 1, 0),
                          splashColor: Colors.grey,
                          textColor: Colors.red,
                          child: Text(
                            'CREATE ACCOUNT',
                            style: GoogleFonts.yanoneKaffeesatz(
                                textStyle: TextStyle(fontSize: 20)),
                            //style: TextStyle(fontSize: 20),
                          ),
                          onPressed: () => {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => CreateAccount()),
                            ), //signup screen
                          },
                        )
                      ],
                      mainAxisAlignment: MainAxisAlignment.center,
                    ),

                    color: Colors.white,

                    //textColor: Colors.red,

                    //padding: EdgeInsets.fromLTRB(1, 0, 1, 0),

                    //splashColor: Colors.grey,
                  ), */

                  //// SizedBox(
                  // height: 10,
                  //),

                  //SizedBox.expand(

                  //Expanded(

                  //child:

                  /* Flexible(
                          child: Expanded(
                              child: Container(
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight,
                                        colors: [
                                          Colors.grey[600],
                                          Colors.grey[400],
                                          Colors.grey[400],
                                          Colors.grey[600],
                                        ],
                                      ),
                                      border: Border(
                                          top: BorderSide(
                                              color: Colors.black, width: 20.0))),
                                  //color: Colors.black, width: (20)),

                                  //borderRadius: BorderRadius.circular(10.0)

                                  padding: const EdgeInsets.only(
                                      left: 300, right: 300, bottom: 116),
                                  margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  alignment: Alignment.bottomCenter,
                                  child: Text(
                                    '',
                                  )))) */

                  /* Container(
                    margin: EdgeInsets.all(0),
                    child: Flexible(
                        child: Expanded(
                            child: Container(
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: [
                                        Colors.grey[600],
                                        Colors.grey[400],
                                        Colors.grey[400],
                                        Colors.grey[600],
                                      ],
                                    ),
                                    border: Border(
                                        top: BorderSide(
                                            color: Colors.black, width: 20.0))),
                                //color: Colors.black, width: (20)),

                                //borderRadius: BorderRadius.circular(10.0)

                                padding: const EdgeInsets.only(
                                    left: 300, right: 300, bottom: 116),
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                alignment: Alignment.bottomCenter,
                                child: Text(
                                  '',
                                )))),
                  ) */
                ]),
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.0772),
            Container(
              margin: EdgeInsets.all(0),
              child: Flexible(
                  child: Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Colors.green,
                                  Colors.green,
                                  Colors.green,  
                                  Colors.green,
                                  Colors.green,
                                ],
                              ),
                              border: Border(
                                  top: BorderSide(
                                      color: Colors.black, width: 0.0))),
                          //color: Colors.black, width: (20)),

                          //borderRadius: BorderRadius.circular(10.0)

                          padding: const EdgeInsets.only(
                              left: 300, right: 300, bottom: 155),
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          alignment: Alignment.bottomCenter,
                          child: Text(
                            '',
                          )))),
            )
          ],
        ),
      ),
    );
  }
}