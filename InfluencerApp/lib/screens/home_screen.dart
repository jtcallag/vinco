import 'package:InfluencerApp/billing/billing.dart';
import 'package:InfluencerApp/campaigns/active_campaign.dart';
import 'package:InfluencerApp/campaigns/campaign.dart';
import 'package:InfluencerApp/stats/stats.dart';
import 'package:flutter/material.dart';




//import 'package:vinco/screens/sign_in.dart';


int _page = 0;
GlobalKey _bottomNavigationKey = GlobalKey();

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      /* appBar: AppBar(
        title: Text('VINCO'), */
      /* centerTitle: true,
        backgroundColor: Colors.lightBlue[600],
      ), */
      /* bottomNavigationBar: CurvedNavigationBar(
        color: Colors.blueAccent,
        backgroundColor: Colors.white,
        buttonBackgroundColor: Colors.blueAccent,
        key: _bottomNavigationKey,
        items: <Widget>[
          Icon(Icons.home, size: 35, color: Colors.black),
          Icon(Icons.add_circle, size: 35, color: Colors.black),
          Icon(
            Icons.card_giftcard,
            size: 35,
            color: Colors.black,
          ),
        ],
        onTap: (index) => {
          Navigator.push(
              context,
              new MaterialPageRoute(
                builder: (context) => ActiveCampaigns(),
              )),
          //setState
          (() {
            _page = index;
          })
        },
      ), */

      /* drawer: Drawer(
          child: new ListView(children: <Widget>[
        new UserAccountsDrawerHeader(
          accountName: new Text("Mike Callaghan"),
          accountEmail: new Text("mqcallaghan9@gmail.com"),
          currentAccountPicture: new CircleAvatar(
            backgroundColor: Colors.cyan,
            child: new Text("M"),
          ),
          otherAccountsPictures: <Widget>[
            new CircleAvatar(
              backgroundColor: Colors.white,
              child: new Text("K"),
            )
          ],
        ),
        new ListTile(
            title: new Text("Campaign"),
            trailing: new Icon(Icons.arrow_upward),
            onTap: () => {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (context) => Campaign(),
                      ))
                  //Navigator.of(context).pop(),
                  // Navigator.of(context).pushNamed("/Campaign"),
                }),
        new ListTile(
            title: new Text("Stats"),
            trailing: new Icon(Icons.arrow_upward),
            onTap: () => {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (context) => Stats(),
                      ))
                  //Navigator.of(context).pop(),
                  // Navigator.of(context).pushNamed("/Campaign"),
                }),
        new ListTile(
            title: new Text("Billing"),
            trailing: new Icon(Icons.arrow_upward),
            onTap: () => {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (context) => Billing(),
                      ))
                  //Navigator.of(context).pop(),
                  // Navigator.of(context).pushNamed("/Campaign"),
                }),
        new ListTile(
            title: new Text("Logout"),
            trailing: new Icon(Icons.arrow_upward),
            onTap: () => {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (context) => Login(),
                      ))
                  //Navigator.of(context).pop(),
                  // Navigator.of(context).pushNamed("/Campaign"),
                }),
        new Divider(),
        new ListTile(
          title: new Text("close"),
          trailing: new Icon(Icons.close),
          onTap: () => Navigator.of(context).pop(),
        ),
      ])), */

      body: Center(
        child: IntrinsicWidth(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(height: 30),
                Text('Welcome, User',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontWeight: FontWeight.w300,
                    )),

                SizedBox(
                  height: 10,
                  //width: 80,
                ),
                Container(
                  //margin: EdgeInsets.only(right: 30, left: 30, bottom: 20),
                  height: 170,
                  width: 350,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Campaign()),
                      );
                    },
                    shape: RoundedRectangleBorder(

                        //borderRadius: BorderRadius.circular(80),
                        ),
                    padding: EdgeInsets.all(0),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Colors.blue[900],
                              Colors.blue[500],
                              Colors.blue[400],
                              Colors.blue[500],
                              Colors.blue[900],
                            ],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          border: Border.all(width: 6, color: Colors.black)
                          //borderRadius: BorderRadius.circular(30)
                          ),
                      child: Container(
                        //constraints:
                        //BoxConstraints(maxWidth: 300, minHeight: 50),
                        alignment: Alignment.center,
                        child: Text(
                          "CAMPAIGNS",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 45,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),

                //SizedBox(height: 30),

                Container(
                  height: 170,
                  width: 350,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Stats()),
                      );
                    },
                    shape: RoundedRectangleBorder(

                        //borderRadius: BorderRadius.circular(80),
                        ),
                    padding: EdgeInsets.all(0),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Colors.blue[900],
                              Colors.blue[500],
                              Colors.blue[400],
                              Colors.blue[500],
                              Colors.blue[900],
                            ],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          border: Border.all(width: 6, color: Colors.black)
                          //borderRadius: BorderRadius.circular(30)
                          ),
                      child: Container(
                        //constraints:
                        // BoxConstraints(maxWidth: 300, minHeight: 50),
                        alignment: Alignment.center,
                        child: Text(
                          "STATS",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 45,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                // SizedBox(height: 30),

                Container(
                  height: 170,
                  width: 350,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Billing()),
                      );
                    },
                    shape: RoundedRectangleBorder(

                        //borderRadius: BorderRadius.circular(80),
                        ),
                    padding: EdgeInsets.all(0),
                    child: Ink(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Colors.blue[900],
                              Colors.blue[500],
                              Colors.blue[400],
                              Colors.blue[500],
                              Colors.blue[900],
                            ],
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                          ),
                          border: Border.all(width: 6, color: Colors.black)
                          //borderRadius: BorderRadius.circular(30)
                          ),
                      child: Container(
                        //constraints:
                        // BoxConstraints(maxWidth: 300, minHeight: 50),
                        alignment: Alignment.center,
                        child: Text(
                          "BILLING",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 45,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),

                //below this are original working buttons
                /* RaisedButton(
                  shape: //RoundedRectangleBorder(
                      //borderRadius: BorderRadius.circular(20)),
                      Border.all(width: 10, color: Colors.black),

                  //child: Ink(),

                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Campaign()),
                    );
                  },
                  child: Text(
                    'CAMPAIGNS',
                    style: TextStyle(
                        fontSize: 50.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  color: Colors.lightBlue[600],
                  padding: const EdgeInsets.only(
                      //top: 80, bottom: 80, right: 120, left: 120),
                      top: 60,
                      bottom: 60,
                      right: 30,
                      left: 30),
                ),

                RaisedButton(
                  shape: // RoundedRectangleBorder(
                      // borderRadius: BorderRadius.circular(20)
                      Border.all(width: 10, color: Colors.black),

                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Stats()),
                    );
                  },
                  child: Text(
                    'STATS',
                    style: TextStyle(
                        fontSize: 50.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  color: Colors.lightBlue[600],
                  padding: const EdgeInsets.only(
                      //top: 80, bottom: 80, right: 140, left: 140)
                      top: 60,
                      bottom: 60,
                      right: 30,
                      left: 30),

                  //padding: EdgeInsets.all(40.0),
                ),
                //SizedBox(
                //height: 80,
                //width: 80,
                //),
                RaisedButton(
                  shape: //RoundedRectangleBorder(
                      //borderRadius: BorderRadius.circular(20),
                      Border.all(width: 10, color: Colors.black),

                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Billing()),
                    );
                  },
                  child: Text(
                    'BILLING',
                    style: TextStyle(
                        fontSize: 50.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  color: Colors.lightBlue[600],
                  padding: const EdgeInsets.only(
                      //top: 80, bottom: 80, right: 134, left: 134),
                      top: 60,
                      bottom: 60,
                      right: 30,
                      left: 30),
                  //padding: EdgeInsets.all(40.0),
                ), */

                //above this are original working buttons
              ]

              //),
              //children: <Widget>[
              //Container(
              //padding: EdgeInsets.all(20.0),
              //color: Colors.cyan,
              //child: Text('CAMPAIGNS')),
              //Container(
              // padding: EdgeInsets.all(20.0),
              //color: Colors.cyan,
              //child: Text('STATS')),
              //Container(
              //padding: EdgeInsets.all(20.0),
              //color: Colors.cyan,
              //child: Text('BILLING')),
              // ])

              //body: Center(
              //child: RaisedButton(
              //onPressed: () {},
              //child: Text('CAMPAIGNS'),
              //color: Colors.lightBlue,
              //),
              //),
              //floatingActionButton: FloatingActionButton(
              //onPressed: () {},
              //child: Text('click'),
              //backgroundColor: Colors.red[600],
              ),
        ),
      ),
    );
  }
}