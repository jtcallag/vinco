import 'package:InfluencerApp/campaigns/active_campaign.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

//import './forgot_password.dart';

class CreateAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    return Scaffold(
        backgroundColor: Colors.white,
        //appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          //title: Text("VINCO"),
          //elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,
        //),
        body: Container(
          margin: EdgeInsets.all(0),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.grey[50],
                Colors.grey[50],
                Colors.grey[50],
                Colors.grey[50],
              ],
            ),
          ),
          
          child: Column(
            children: [
              SizedBox(height: 50),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    border: Border.all(width: 0, color: Colors.black),
                    color: Colors.white),
                margin:
                    EdgeInsets.only(right: 40, left: 40, top: 15, bottom: 5),
                child: Padding(
                    padding: const EdgeInsets.only(
                        top: 10, right: 0, left: 0, bottom: 0),
                    child: Column(
                      children: [
                        Align(
                            alignment: Alignment.center,
                            child: Container(
                              padding: EdgeInsets.only(
                                  left: 0, right: 0, top: 0, bottom: 0),
                              child: Text('S i g n   U p',
                                  textAlign: TextAlign.right,
                                  style: GoogleFonts.staatliches(
                                 textStyle: TextStyle(
                                    
                                    color: Colors.black,
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                  ))),
                            )),
                        SizedBox(height: 5),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },

                              //onTap: () {
                              //FocusScope.of(context).requestFocus();
                              //},
                              decoration: InputDecoration(
                                hintText: 'EMAIL',
                                fillColor: Colors.grey[200],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.grey[200],
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(10, 10))),
                              ),
                            )),
                        SizedBox(height: 5),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'Confirm EMAIL',
                                fillColor: Colors.grey[200],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.grey[200],
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(10, 10))),
                              ),
                            )),
                        SizedBox(height: 20),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'PASSWORD',
                                fillColor: Colors.grey[200],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.grey[200],
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(10, 10))),
                              ),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'CONFIRM PASSWORD',
                                fillColor: Colors.grey[200],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.grey[200],
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(10, 10))),
                              ),
                            )),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'USERNAME',
                                fillColor: Colors.grey[200],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.grey[200],
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(10, 10))),
                              ),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'FULL NAME',
                                fillColor: Colors.grey[200],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.grey[200],
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(10, 10))),
                              ),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'PHONE NUMBER',
                                fillColor: Colors.grey[200],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.grey[200],
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(10, 10))),
                              ),
                            )),
                        SizedBox(height: 20),
                        Align(
                            alignment: Alignment.center,
                            child: RaisedButton(
                              child: Text("C R E A T E",
                                  style: GoogleFonts.staatliches(
                                  textStyle: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ))),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2),
                                side: BorderSide(
                                  color: Colors.green,
                                  width: 0,
                                ),
                              ),
                              onPressed: () => {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ActiveCampaigns()),
                                ),
                              },
                              color: Colors.green,
                              padding: EdgeInsets.fromLTRB(110, 10, 110, 10),
                              splashColor: Colors.grey,
                            )),
                        SizedBox(height: 10),
                      ],

                      //SizedBox.expand(

                      //Expanded(

                      //child:

                      /* Flexible(
                          child: Expanded(
                              child: Container(
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight,
                                        colors: [
                                          Colors.blue[900],
                                          Colors.blue[500],
                                          Colors.blue[400],
                                          Colors.blue[500],
                                          Colors.blue[900],
                                        ],
                                      ),
                                      //color: Colors.lightBlue,
                                      border: Border(
                                          top: BorderSide(
                                              color: Colors.black, width: 20.0))),
                                  //color: Colors.black, width: (20)),

                                  //borderRadius: BorderRadius.circular(10.0)

                                  padding: const EdgeInsets.only(
                                      left: 300, right: 300, bottom: 116),
                                  margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  alignment: Alignment.bottomCenter,
                                  child: Text(
                                    '',
                                  )))) */
                    )),
              ),
              SizedBox(height: 60),
              Flexible(
                  child: Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Colors.green,
                                  Colors.green,
                                  Colors.green,
                                  Colors.green,
                                  Colors.green,
                                ],
                              ),
                              //color: Colors.lightBlue,
                              border: Border(
                                  top: BorderSide(
                                      color: Colors.black, width: 0))),
                          //color: Colors.black, width: (20)),

                          //borderRadius: BorderRadius.circular(10.0)

                          padding: const EdgeInsets.only(
                              left: 300, right: 300, bottom: 116),
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          alignment: Alignment.bottomCenter,
                          child: Text(
                            '',
                          ))))
            ],
          ),
        ));
  }
}
