import 'package:InfluencerApp/billing/billing.dart';
import 'package:InfluencerApp/billing/pay_stubs.dart';
import 'package:InfluencerApp/billing/paystubs2.dart';
import 'package:InfluencerApp/billing/redeem_gifts.dart';
import 'package:InfluencerApp/campaigns/active_campaign.dart';
import 'package:InfluencerApp/campaigns/campaign.dart';
import 'package:InfluencerApp/campaigns/campaign_search.dart';
import 'package:InfluencerApp/campaigns/new_campaign.dart';
import 'package:InfluencerApp/screens/new_homescreen.dart';
import 'package:InfluencerApp/screens/sign_in.dart';
import 'package:InfluencerApp/stats/stats.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


//import 'package:flutter/foundation.dart';
//import 'package:vinco/home_screen.dart';
//import './forgot_password.dart';

class Instructions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    final urlImage = 'https://wallpaperaccess.com/full/882.jpg';
    
    return Scaffold(
    
    resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(urlImage)
            )
        )
        ,

         child: Column(
          
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.08),
            Padding(
              padding: EdgeInsets.only(right: 0, left: 0, top: 20, bottom: 0),

              //const EdgeInsets.all(64.0), //64

              child: Column(children: [
                Align(
                    alignment: Alignment.center,

                    child: Container(
                        padding: EdgeInsets.only(
                            left: 0, right: 0, top: 0, bottom: 0),
                        child: Text(
                          'Instructions',
                          //'S i g n   I n',
                          textAlign: TextAlign.center,
                          //alternative font: fredoka one, fugaz one
                          style: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                            color: Colors.lightGreenAccent[700],
                            fontSize: 48,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.italic
                          )),
                        )
                        ),),

                        SizedBox(height: MediaQuery.of(context).size.height * 0.04),
                     Container(
                        padding: EdgeInsets.only(right: 0, left: 0, top: 20, bottom: 0),
                          width: MediaQuery.of(context).size.width * 0.8,
                          height: MediaQuery.of(context).size.height * 0.5,
                          
                          decoration: BoxDecoration(
                            color: Colors.white,
                        borderRadius: BorderRadius.circular(10),),
                        child: Text(
                          'If you have made it past the sign up page, you have affiliate codes to all brands on the home page which are your username',
                          //'S i g n   I n',
                          textAlign: TextAlign.center,
                          //alternative font: fredoka one, fugaz one
                          style: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                            color: Colors.lightGreenAccent[700],
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.italic
                          )),
                        )
                        ),

                        SizedBox(height: MediaQuery.of(context).size.height * 0.21),


                        Row(
                          children: [
                            SizedBox(width: MediaQuery.of(context).size.width * 0.22),
                    FlatButton(
                      child: Icon(Icons.home, size: 35, color: Colors.white),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomePage()),
                        );
                      },
                    ),
                    
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.14,
                    ),
                    FlatButton(
                      child: Icon(Icons.find_in_page,
                          size: 35, color: Colors.white),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Instructions()),
                        );
                      },
                    ),
                    
                          ],
                        ),

                        



                        

              ]))])
    ));
        }
        }