import 'package:InfluencerApp/campaigns/active_campaign.dart';
import 'package:InfluencerApp/screens/new_homescreen.dart';
import 'package:flutter/material.dart';

//import 'package:flutter/foundation.dart';

import './home_screen.dart';

import './create_account.dart';

import './forgot_password.dart';
import 'package:google_fonts/google_fonts.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
bool _isChecked = false;
//Attempting to get the backend talking to sign in page

  //final String baseUrl = "insert url to local host"

  //void _fetchDataFromTheServer() async{
  //final Dio dio = new Dio();

  //try {
  // var response = await dio.get("$baseUrl/api/users");
  //var responseData = responseData as List;

  //setState(() {
  //users = responseData.map((e) => User.fromJson(e)).toList();
  //});
  //}
  // on DioError catch(e) {
//    print (e);
  //}

  // }

  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
     final urlImage = 'https://wallpaperaccess.com/full/882.jpg';
     
     //bool _isChecked = false;

    return Scaffold(
      //backgroundColor: Colors.white,

      resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,

      /* body: Container(
        margin: EdgeInsets.all(0),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.blue[900],
              Colors.blue[500],
              Colors.blue[400],
              Colors.blue[500],
              Colors.blue[900],
            ],
          ),
        ), */

      //Container(

      // Center(child: Container(

      // margin: const EdgeInsets.all(10.0),

      // color: Colors.lightBlue,

      // width: 48,

      // height:48

      //),

      //),

      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(urlImage)
            )
        ),
        margin: EdgeInsets.all(0),
        //color: Colors.white,
        /* decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              
              Colors.grey[400],
              Colors.grey[200],
              Colors.grey[200],
              Colors.grey[400],
            ],
          ),
          
        ), */
        child: Column(
          
          children: [
            SizedBox(height: MediaQuery.of(context).size.height * 0.2),
            Padding(
              padding: EdgeInsets.only(right: 0, left: 0, top: 20, bottom: 0),

              //const EdgeInsets.all(64.0), //64

              child: Column(children: [
                Align(
                    alignment: Alignment.center,
                    child: Container(
                        padding: EdgeInsets.only(
                            left: 0, right: 0, top: 0, bottom: 0),
                        child: Text(
                          'CAMPAIGN',
                          //'S i g n   I n',
                          textAlign: TextAlign.center,
                          //alternative font: fredoka one, fugaz one
                          style: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                            color: Colors.lightGreenAccent[700],
                            fontSize: 48,
                            fontWeight: FontWeight.w900,
                            fontStyle: FontStyle.italic
                          )),
                        ))),

                SizedBox(height: MediaQuery.of(context).size.height * 0.02),

                Container(
                    width: MediaQuery.of(context).size.width * 0.68,
                    child: TextField(
                      onChanged: (String str) {
                        print(str);
                      },

                      //onTap: () {

                      //FocusScope.of(context).requestFocus();

                      //},

                      decoration: InputDecoration(
                        hintText: 'U s e r n a m e',
                        hintStyle: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                            color: Colors.lightGreenAccent[700],
                            //fontSize: 48,
                            fontWeight: FontWeight.w900,
                          )),

                        fillColor: Colors.grey[200],

                        filled: true,

                        contentPadding: const EdgeInsets.all(16.0),

                        //child: padding(  EdgeInsets.only(

                        // left: 300, right: 300, bottom: 116),

                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.lightGreenAccent[700],
                              width: 2,
                            ),

                            // borderRadius: BorderRadius.all(Radius.circular(30)),

                            borderRadius:
                                BorderRadius.all(Radius.elliptical(10,10))),
                      ),
                    )),

                SizedBox(height: MediaQuery.of(context).size.height * 0.014),

                Container(
                    width: MediaQuery.of(context).size.width * 0.68,
                    child: TextField(
                      onChanged: (String str) {
                        print(str);
                      },
                      decoration: InputDecoration(
                        hintText:  'E m a i l',
                        hintStyle: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                            color: Colors.lightGreenAccent[700],
                            //fontSize: 48,
                            fontWeight: FontWeight.bold,
                          )),
                        fillColor: Colors.grey[200],
                        filled: true,
                        contentPadding: const EdgeInsets.all(16.0),
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.lightGreenAccent[700], width: 2),

                            // borderRadius: BorderRadius.all(Radius.circular(30)),

                            borderRadius:
                                BorderRadius.all(Radius.elliptical(10,10))),
                      ),
                    )),

                    SizedBox(height: MediaQuery.of(context).size.height * 0.014),

                Container(
                    width: MediaQuery.of(context).size.width * 0.68,
                    child: TextField(
                      onChanged: (String str) {
                        print(str);
                      },
                      decoration: InputDecoration(
                        hintText:  'V E N M O',
                        hintStyle: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                            color: Colors.blue,
                            fontStyle: FontStyle.italic,
                            //fontSize: 48,
                            fontWeight: FontWeight.bold,
                          )),
                        fillColor: Colors.grey[200],
                        filled: true,
                        contentPadding: const EdgeInsets.all(16.0),
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.lightGreenAccent[700], width: 2),

                            // borderRadius: BorderRadius.all(Radius.circular(30)),

                            borderRadius:
                                BorderRadius.all(Radius.elliptical(10,10))),
                      ),
                    )),

                    

                SizedBox(height: MediaQuery.of(context).size.height * 0.032),
               SizedBox(width: 20),
               //SizedBox(height: 10)
                //Align(
                  
                    //alignment: Alignment.centerRight,
                    
                    //child: FlatButton(
                      
                      //child: Text(
                        //"FORGOT PASSWORD?",
                        //style: GoogleFonts.tajawal(
                        //    textStyle: TextStyle(fontSize: 15)),
                      //),

                      //shape: Border.all(width: 2, color: Colors.black),

                      //onPressed: () => {
                        //Navigator.push(
                          //context,
                          //MaterialPageRoute(
                          //    builder: (context) => ForgotPassword()),
                        //),
                      //},

                      //color: Colors.white,

                      //textColor: Colors.black,

                      //padding: EdgeInsets.fromLTRB(1, 0, 30, 0),

                      //splashColor: Colors.grey,
                    //)),

                SizedBox(height: MediaQuery.of(context).size.height * 0.001),

                Align(
                    alignment: Alignment.center,
                    child: RaisedButton(
                      child: Text(
                        "Terms and Conditions",
                        style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        )),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(
                          color: Colors.cyan[200],
                          width: 0,
                        ),
                      ),
                      onPressed: () => {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ActiveCampaigns()),
                        ),
                      },
                      color: Colors.cyan[200],
                      padding: EdgeInsets.fromLTRB(40, 12, 40, 12),
                      splashColor: Colors.grey,
                    )
                    
                    ),

                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.012,),
         
         //CheckboxListTile(
                   //checkColor: Colors.green,
                    //title: Text("Agree",
                    //style: GoogleFonts.montserrat(
                    //textStyle: TextStyle(color: Colors.white)
                    //),),
                    //secondary: Icon(Icons.beach_access,
                    //color: Colors.white,
                    //),
                    //controlAffinity: ListTileControlAffinity.leading,
                    
                  //  value: _isChecked,
                //    activeColor: Colors.white,
              //      onChanged: (bool value) {
            //          setState(() {
          //_isChecked = value;
        //});
                    //},
                  //),
SizedBox(
                  height: MediaQuery.of(context).size.height * 0.012,),
                
                Align(
                    alignment: Alignment.center,
                    child: RaisedButton(
                      child: Text(
                        "S U B M I T",
                        style: GoogleFonts.montserrat(
                            textStyle: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        )),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(
                          color: Colors.green,
                          width: 0,
                        ),
                      ),
                      onPressed: () => {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => HomePage()),
                        ),
                      },
                      color: Colors.lightGreenAccent[700],
                      padding: EdgeInsets.fromLTRB(40, 12, 40, 12),
                      splashColor: Colors.grey,
                    )
                    
                    ),

                

                  

                  //textColor: Colors.red,

                  //padding: EdgeInsets.fromLTRB(1, 0, 1, 0),

                  //splashColor: Colors.grey,
              ]),

                //// SizedBox(
                // height: 10,
                //),

                //SizedBox.expand(

                //Expanded(

                //child:

                /* Flexible(
                        child: Expanded(
                            child: Container(
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: [
                                        Colors.grey[600],
                                        Colors.grey[400],
                                        Colors.grey[400],
                                        Colors.grey[600],
                                      ],
                                    ),
                                    border: Border(
                                        top: BorderSide(
                                            color: Colors.black, width: 20.0))),
                                //color: Colors.black, width: (20)),

                                //borderRadius: BorderRadius.circular(10.0)

                                padding: const EdgeInsets.only(
                                    left: 300, right: 300, bottom: 116),
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                alignment: Alignment.bottomCenter,
                                child: Text(
                                  '',
                                )))) */

                /* Container(
                  margin: EdgeInsets.all(0),
                  child: Flexible(
                      child: Expanded(
                          child: Container(
                              decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                    colors: [
                                      Colors.grey[600],
                                      Colors.grey[400],
                                      Colors.grey[400],
                                      Colors.grey[600],
                                    ],
                                  ),
                                  border: Border(
                                      top: BorderSide(
                                          color: Colors.black, width: 20.0))),
                              //color: Colors.black, width: (20)),

                              //borderRadius: BorderRadius.circular(10.0)

                              padding: const EdgeInsets.only(
                                  left: 300, right: 300, bottom: 116),
                              margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                              alignment: Alignment.bottomCenter,
                              child: Text(
                                '',
                              )))),
                ) */
              ),
            
            /* SizedBox(height: 109.85),
            Container(
              margin: EdgeInsets.all(0),
              child: Flexible(
                  child: Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Colors.green,
                                  Colors.green,
                                  Colors.green,
                                  Colors.green,
                                  Colors.green,
                                ],
                              ),
                              border: Border(
                                  top: BorderSide(
                                      color: Colors.black, width: 0.0))),
                          //color: Colors.black, width: (20)),

                          //borderRadius: BorderRadius.circular(10.0)

                          padding: const EdgeInsets.only(
                              left: 300, right: 300, bottom: 119),
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          alignment: Alignment.bottomCenter,
                          child: Text(
                            '',
                          )))),
            ) */
          ],
        ),
      ),
    );
  }
}
