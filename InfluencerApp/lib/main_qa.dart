import 'package:InfluencerApp/environment/flavor.dart';
import 'package:flutter/material.dart';
import 'environment/appconfig.dart';
import 'main.dart';

void main() {
  var appConfig = AppConfig(
    apiUrl: "",
    flavor: Flavor.qa,
    child: MyApp(),
  );

  return runApp(appConfig);
}
