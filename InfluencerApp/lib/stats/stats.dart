/* import 'package:flutter/material.dart';
import 'package:vinco/billing/billing.dart';
import 'package:vinco/campaigns/campaign.dart';
import 'package:vinco/screens/sign_in.dart';
//import 'package:flutter/foundation.dart';
//import 'package:vinco/home_screen.dart';
//import './forgot_password.dart';

class Stats extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    return Scaffold(
        backgroundColor: Colors.grey[600],
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text("STATS"),
        ), //elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,
        drawer: Drawer(
            child: new ListView(children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: new Text("Mike Callaghan"),
            accountEmail: new Text("mqcallaghan9@gmail.com"),
            currentAccountPicture: new CircleAvatar(
              backgroundColor: Colors.cyan,
              child: new Text("M"),
            ),
            otherAccountsPictures: <Widget>[
              new CircleAvatar(
                backgroundColor: Colors.white,
                child: new Text("K"),
              )
            ],
          ),
          new ListTile(
              title: new Text("Campaign"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Campaign(),
                        ))
                    //Navigator.of(context).pop(),
                    //Navigator.of(context).pushNamed("/Campaign"),
                  }),
          new ListTile(
              title: new Text("Stats"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Stats(),
                        ))
                  }),
          new ListTile(
              title: new Text("Billing"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Billing(),
                        ))
                    //Navigator.pop(context)
                    //Navigator.pop(
                    // context,
                    // MaterialPageRoute(builder: (context) => Billing()),
                    // )
                    // Navigator.of(context).pop(),
                    // Navigator.of(context).pushNamed("/Billing"),
                  }),
          new ListTile(
              title: new Text("Logout"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Login(),
                        ))
                  }),
          new Divider(),
          new ListTile(
            title: new Text("close"),
            trailing: new Icon(Icons.close),
            onTap: () => Navigator.of(context).pop(),
          ),
        ])),
        body: Column(
          children: [],
        ));
  }
}
 */

import 'package:InfluencerApp/stats/chart1.dart';
import 'package:InfluencerApp/stats/chart2.dart';

import 'package:flutter/material.dart';

//import 'package:clay_containers/clay_containers.dart';

//import 'package:flutter/foundation.dart';

//import 'package:vinco/home_screen.dart';

//import './forgot_password.dart';

final data = [55.0, 90.0, 50.0, 40.0, 35.0, 55.0, 70.0, 100.0];

class Stats extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by

        // the App.build method, and use it to set our appbar title.

        title: Text("STATS"),
      ),
      //elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,

      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Text(
                  "STATS",
                  style: TextStyle(fontSize: 40, color: Colors.black),
                ),
                /* Text(
                  "USER",
                  style: TextStyle(
                    fontSize: 26,
                    color: Colors.black,
                    fontWeight: FontWeight.w800,
                  ),
                ), */
                SizedBox(
                  height: 10,
                ),
                //ClayContainer(
                Container(
                  height: 270,
                  width: 370,
                  //depth: 12,
                  //spread: 12,
                  //borderRadius: 16,
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 16,
                      right: 16,
                      top: 16,
                    ),
                    child: Column(
                        //crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Sales",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.w900,
                                color: Colors.blue),
                          ),
                          Text(
                            "20",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.w900,
                                color: Colors.blue),
                          ),
                          SizedBox(height: 20),
                          Chart(
                            data: data,
                          ),
                        ]),
                  ),
                ),
                SizedBox(height: 30),

                Container(
                  height: 270,
                  width: 370,
                  //depth: 12,
                  //spread: 12,
                  //borderRadius: 16,
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 16,
                      right: 16,
                      top: 16,
                    ),
                    child: Column(
                        //crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Commission",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.w900,
                                color: Colors.blue),
                          ),
                          Text(
                            "138 DOLLARS",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.w900,
                                color: Colors.blue),
                          ),
                          SizedBox(height: 20),
                          Chart2(
                            data: data,
                          ),
                        ]),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}