import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Chart2 extends StatelessWidget {
  final List<double> data;

  const Chart2({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ClipPath(
        clipper: ChartClipper(data: data, maxValue: data.reduce(max)),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(16)),
            border: Border.all(width: 4, color: Colors.black),
            //side: BorderSide(
            // color: Colors.black,
            // width: 3,
            // ),
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Colors.blue[900],
                Colors.blue[700],
                Colors.blue[400],
                Colors.blue[300],
                Colors.grey[300],
                Colors.grey[400],
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ChartClipper extends CustomClipper<Path> {
  final double maxValue;
  final List<double> data;

  ChartClipper({this.maxValue, this.data});

  @override
  Path getClip(Size size) {
    double sectionWidth = size.width / (data.length - 1);
    Path path = Path();

    path.moveTo(0, size.height);

    for (int i = 0; i < data.length; i++) {
      path.lineTo(
          i * sectionWidth, size.height - size.height * (data[i] / maxValue));
    }

    /* path.lineTo(0 * sectionWidth, size.height - size.height * (data[0]/maxValue));
    path.lineTo(1 * sectionWidth, size.height - size.height * (data[1]/maxValue));
    path.lineTo(2 * sectionWidth, size.height - size.height * (data[2]/maxValue)); */

    path.lineTo(size.width, size.height);

    return path;
    //throw UnimplementedError();
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
  //{

  // }
}