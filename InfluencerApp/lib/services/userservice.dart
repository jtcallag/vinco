import 'dart:convert';

import 'package:InfluencerApp/environment/appconfig.dart';
import 'package:InfluencerApp/models/influencer_model.dart';
import 'package:InfluencerApp/services/urlroutes.dart';
import 'package:InfluencerApp/vinco/vincouserpreferences.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class UserService {
  BuildContext _context;

  UserService(BuildContext context) {
    _context = context;
  }
  Future<String> login(String email, String password) async {
    final baseUrl = Provider.of<AppConfig>(_context).apiUrl;
    final url = UrlRoutes.loginPost;
    final response = await http.post('$baseUrl$url');

    if (response.statusCode == 200) {
      //set the token in UserPreferences
      final settings = InfluencerModel.fromJson(json.decode(response.body));

      await VincoUserPreferences.saveInfluencerInfo(settings);
      return settings.authenticationToken;
    }

    throw Exception('Invalid email and/or password.');
  }
}
