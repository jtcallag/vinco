class UrlRoutes {
  static final String registerPost = '/influencer/register';
  static final String loginPost = '/influencer/login';
  static final String influencerGet = '/influencer/{uniqID}';
  static final String influencerPut = '/influencer';
  static final String influencerAddCampaignPost =
      '/influencer/{uniqID}/campaign/{campaignUniqID}';
  static final String influencerCampaignsGet = '/influencer/{uniqID}/campaigns';
  static final String influencerCampaignDetailGet =
      '/influencer/{uniqID}/campaign/{campaignUniqID}';
      
}
