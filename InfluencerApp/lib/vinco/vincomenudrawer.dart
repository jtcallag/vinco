import 'package:flutter/material.dart';
//import 'package:flutter/foundation.dart';

class VincoMenu extends StatelessWidget {
  

  

  @override
  Widget build(BuildContext context) {
    return new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text("Mike Callaghan"), 
              accountEmail: new Text("mqcallaghan9@gmail.com"),
              currentAccountPicture: new CircleAvatar(
                backgroundColor: Colors.red,
                child: new Text("M"),
              ),
              otherAccountsPictures: <Widget>[
                new CircleAvatar(
                  backgroundColor: Colors.white,
                  child: new Text("K"),
                )
              ],
            ),
            new ListTile(
              title: new Text("Login"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                Navigator.of(context).pop(),
                Navigator.of(context).pushNamed("/login"),
              }
            ),
            new ListTile(
              title: new Text("Promote"),
              trailing: new Icon(Icons.arrow_upward),
            ),
            new ListTile(
              title: new Text("Paycheck"),
              trailing: new Icon(Icons.arrow_upward),
            ),
            new ListTile(
              title: new Text("Summary"),
              trailing: new Icon(Icons.arrow_upward),
            ),
            new Divider(),
            new ListTile(
               
              title: new Text("close"),
              trailing: new Icon(Icons.close),  
              onTap: () => Navigator.of(context).pop(),         
            ),
          ]
        )
      );

      
    
  }
}