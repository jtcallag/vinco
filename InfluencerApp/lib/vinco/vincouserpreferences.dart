import 'package:InfluencerApp/models/influencer_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VincoUserPreferences {
  static Future<dynamic> getPref(VincoPref prefType) async {
    final prefs = await SharedPreferences.getInstance();
    final pref = await prefs.get(prefType.toString().split('.').last);
    return pref;
  }

  static Future<void> setPref(VincoPref prefType, dynamic value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(prefType.toString().split('.').last, value);
  }

  static Future<void> saveInfluencerInfo(InfluencerModel influencer) async {
    await setPref(VincoPref.authtoken, influencer.authenticationToken);
    await setPref(VincoPref.firstName, influencer.firstName);
    await setPref(VincoPref.lastName, influencer.lastName);
    await setPref(VincoPref.email, influencer.email);
    await setPref(VincoPref.mobilePhone, influencer.mobilePhone);
    await setPref(VincoPref.isActive, influencer.isActive);
    await setPref(
        VincoPref.isRegistrationComplete, influencer.isRegistrationComplete);
  }
}

enum VincoPref {
  authtoken,
  firstName,
  lastName,
  email,
  mobilePhone,
  dateOfBirth,
  isActive,
  isRegistrationComplete
}
