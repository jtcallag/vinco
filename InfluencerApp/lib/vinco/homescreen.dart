//  import 'package:flutter/material.dart';
// import 'package:flutter/foundation.dart';

// class Login extends StatelessWidget {
  

  

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(



       
//       appBar: AppBar(
//         // Here we take the value from the MyHomePage object that was created by
//         // the App.build method, and use it to set our appbar title.
//         title: Text("VINCO Influencer"),
//         elevation: defaultTargetPlatform == TargetPlatform.android? 5.0 : 0.0,
//       ),
//       body: 
//         Padding(
//   padding: const EdgeInsets.all(64.0),
//         child: Column(children: [
//          TextField(
//           onChanged: (String str){
//             print (str);
//           },
         
        
//           decoration: InputDecoration(
//             hintText: 'Promote',
//             contentPadding: const EdgeInsets.all(48.0),
//             enabledBorder: OutlineInputBorder(
//               borderSide: BorderSide(color: Colors.red),
//               borderRadius: BorderRadius.all(Radius.circular(30)
//               ),
//             ),
//           ),

          
//         ), 
         
//         SizedBox(height: 60),

//         TextField(
//           onChanged: (String str){
//             print (str);
//           },
         
        
//           decoration: InputDecoration(
//             hintText: 'Summary',
//             contentPadding: const EdgeInsets.all(48.0),
//             enabledBorder: OutlineInputBorder(
//               borderSide: BorderSide(color: Colors.red),
//               borderRadius: BorderRadius.all(Radius.circular(30)
//               ),
//             ),
//           ),

          
//         ), 
//           SizedBox(height: 60),

//          TextField(
//           onChanged: (String str){
//             print (str);
//           },
         
        
//           decoration: InputDecoration(
//             hintText: 'Paycheck',
//             contentPadding: const EdgeInsets.all(48.0),
//             enabledBorder: OutlineInputBorder(
//               borderSide: BorderSide(color: Colors.red),
//               borderRadius: BorderRadius.all(Radius.circular(30)
//               ),
//             ),
//           ),

        
         
//         ),
       
        
//         ],
        
//     ),
//         ),
//     );

   
         
//   }
// }





  
 import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Navigation Basics',
    home: FirstRoute(),
  ));
}

class FirstRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('First Route'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Open route'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SecondRoute()),
            );
          },
        ),
      ),

       );
  }
}

class SecondRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Second Route"),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}