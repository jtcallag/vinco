import 'package:InfluencerApp/environment/flavor.dart';
import 'package:flutter/material.dart';
import 'environment/appconfig.dart';
import 'main.dart';

void main() {
  var appConfig = AppConfig(
    apiUrl: 'https://http://159.65.65.121:31100/api',
    flavor: Flavor.dev,
    child: MyApp(),
  );

  return runApp(appConfig);
}
