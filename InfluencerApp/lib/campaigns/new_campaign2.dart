//import 'dart:io';

import 'package:InfluencerApp/billing/billing.dart';
import 'package:InfluencerApp/billing/pay_stubs.dart';
import 'package:InfluencerApp/billing/redeem_gifts.dart';
import 'package:InfluencerApp/billing/setup_billing.dart';
import 'package:InfluencerApp/campaigns/active_campaign.dart';
import 'package:InfluencerApp/campaigns/campaign.dart';
import 'package:InfluencerApp/campaigns/campaign_search.dart';
import 'package:InfluencerApp/campaigns/new_campaign.dart';
import 'package:InfluencerApp/campaigns/termsofservice.dart';
import 'package:InfluencerApp/screens/sign_in.dart';
import 'package:InfluencerApp/stats/stats.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
//import 'package:google_fonts/google_fonts.dart';



//import 'package:vinco/campaigns/active_campaign.dart';


//import 'package:vinco/screens/home_screen.dart';


//import 'package:flutter/foundation.dart';
//import 'package:vinco/home_screen.dart';
//import './forgot_password.dart';

class NewCampaign2 extends StatefulWidget {
  @override
  _NewCampaign2State createState() => _NewCampaign2State();
}

class _NewCampaign2State extends State<NewCampaign2> {
bool _isChecked = false;

  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    return Scaffold(
      backgroundColor: Colors.white,
      /* appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text("NEW CAMPAIGNS"),
      ), */ //elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,
      drawer: Drawer(
          child: new ListView(children: <Widget>[
        new UserAccountsDrawerHeader(
          accountName: new Text("Mike Callaghan"),
          accountEmail: new Text("mqcallaghan9@gmail.com"),
          currentAccountPicture: new CircleAvatar(
            backgroundColor: Colors.cyan,
            child: new Text("M"),
          ),
          otherAccountsPictures: <Widget>[
            new CircleAvatar(
              backgroundColor: Colors.white,
              child: new Text("K"),
            )
          ],
        ),
        new ListTile(
            title: new Text("Campaign"),
            trailing: new Icon(Icons.arrow_upward),
            onTap: () => {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (context) => Campaign(),
                      ))
                  //Navigator.of(context).pop(),
                  //Navigator.of(context).pushNamed("/Campaign"),
                }),
        new ListTile(
            title: new Text("Stats"),
            trailing: new Icon(Icons.arrow_upward),
            onTap: () => {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (context) => Stats(),
                      ))
                }),
        new ListTile(
            title: new Text("Billing"),
            trailing: new Icon(Icons.arrow_upward),
            onTap: () => {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (context) => Billing(),
                      ))
                  //Navigator.pop(context)
                  //Navigator.pop(
                  // context,
                  // MaterialPageRoute(builder: (context) => Billing()),
                  // )
                  // Navigator.of(context).pop(),
                  // Navigator.of(context).pushNamed("/Billing"),
                }),
        new ListTile(
            title: new Text("Logout"),
            trailing: new Icon(Icons.arrow_upward),
            onTap: () => {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (context) => Login(),
                      ))
                }),
        new Divider(),
        new ListTile(
          title: new Text("close"),
          trailing: new Icon(Icons.close),
          onTap: () => Navigator.of(context).pop(),
        ),
      ])),
      body: Center(
        child: Column(
            //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //crossAxisAlignment: CrossAxisAlignment.stretch,

            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.height * 0.08),
              Text('C O M P A N Y',
                  textAlign: TextAlign.left,
                  style: GoogleFonts.staatliches(
                      textStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 30,
                    fontWeight: FontWeight.w300,
                  ))),
              SizedBox(height: MediaQuery.of(context).size.height * 0.06),
              Padding(
                padding: const EdgeInsets.only(right: 30.0, left: 30),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4),
                    side: BorderSide(
                      color: Colors.black,
                      width: 1,
                    ),
                  ),
                  //Border.all(width: 4, color: Colors.black),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => BillingSetup()),
                    );
                  },
                  child: Text('C A S H A P P',
                      style: GoogleFonts.staatliches(
                        textStyle: TextStyle(
                            fontSize: 30.0,
                            //fontWeight: FontWeight.bold,
                            color: Colors.white),
                      )),
                  color: Colors.yellow,
                  padding: const EdgeInsets.only(
                      //top: 80, bottom: 80, right: 120, left: 120),
                      top: 60,
                      bottom: 60,
                      right: 100,
                      left: 100),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.01),
              Padding(
                padding: const EdgeInsets.only(right: 30, left: 30),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4),
                    side: BorderSide(
                      color: Colors.black,
                      width: 1,
                    ),
                  ),
                  //Border.all(width: 4, color: Colors.black),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => ServiceTerms()),
                    );
                  },
                  child: Text('V E N M O',
                      style: GoogleFonts.staatliches(
                        textStyle: TextStyle(
                            fontSize: 30.0,
                            //fontWeight: FontWeight.bold,
                            color: Colors.white),
                      )),
                  color: Colors.yellow,
                  padding: const EdgeInsets.only(
                      //top: 80, bottom: 80, right: 120, left: 120),
                      top: 60,
                      bottom: 60,
                      right: 118,
                      left: 118),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.01),
              Padding(
                padding: const EdgeInsets.only(right: 30.0, left: 30),
                
                  child: CheckboxListTile(
                    title: Text("T E R M S   O F   S E R V I C E",
                    style: GoogleFonts.staatliches(),
                    ),
                    secondary: Icon(Icons.beach_access),
                    controlAffinity: ListTileControlAffinity.platform,
                    
                    value: _isChecked,
                    activeColor: Colors.green,
                   onChanged: (bool value) {
                      setState(() {
          _isChecked = value;
        });
                    },
                  ),
                
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.03),
              Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 30, left: 30),
                    child: RaisedButton(
                      child: Text("A C T I V A T E   C A M P A I G N",
                          style: GoogleFonts.staatliches(
                          textStyle: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ))),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(2),
                        side: BorderSide(
                          color: Colors.black,
                          width: 0,
                        ),
                      ),
                      onPressed: () => {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ActiveCampaigns()),
                        ),
                      },
                      color: Colors.green,
                      padding: EdgeInsets.fromLTRB(90, 20, 74, 20),
                      splashColor: Colors.grey,
                    ),
                  )),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.11,
              ),
              Container(
                padding: EdgeInsets.only(top: 12),
                decoration: BoxDecoration(
                    /* gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [
                                Colors.blue[900],
                                Colors.blue[500],
                                Colors.blue[400],
                                Colors.blue[500],
                                Colors.blue[900],
                              ],
                            ), */
                    border: Border(
                        top: BorderSide(color: Colors.grey[700], width: 1.0))),
                child: Row(
                  children: [
                    SizedBox(width: 10),
                    FlatButton(
                      child: Icon(Icons.home, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ActiveCampaigns()),
                        );
                      },
                    ),
                    SizedBox(width: 16),
                    FlatButton(
                      child:
                          Icon(Icons.add_circle, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CampaignSearch()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    FlatButton(
                      child: Icon(Icons.card_giftcard,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RedeemGifts()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    FlatButton(
                      child: Icon(Icons.attach_money,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PayStubs()),
                        );
                      },
                    ),
                  ],
                ),
              )
            ]),
      ),
    );
  }
}