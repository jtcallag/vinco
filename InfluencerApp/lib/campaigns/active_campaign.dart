import 'package:InfluencerApp/billing/billing.dart';
import 'package:InfluencerApp/billing/pay_stubs.dart';
import 'package:InfluencerApp/billing/redeem_gifts.dart';
import 'package:InfluencerApp/campaigns/campaign.dart';
import 'package:InfluencerApp/campaigns/campaign_search.dart';
import 'package:InfluencerApp/campaigns/new_campaign.dart';
import 'package:InfluencerApp/screens/sign_in.dart';
import 'package:InfluencerApp/stats/stats.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_rounded_progress_bar/flutter_icon_rounded_progress_bar.dart';
import 'package:flutter_rounded_progress_bar/flutter_rounded_progress_bar.dart';
import 'package:flutter_rounded_progress_bar/rounded_progress_bar_style.dart';



//import 'package:flutter/foundation.dart';
//import 'package:vinco/home_screen.dart';
//import './forgot_password.dart';
//int _page = 0;
//GlobalKey _bottomNavigationKey = GlobalKey();

class ActiveCampaigns extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    return Scaffold(

        /* items: [
              Column(
                children: [
                  FlatButton(
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ActiveCampaigns()),
                        );
                      },
                      child: Icon(
                        Icons.home,
                        size: 35,
                        color: Colors.black,
                      )),
                ],
              ),
              Column(
                children: [
                  FlatButton(
                    color: Colors.transparent,
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewCampaign()),
                      );
                    },
                    child:
                        Icon(Icons.add_circle, size: 35, color: Colors.black),
                  ),
                ],
              ),
              Column(
                children: [
                  FlatButton(
                    color: Colors.transparent,
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => RedeemGifts()),
                      );
                    },
                    child: Icon(Icons.card_giftcard,
                        size: 35, color: Colors.black),
                  ),
                ],
              ), */
        //below is rough navbar

        /* bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.blueAccent,
          currentIndex: 0, // this will be set when a new tab is tapped
          items: [
            BottomNavigationBarItem(
              icon: new Icon(
                Icons.home,
                color: Colors.black,
                size: 35,
              ),
              title: new Text(''),
            ),
            BottomNavigationBarItem(
              icon: new Icon(
                Icons.add_circle,
                color: Colors.black,
                size: 35,
              ),
              title: new Text(''),
            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.card_giftcard,
                  color: Colors.black,
                  size: 35,
                ),
                title: Text(''))
          ],
        ), */

        drawer: Drawer(
            child: new ListView(children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: new Text("Mike Callaghan"),
            accountEmail: new Text("mqcallaghan9@gmail.com"),
            currentAccountPicture: new CircleAvatar(
              backgroundColor: Colors.cyan,
              child: new Text("M"),
            ),
            otherAccountsPictures: <Widget>[
              new CircleAvatar(
                backgroundColor: Colors.white,
                child: new Text("K"),
              )
            ],
          ),
          new ListTile(
              title: new Text("Campaign"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Campaign(),
                        ))
                    //Navigator.of(context).pop(),
                    //Navigator.of(context).pushNamed("/Campaign"),
                  }),
          new ListTile(
              title: new Text("Stats"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Stats(),
                        ))
                  }),
          new ListTile(
              title: new Text("Billing"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Billing(),
                        ))
                    //Navigator.pop(context)
                    //Navigator.pop(
                    // context,
                    // MaterialPageRoute(builder: (context) => Billing()),
                    // )
                    // Navigator.of(context).pop(),
                    // Navigator.of(context).pushNamed("/Billing"),
                  }),
          new ListTile(
              title: new Text("Logout"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Login(),
                        ))
                  }),
          new Divider(),
          new ListTile(
            title: new Text("close"),
            trailing: new Icon(Icons.close),
            onTap: () => Navigator.of(context).pop(),
          ),
        ])),
        body: Padding(
        
          padding: const EdgeInsets.only(top: 20),
          //child: SingleChildScrollView(
          child: Column(children: [
            Padding(
                padding: EdgeInsets.only(
                  right: 0,
                  left: 0,
                  top: 20,
                ),
                child: SingleChildScrollView(
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  //crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    SizedBox(height: 0),
                    Text('C A M P A I G N S',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.staatliches(
                            textStyle: TextStyle(
                          color: Colors.black,
                          fontSize: 30,
                          fontWeight: FontWeight.w500,
                        ))),
                    SizedBox(height: 5),

                    //child:
                    //child:
                    Padding(
                      padding: const EdgeInsets.only(left: 0, right: 0),
                      child: Container(
                        padding: EdgeInsets.only(bottom: 0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border:
                                Border.all(color: Colors.black, width: 0.0)),

                        //color: Colors.grey,
                        height: MediaQuery.of(context).size.height * 0.82,
                       //630
                        child: //ListWheelScrollView(
                            Padding(
                          padding: const EdgeInsets.only(top: 0, bottom: 0),
                          child: ListView(
                              //padding: EdgeInsets.only(top: 0, bottom: 0),
                              //scrollDirection: Axis.horizontal,
                              itemExtent: 124,
                              children: [
                                // SizedBox(height: 2),

                                Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          right: 190,
                                          left: 0,
                                          top: 0,
                                          bottom: 0),
                                      child: Text(
                                        'C O M P A N Y   Y',
                                        style: GoogleFonts.staatliches(
                                          textStyle: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    SizedBox(height: 3),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 160.0),
                                      child: SelectableText(
                                          'www.companyy.com/user1234',
                                          style: GoogleFonts
                                              .firaSansExtraCondensed(
                                            textStyle: TextStyle(
                                              fontSize: 12,
                                            ),
                                          )),
                                    ),
                                    SizedBox(height: 16),
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 40.0,
                                          right: 40,
                                          top: 0,
                                          bottom: 0),
                                      child: Container(
                                        padding:
                                            EdgeInsets.only(top: 0, bottom: 0),
                                        //decoration: BoxDecoration(
                                        //border: Border.all(
                                        // width: 4, color: Colors.black)),
                                        height: 56,
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 0, bottom: 0),
                                          child: RoundedProgressBar(
                                            height: 56,
                                            childCenter: Text(
                                              '6 0 %',
                                              style: GoogleFonts.staatliches(
                                                  textStyle:
                                                      TextStyle(fontSize: 20)),
                                            ),
                                            percent: 60,
                                            //paddingChildLeft: ,

                                            //icon: Padding(
                                            //padding: EdgeInsets.all(8),
                                            //child: Icon(Icons.airline_seat_flat, color: Colors.white)),
                                            style: RoundedProgressBarStyle(

                                                //colorBackgroundIcon:
                                                //Color(0xffc0392b),
                                                colorProgress:
                                                    //linearGradient: LinearGradient(colors: Colors.blue),

                                                    Colors.green,
                                                //colorProgressDark: Colors.grey,
                                                //colorBorder: Colors.black,
                                                backgroundProgress: Colors.grey[100],
                                                borderWidth: 0,
                                                widthShadow: 0),
                                            margin: EdgeInsets.symmetric(
                                                vertical: 0),
                                            borderRadius:
                                                BorderRadius.circular(4),
                                            //percent: percent,
                                          ),
                                        ),
                                        /* child: LinearPercentIndicator(
                                          
                                          
                                          linearGradient: LinearGradient(colors: [
                                            Colors.blue[300],
                                            Colors.blue[600],
                                            Colors.blue[900]
                                          ]),
                                          width: 280,
                                          //MediaQuery.of(context).size.width - 30,
                                          animation: true,
                                          lineHeight: 60.0,
                                          animationDuration: 2000,
                                          
                                          percent: 0.68,
                                          center: Text(
                                            "68.0%",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          // linearStrokeCap: LinearStrokeCap.roundAll,
                                          // progressColor: Colors.blueAccent,
                                        ), */
                                      ),
                                    ),
                                  ],
                                ),

                                SizedBox(height: 0),

                                Column(
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 180),
                                      child: Text('C O M P A N Y   X',
                                          style: GoogleFonts.staatliches(
                                            textStyle: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          )),
                                    ),
                                    SizedBox(height: 3),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 160.0),
                                      child: SelectableText(
                                          'www.companyx.com/user1234',
                                          style: GoogleFonts
                                              .firaSansExtraCondensed(
                                            textStyle: TextStyle(
                                              fontSize: 12,
                                            ),
                                          )),
                                    ),
                                    SizedBox(height: 16),
                                    /* Flexible(
                                        child: Expanded( */
                                            /* child: */ Container(
                                      //decoration: BoxDecoration(
                                      // border: Border.all(
                                      // width: 4, color: Colors.black),
                                      // ),
                                      height: 56,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                            left: 40.0, right: 40, top: 0, bottom: 0),
                                        child: Expanded(
                                          child:  RoundedProgressBar(
                                            height: 56,
                                            childCenter: Text(
                                              '3 0 %',
                                              style: GoogleFonts.staatliches(
                                                  textStyle:
                                                      TextStyle(fontSize: 20)),
                                            ),
                                            percent: 30,
                                            //paddingChildLeft: ,

                                            //icon: Padding(
                                            //padding: EdgeInsets.all(8),
                                            //child: Icon(Icons.airline_seat_flat, color: Colors.white)),
                                            style: RoundedProgressBarStyle(

                                                //colorBackgroundIcon:
                                                //Color(0xffc0392b),
                                                colorProgress:
                                                    //linearGradient: LinearGradient(colors: Colors.blue),

                                                    Colors.green,
                                                //colorProgressDark: Colors.grey,
                                                //colorBorder: Colors.black,
                                                backgroundProgress: Colors.grey[100],
                                                borderWidth: 0,
                                                widthShadow: 0),
                                            margin: EdgeInsets.symmetric(
                                                vertical: 0),
                                            borderRadius:
                                                BorderRadius.circular(4),
                                            //percent: percent,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                /* Padding(
                                        padding: EdgeInsets.only(left: 50.0, right: 50),
                                        child: LinearPercentIndicator(
                                          width: 280,
                                          //MediaQuery.of(context).size.width - 30,
                                          animation: true,
                                          lineHeight: 60.0,
                                          animationDuration: 2000,
                                          percent: 0.9,
                                          center: Text("90.0%"),
                                          linearStrokeCap: LinearStrokeCap.roundAll,
                                          progressColor: Colors.blueAccent,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: 50.0, right: 50),
                                        child: LinearPercentIndicator(
                                          width: 280,
                                          //MediaQuery.of(context).size.width - 30,
                                          animation: true,
                                          lineHeight: 60.0,
                                          animationDuration: 2000,
                                          percent: 0.9,
                                          center: Text("90.0%"),
                                          linearStrokeCap: LinearStrokeCap.roundAll,
                                          progressColor: Colors.blueAccent,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: 50.0, right: 50),
                                        child: LinearPercentIndicator(
                                          width: 280,
                                          //MediaQuery.of(context).size.width - 30,
                                          animation: true,
                                          lineHeight: 60.0,
                                          animationDuration: 2000,
                                          percent: 0.9,
                                          center: Text("90.0%"),
                                          linearStrokeCap: LinearStrokeCap.roundAll,
                                          progressColor: Colors.blueAccent,
                                        ),
                                      ), */
                              ]),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Row(
                      children: [
                        SizedBox(width: 10),
                    FlatButton(
                      child: Icon(Icons.home, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ActiveCampaigns()),
                        );
                      },
                    ),
                    SizedBox(width: 16),
                    FlatButton(
                      child:
                          Icon(Icons.add_circle, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CampaignSearch()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    FlatButton(
                      child: Icon(Icons.card_giftcard,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RedeemGifts()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    FlatButton(
                      child: Icon(Icons.attach_money,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PayStubs()),
                        );
                      },
                    ),
                      ],
                    )

                    /* Stack(children: [
                        Container(
                            margin: EdgeInsets.all(0),
                            child: Flexible(
                                child: Expanded(
                              child: Container(
                                child: Row(
                                  //crossAxisAlignment: Alignment.centerLeft,
                                  children: [
                                    FlatButton(
                                      child: Icon(Icons.home,
                                          size: 35, color: Colors.black),
                                      color: Colors.white,
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  NewCampaign()),
                                        );
                                      },
                                    ),
                                    SizedBox(width: 20),
                                    FlatButton(
                                      child: Icon(Icons.home,
                                          size: 35, color: Colors.black),
                                      color: Colors.white,
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  NewCampaign()),
                                        );
                                      },
                                    )
                                  ],
                                ),

                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: [
                                        Colors.blue[900],
                                        Colors.blue[500],
                                        Colors.blue[400],
                                        Colors.blue[500],
                                        Colors.blue[900],
                                      ],
                                    ),
                                    border: Border(
                                        top: BorderSide(
                                            color: Colors.black, width: 5.0))),
                                //color: Colors.black, width: (20)),

                                //borderRadius: BorderRadius.circular(10.0)

                                padding: const EdgeInsets.only(
                                    left: 300, right: 300, bottom: 58),
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                alignment: Alignment.bottomCenter,
                              ),
                            ))),
                      ]),
                    ]),
              ) */
                  ],
                )

                /* body: Center(
            child: Padding(
              padding: EdgeInsets.only(left: 50.0, right: 50),
              child: LinearPercentIndicator(
                width: 280,
                //MediaQuery.of(context).size.width - 30,
                animation: true,
                lineHeight: 60.0,
                animationDuration: 2000,
                percent: 0.9,
                center: Text("90.0%"),
                linearStrokeCap: LinearStrokeCap.roundAll,
                progressColor: Colors.blue,
              ),
            ), */
//above is working progress bar

                /* child: Column(children: [
              SizedBox(height: 40),
              Container(
                  height: 70,
                  width: 200,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        LinearProgressIndicator(
                          minHeight: 40,
                          //backroundColors: Colors.black,
                          //valueColor: AlwaysStoppedAnimation(Colors.green),
                        ),
                      ]))
            ]), */
                )
          )]),
        ));

    //elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,
  }
}