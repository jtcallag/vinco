import 'package:InfluencerApp/billing/pay_stubs.dart';
import 'package:InfluencerApp/billing/redeem_gifts.dart';
import 'package:InfluencerApp/campaigns/active_campaign.dart';
import 'package:InfluencerApp/campaigns/campaign_search.dart';
import 'package:InfluencerApp/campaigns/new_campaign.dart';
import 'package:InfluencerApp/campaigns/new_campaign2.dart';
import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';

//import 'package:flutter/foundation.dart';
//import 'package:vinco/home_screen.dart';
//import './forgot_password.dart';
//int _page = 0;
//GlobalKey _bottomNavigationKey = GlobalKey();

class CategoryTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    return Scaffold(
      body: Padding(
          //Container(

          padding: EdgeInsets.only(right: 0, left: 0, top: 0, bottom: 0),
          child: Column(
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(height: 40),
                Container(
                  width: 420,
                  child: Text('S H O E S',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.staatliches(
                          textStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 30,
                        fontWeight: FontWeight.w500,
                      ))),
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: Colors.grey[700], width: 1.0))),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.048),

                Column(children: <Widget>[ 
                  Padding(padding: EdgeInsets.only(left: 20, right: 20),),
                 Container(
                      width: 240,
                      child: TextField(
                        onChanged: (String str) {
                          print(str);
                        },

                        //onTap: () {

                        //FocusScope.of(context).requestFocus();

                        //},

                        decoration: InputDecoration(
                          hintText: '                S E A R C H ',

                          fillColor: Colors.grey[50],

                          filled: true,

                          contentPadding: const EdgeInsets.all(16.0),

                          //child: padding(  EdgeInsets.only(

                          // left: 300, right: 300, bottom: 116),

                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.black,
                                width: 1,
                              ),

                              // borderRadius: BorderRadius.all(Radius.circular(30)),

                              borderRadius:
                                  BorderRadius.all(Radius.elliptical(2, 2))),
                        ),
                      )),
                      SizedBox(height: MediaQuery.of(context).size.height * 0.048),
                      Row(
                        
                         children: <Widget>[
                         SizedBox(width: 20),
                         RaisedButton(
                    child: Text("B R A N D   1",
                        style: GoogleFonts.staatliches(
                            textStyle: TextStyle(
                          fontSize: 12,
                          //fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewCampaign()),
                      ),
                    },
                    color: Colors.green,
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
                    splashColor: Colors.grey,
                  ),
                  SizedBox(width: 30),
                         RaisedButton(
                    child: Text("B R A N D   2",
                        style: GoogleFonts.staatliches(
                            textStyle: TextStyle(
                          fontSize: 12,
                          //fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewCampaign()),
                      ),
                    },
                    color: Colors.yellow,
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
                    splashColor: Colors.grey,
                  ),
                  SizedBox(width: 30),
                         RaisedButton(
                    child: Text("B R A N D   3",
                        style: GoogleFonts.staatliches(
                            textStyle: TextStyle(
                          fontSize: 12,
                          //fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewCampaign()),
                      ),
                    },
                    color: Colors.green,
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
                    splashColor: Colors.grey,
                  ),
                          ] ),
                          SizedBox(height: MediaQuery.of(context).size.height * 0.024),
                      Row(
                        
                         children: <Widget>[
                         SizedBox(width: 20),
                         RaisedButton(
                    child: Text("B R A N D   4",
                        style: GoogleFonts.staatliches(
                            textStyle: TextStyle(
                          fontSize: 12,
                          //fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewCampaign()),
                      ),
                    },
                    color: Colors.yellow,
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
                    splashColor: Colors.grey,
                  ),
                  SizedBox(width: 30),
                         RaisedButton(
                    child: Text("B R A N D   5",
                        style: GoogleFonts.staatliches(
                            textStyle: TextStyle(
                          fontSize: 12,
                          //fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewCampaign()),
                      ),
                    },
                    color: Colors.green,
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
                    splashColor: Colors.grey,
                  ),
                  SizedBox(width: 30),
                         RaisedButton(
                    child: Text("B R A N D   6",
                        style: GoogleFonts.staatliches(
                            textStyle: TextStyle(
                          fontSize: 12,
                          //fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewCampaign()),
                      ),
                    },
                    color: Colors.yellow,
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
                    splashColor: Colors.grey,
                  ),
                          ] ),
                          SizedBox(height: MediaQuery.of(context).size.height * 0.024),
                      Row(
                        
                         children: <Widget>[
                         SizedBox(width: 20),
                         RaisedButton(
                    child: Text("B R A N D   7",
                        style: GoogleFonts.staatliches(
                            textStyle: TextStyle(
                          fontSize: 12,
                          //fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewCampaign()),
                      ),
                    },
                    color: Colors.green,
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
                    splashColor: Colors.grey,
                  ),
                  SizedBox(width: 30),
                         RaisedButton(
                    child: Text("B R A N D   8",
                        style: GoogleFonts.staatliches(
                            textStyle: TextStyle(
                          fontSize: 12,
                          //fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewCampaign()),
                      ),
                    },
                    color: Colors.yellow,
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
                    splashColor: Colors.grey,
                  ),
                  SizedBox(width: 30),
                         RaisedButton(
                    child: Text("B R A N D   9",
                        style: GoogleFonts.staatliches(
                            textStyle: TextStyle(
                          fontSize: 12,
                          //fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewCampaign()),
                      ),
                    },
                    color: Colors.green,
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
                    splashColor: Colors.grey,
                  ),
                          ] ),
                           SizedBox(height: MediaQuery.of(context).size.height * 0.316),
            
              Container(
                padding: EdgeInsets.only(top: 12),
                decoration: BoxDecoration(
                    /* gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Colors.blue[900],
                                  Colors.blue[500],
                                  Colors.blue[400],
                                  Colors.blue[500],
                                  Colors.blue[900],
                                ],
                              ), */
                    border: Border(
                        top: BorderSide(color: Colors.grey[700], width: 1.0))),
                child: Row(
                  children: [
                    SizedBox(width: 10),
                    FlatButton(
                      child: Icon(Icons.home, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ActiveCampaigns()),
                        );
                      },
                    ),
                    SizedBox(width: 16),
                    FlatButton(
                      child:
                          Icon(Icons.add_circle, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CampaignSearch()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    FlatButton(
                      child: Icon(Icons.card_giftcard,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RedeemGifts()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    FlatButton(
                      child: Icon(Icons.attach_money,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PayStubs()),
                        );
                      },
                    ),
                  ],
                ),
              )
],


)
              ])));}}