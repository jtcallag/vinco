import 'dart:io';

import 'package:InfluencerApp/billing/billing.dart';
import 'package:InfluencerApp/billing/pay_stubs.dart';
import 'package:InfluencerApp/billing/redeem_gifts.dart';
import 'package:InfluencerApp/campaigns/active_campaign.dart';
import 'package:InfluencerApp/campaigns/campaign.dart';
import 'package:InfluencerApp/campaigns/campaign_search.dart';
import 'package:InfluencerApp/campaigns/new_campaign2.dart';
import 'package:InfluencerApp/screens/sign_in.dart';
import 'package:InfluencerApp/stats/stats.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


//import 'package:flutter/foundation.dart';
//import 'package:vinco/home_screen.dart';
//import './forgot_password.dart';
int _page = 1;
GlobalKey _bottomNavigationKey = GlobalKey();

class NewCampaign extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    return Scaffold(
        backgroundColor: Colors.white,
        /* appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text("NEW CAMPAIGNS"),
        ), //elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0, */
        /* bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.blueAccent,
          currentIndex: 0, // this will be set when a new tab is tapped
          items: [
            BottomNavigationBarItem(
              icon: new Icon(
                Icons.home,
                color: Colors.black,
                size: 35,
              ),
              title: new Text(''),
            ),
            BottomNavigationBarItem(
              icon: new Icon(
                Icons.add_circle,
                color: Colors.black,
                size: 35,
              ),
              title: new Text(''),
            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.card_giftcard,
                  color: Colors.black,
                  size: 35,
                ),
                title: Text(''))
          ],
        ), */
        drawer: Drawer(
            child: new ListView(children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: new Text("Mike Callaghan"),
            accountEmail: new Text("mqcallaghan9@gmail.com"),
            currentAccountPicture: new CircleAvatar(
              backgroundColor: Colors.cyan,
              child: new Text("M"),
            ),
            otherAccountsPictures: <Widget>[
              new CircleAvatar(
                backgroundColor: Colors.white,
                child: new Text("K"),
              )
            ],
          ),
          new ListTile(
              title: new Text("Campaign"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Campaign(),
                        ))
                    //Navigator.of(context).pop(),
                    //Navigator.of(context).pushNamed("/Campaign"),
                  }),
          new ListTile(
              title: new Text("Stats"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Stats(),
                        ))
                  }),
          new ListTile(
              title: new Text("Billing"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Billing(),
                        ))
                    //Navigator.pop(context)
                    //Navigator.pop(
                    // context,
                    // MaterialPageRoute(builder: (context) => Billing()),
                    // )
                    // Navigator.of(context).pop(),
                    // Navigator.of(context).pushNamed("/Billing"),
                  }),
          new ListTile(
              title: new Text("Logout"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Login(),
                        ))
                  }),
          new Divider(),
          new ListTile(
            title: new Text("close"),
            trailing: new Icon(Icons.close),
            onTap: () => Navigator.of(context).pop(),
          ),
        ])),

        /* body: Row(
        children: [
          RaisedButton(
            child: Text("COLORADO HONEY",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                )),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
              side: BorderSide(
                color: Colors.black,
                width: 6,
              ),
            ),
            onPressed: () => {},
            color: Colors.blue[400],
            padding: EdgeInsets.fromLTRB(40, 40, 40, 40),
            splashColor: Colors.grey,
          ),
        ],
      ), */

        /* body: Padding(
          //Container(

          padding: EdgeInsets.only(right: 30, left: 30, top: 2, bottom: 0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                // SizedBox(height: 1),
                Text('CATEGORY',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontWeight: FontWeight.w300,
                    )),
                // SizedBox(height: 1),

                //child:
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Colors.white, width: 0.0)),
                  //color: Colors.grey,
                  height: 200,
                  padding: EdgeInsets.only(
                    top: 5,
                    bottom: 5,
                    right: 5,
                    left: 5,
                  ),

                  child: //ListWheelScrollView(
                      ListView(
                          scrollDirection: Axis.horizontal,
                          itemExtent: 100,
                          children: [
                        //SizedBox(height: 10),
                        Padding(
                            padding: const EdgeInsets.only(
                                right: 5, left: 5, bottom: 5, top: 5),
                            child: RaisedButton(
                              child: Text("",
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  )),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 3,
                                ),
                              ),
                              onPressed: () => {},
                              color: Colors.blue[400],
                              padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                              splashColor: Colors.grey,
                            )),
                        Padding(
                            padding: const EdgeInsets.only(
                                right: 5, left: 5, bottom: 5, top: 5),
                            child: RaisedButton(
                              child: Text("",
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  )),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 3,
                                ),
                              ),
                              onPressed: () => {},
                              color: Colors.blue[400],
                              padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                              splashColor: Colors.grey,
                            )),
                        Padding(
                            padding: const EdgeInsets.only(
                                right: 5, left: 5, bottom: 5, top: 5),
                            child: RaisedButton(
                              child: Text("",
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  )),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 3,
                                ),
                              ),
                              onPressed: () => {},
                              color: Colors.blue[400],
                              padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                              splashColor: Colors.grey,
                            )),
                        Padding(
                            padding: const EdgeInsets.only(
                                right: 5, left: 5, bottom: 5, top: 5),
                            child: RaisedButton(
                              child: Text("",
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  )),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 3,
                                ),
                              ),
                              onPressed: () => {},
                              color: Colors.blue[400],
                              padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                              splashColor: Colors.grey,
                            )),
                        Padding(
                            padding: const EdgeInsets.only(
                                right: 5, left: 5, bottom: 5, top: 5),
                            child: //Container(

                                //height: 20,
                                //width: MediaQuery.of(context).size.width,
                                //width: 40,
                                //color: Colors.blue,
                                //),
                                RaisedButton(
                              child: Text("",
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  )),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 3,
                                ),
                              ),
                              onPressed: () => {},
                              color: Colors.blue[400],
                              padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                              splashColor: Colors.grey,
                            )),
                        Padding(
                            padding: const EdgeInsets.only(
                                right: 5, left: 5, bottom: 5, top: 5),
                            child: RaisedButton(
                              child: Text("",
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  )),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 3,
                                ),
                              ),
                              onPressed: () => {},
                              color: Colors.blue[400],
                              padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                              splashColor: Colors.grey,
                            )),
                        Padding(
                            padding: const EdgeInsets.only(
                                right: 5, left: 5, bottom: 5, top: 5),
                            child: RaisedButton(
                              child: Text("",
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  )),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 3,
                                ),
                              ),
                              onPressed: () => {},
                              color: Colors.blue[400],
                              padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                              splashColor: Colors.grey,
                            )),
                        Padding(
                            padding: const EdgeInsets.only(
                                right: 5, left: 5, bottom: 5, top: 5),
                            child: RaisedButton(
                              child: Text("",
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  )),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 3,
                                ),
                              ),
                              onPressed: () => {},
                              color: Colors.blue[400],
                              padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                              splashColor: Colors.grey,
                            )),
                      ]),
                ),
              ]),
        ) */

        body: SingleChildScrollView(
          child: Center(
            child: IntrinsicWidth(
                child: Column(mainAxisAlignment: MainAxisAlignment.start,
                    //crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
              SizedBox(height: 40),
              Container(
                width: 420,
                child: Text('N E W   C A M P A I G N',
                    textAlign: TextAlign.center,
                    style: GoogleFonts.staatliches(
                        textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontWeight: FontWeight.w500,
                    ))),
                    
                decoration: BoxDecoration(
                  
                    border: Border(
                        bottom: BorderSide(color: Colors.grey[700], width: 1.0))),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.064,
                width: 150,
              ),
              FlatButton(
                //shape: //RoundedRectangleBorder(
                //borderRadius: BorderRadius.circular(20)),
                // Border.all(width: 10, color: Colors.black),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ActiveCampaigns()),
                  );
                },
                child: Text(
                  'L O G O',
                  style: GoogleFonts.staatliches(
                  textStyle: TextStyle(
                      fontSize: 32.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black),
                )),
                color: Colors.white,
                padding: const EdgeInsets.only(
                    top: 40, bottom: 40, right: 40, left: 40),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.12),
              //Flexible(
              // child: Expanded(
              //child:
              Padding(
                padding: const EdgeInsets.only(right: 30.0, left: 30),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 2),
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.circular(4)),

                  // border: //Border(
                  // Border.all(width: 5, color: Colors.black)),

                  // top: BorderSide(color: Colors.black, width: 20.0))),
                  //color: Colors.black, width: (20)),

                  //borderRadius: BorderRadius.circular(10.0)

                  padding: const EdgeInsets.only(
                      left: 20, right: 170, bottom: 170, top: 20),
                  // margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  alignment: Alignment.bottomCenter,
                  child: Text('D E S C R I P T I O N',
                      textAlign: TextAlign.left,
                      style: GoogleFonts.staatliches(
                          textStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        //fontWeight: FontWeight.bold,
                      ))),
                  //child: Text(
                  // 'Description',
                ),
              ), //)
              SizedBox(height: MediaQuery.of(context).size.height * 0.03),
              Align(
                alignment: Alignment.centerRight,
              
                child: Padding(
                  padding: const EdgeInsets.only(right: 30),
                  child: RaisedButton(
                    child: Text("S E E   M O R E",
                        style: GoogleFonts.staatliches(
                            textStyle: TextStyle(
                          fontSize: 20,
                          //fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewCampaign2()),
                      ),
                    },
                    color: Colors.green,
                    padding: EdgeInsets.fromLTRB(40, 20, 40, 20),
                    splashColor: Colors.grey,
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.078,
              ),
              Container(
                padding: EdgeInsets.only(top: 12),
                decoration: BoxDecoration(
                    /* gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Colors.blue[900],
                                  Colors.blue[500],
                                  Colors.blue[400],
                                  Colors.blue[500],
                                  Colors.blue[900],
                                ],
                              ), */
                    border: Border(
                        top: BorderSide(color: Colors.grey[700], width: 1.0))),
                child: Row(
                  children: [
                    SizedBox(width: 10),
                    FlatButton(
                      child: Icon(Icons.home, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ActiveCampaigns()),
                        );
                      },
                    ),
                    SizedBox(width: 16),
                    FlatButton(
                      child:
                          Icon(Icons.add_circle, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CampaignSearch()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    FlatButton(
                      child: Icon(Icons.card_giftcard,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RedeemGifts()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    FlatButton(
                      child: Icon(Icons.attach_money,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PayStubs()),
                        );
                      },
                    ),
                  ],
                ),
              )
            ])))));
  }
}
