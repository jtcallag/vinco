import 'package:flutter/material.dart';
import "package:meta/meta.dart";
import "package:InfluencerApp/environment/flavor.dart";

class AppConfig extends InheritedWidget {
  final String apiUrl;
  final Flavor flavor;
  final Widget child;

  AppConfig(
      {@required this.apiUrl, @required this.flavor, @required this.child});

  static AppConfig of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType();
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => false;
}
