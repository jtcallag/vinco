import 'package:InfluencerApp/billing/billing.dart';
import 'package:InfluencerApp/billing/paystubs2.dart';
import 'package:InfluencerApp/billing/redeem_gifts.dart';
import 'package:InfluencerApp/campaigns/active_campaign.dart';
import 'package:InfluencerApp/campaigns/campaign.dart';
import 'package:InfluencerApp/campaigns/campaign_search.dart';
import 'package:InfluencerApp/campaigns/new_campaign.dart';
import 'package:InfluencerApp/screens/sign_in.dart';
import 'package:InfluencerApp/stats/stats.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


//import 'package:flutter/foundation.dart';
//import 'package:vinco/home_screen.dart';
//import './forgot_password.dart';

class PayStubs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    return Scaffold(
        backgroundColor: Colors.white,
        /* appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text("PAY STUBS"),
        ), //elevati */ /* on: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0, */
        drawer: Drawer(
            child: new ListView(children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: new Text("Mike Callaghan"),
            accountEmail: new Text("mqcallaghan9@gmail.com"),
            currentAccountPicture: new CircleAvatar(
              backgroundColor: Colors.cyan,
              child: new Text("M"),
            ),
            otherAccountsPictures: <Widget>[
              new CircleAvatar(
                backgroundColor: Colors.white,
                child: new Text("K"),
              )
            ],
          ),
          new ListTile(
              title: new Text("Campaign"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Campaign(),
                        ))
                    //Navigator.of(context).pop(),
                    //Navigator.of(context).pushNamed("/Campaign"),
                  }),
          new ListTile(
              title: new Text("Stats"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Stats(),
                        ))
                  }),
          new ListTile(
              title: new Text("Billing"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Billing(),
                        ))
                    //Navigator.pop(context)
                    //Navigator.pop(
                    // context,
                    // MaterialPageRoute(builder: (context) => Billing()),
                    // )
                    // Navigator.of(context).pop(),
                    // Navigator.of(context).pushNamed("/Billing"),
                  }),
          new ListTile(
              title: new Text("Logout"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Login(),
                        ))
                  }),
          new Divider(),
          new ListTile(
            title: new Text("close"),
            trailing: new Icon(Icons.close),
            onTap: () => Navigator.of(context).pop(),
          ),
        ])),
        body: Padding(
          //Container(

          padding: EdgeInsets.only(right: 0, left: 0, top: 0, bottom: 0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(height: 30),
                Container(
                  width: 420,
                  child: Text('P a y   S t u b s',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.staatliches(
                          textStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 30,
                        fontWeight: FontWeight.w500,
                      ))),
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: Colors.white, width: 1.0))),
                ),
                SizedBox(height: 10),

                //child:
                //child:
                Padding(
                  padding: const EdgeInsets.only(right: 0.0, left: 0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.black, width: 0.0)),

                    //color: Colors.grey,
                    height: MediaQuery.of(context).size.height * 0.8,

                    child: //ListWheelScrollView(
                        ListView(
                            //padding: EdgeInsets.only(bottom: 0, top: 0),

                            //scrollDirection: Axis.horizontal,
                            itemExtent: 100,
                            children: [
                          // SizedBox(height: 2),
                          Padding(
                              padding: const EdgeInsets.only(
                                  right: 5, left: 5, bottom: 5, top: 5),
                              child: RaisedButton(
                                child: Text("2 7 . 4 8   ( 8 / 2 2 / 2 0 )",
                                    style: GoogleFonts.staatliches(
                                    textStyle: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.green,
                                    ))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2),
                                  side: BorderSide(
                                    color: Colors.green,
                                    width: 0,
                                  ),
                                ),
                                onPressed: () => {Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PayStubs2()),
                      )},
                                color: Colors.green[100],
                                padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                                splashColor: Colors.grey,
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  right: 5, left: 5, bottom: 5, top: 5),
                              child: RaisedButton(
                                child: Text("2 7 . 4 8   ( 8 / 2 2 / 2 0 )",
                                    style: GoogleFonts.staatliches(
                                    textStyle: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2),
                                  side: BorderSide(
                                    color: Colors.green,
                                    width: 0,
                                  ),
                                ),
                                onPressed: () => {Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PayStubs2()),
                      )},
                                color: Colors.green,
                                padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                                splashColor: Colors.grey,
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  right: 5, left: 5, bottom: 5, top: 5),
                              child: RaisedButton(
                                child: Text("2 7 . 4 8   ( 8 / 2 2 / 2 0 )",
                                    style: GoogleFonts.staatliches(
                                    textStyle: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.green,
                                    ))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2),
                                  side: BorderSide(
                                    color: Colors.green,
                                    width: 0,
                                  ),
                                ),
                                onPressed: () => {Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PayStubs2()),
                      )},
                                color: Colors.green[100],
                                padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                                splashColor: Colors.grey,
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  right: 5, left: 5, bottom: 5, top: 5),
                              child: RaisedButton(
                                child: Text("2 7 . 4 8   ( 8 / 2 2 / 2 0 )",
                                    style: GoogleFonts.staatliches(
                                    textStyle: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2),
                                  side: BorderSide(
                                    color: Colors.green,
                                    width: 0,
                                  ),
                                ),
                                onPressed: () => {Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PayStubs2()),
                      )},
                                color: Colors.green,
                                padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                                splashColor: Colors.grey,
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  right: 5, left: 5, bottom: 5, top: 5),
                              child: //Container(

                                  //height: 20,
                                  //width: MediaQuery.of(context).size.width,
                                  //width: 40,
                                  //color: Colors.blue,
                                  //),
                                  RaisedButton(
                                child: Text("2 7 . 4 8   ( 8 / 2 2 / 2 0 )",
                                    style: GoogleFonts.staatliches(
                                    textStyle: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.green,
                                    ))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2),
                                  side: BorderSide(
                                    color: Colors.green,
                                    width: 0,
                                  ),
                                ),
                                onPressed: () => {Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PayStubs2()),
                      )},
                                color: Colors.green[100],
                                padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                                splashColor: Colors.grey,
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  right: 5, left: 5, bottom: 5, top: 5),
                              child: RaisedButton(
                                child: Text("2 7 . 4 8   ( 8 / 2 2 / 2 0 )",
                                    style: GoogleFonts.staatliches(
                                    textStyle: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2),
                                  side: BorderSide(
                                    color: Colors.green,
                                    width: 0,
                                  ),
                                ),
                                onPressed: () => {Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PayStubs2()),
                      )},
                                color: Colors.green,
                                padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                                splashColor: Colors.grey,
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  right: 5, left: 5, bottom: 5, top: 5),
                              child: RaisedButton(
                                child: Text("2 7 . 4 8   ( 8 / 2 2 / 2 0 )",
                                    style: GoogleFonts.staatliches(
                                    textStyle: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.green,
                                    ))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2),
                                  side: BorderSide(
                                    color: Colors.green,
                                    width: 0,
                                  ),
                                ),
                                onPressed: () => {Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PayStubs2()),
                      )},
                                color: Colors.green[100],
                                padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                                splashColor: Colors.grey,
                              )),
                          Padding(
                              padding: const EdgeInsets.only(
                                  right: 5, left: 5, bottom: 5, top: 5),
                              child: RaisedButton(
                                child: Text("2 7 . 4 8   ( 8 / 2 2 / 2 0 )",
                                    style: GoogleFonts.staatliches(
                                    textStyle: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2),
                                  side: BorderSide(
                                    color: Colors.green,
                                    width: 0,
                                  ),
                                ),
                                onPressed: () => {Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PayStubs2()),
                      )},
                                color: Colors.green,
                                padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                                splashColor: Colors.grey,
                              )),
                        ]),
                  ),
                ),
                SizedBox(height: 0),
                Padding(
                  padding: const EdgeInsets.only(right: 0, left: 0),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 12),
                        decoration: BoxDecoration(
                            /* gradient: LinearGradient(
                                      begin: Alignment.centerLeft,
                                      end: Alignment.centerRight,
                                      colors: [
                                        Colors.blue[900],
                                        Colors.blue[500],
                                        Colors.blue[400],
                                        Colors.blue[500],
                                        Colors.blue[900],
                                      ],
                                    ), */
                            border: Border(
                                top: BorderSide(
                                    color: Colors.grey[700], width: 1.0))),
                        child: Row(
                          children: [
                            SizedBox(width: 10),
                    FlatButton(
                      child: Icon(Icons.home, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ActiveCampaigns()),
                        );
                      },
                    ),
                    SizedBox(width: 16),
                    FlatButton(
                      child:
                          Icon(Icons.add_circle, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CampaignSearch()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    FlatButton(
                      child: Icon(Icons.card_giftcard,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RedeemGifts()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    FlatButton(
                      child: Icon(Icons.attach_money,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PayStubs()),
                        );
                      },
                    ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ]),
        ));
  }
}