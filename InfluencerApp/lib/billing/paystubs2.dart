import 'package:InfluencerApp/billing/billing.dart';
import 'package:InfluencerApp/billing/pay_stubs.dart';
import 'package:InfluencerApp/billing/redeem_gifts.dart';
import 'package:InfluencerApp/campaigns/active_campaign.dart';
import 'package:InfluencerApp/campaigns/campaign.dart';
import 'package:InfluencerApp/campaigns/campaign_search.dart';
import 'package:InfluencerApp/campaigns/new_campaign.dart';
import 'package:InfluencerApp/screens/sign_in.dart';
import 'package:InfluencerApp/stats/stats.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:InfluencerApp/stats/chart1.dart';
import 'package:InfluencerApp/stats/chart2.dart';


//import 'package:flutter/foundation.dart';
//import 'package:vinco/home_screen.dart';
//import './forgot_password.dart';

final data = [55.0, 90.0, 50.0, 40.0, 35.0, 55.0, 70.0, 100.0];

class PayStubs2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    return Scaffold(
        backgroundColor: Colors.white,
        /* appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text("PAY STUBS"),
        ), //elevati */ /* on: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0, */
        drawer: Drawer(
            child: new ListView(children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: new Text("Mike Callaghan"),
            accountEmail: new Text("mqcallaghan9@gmail.com"),
            currentAccountPicture: new CircleAvatar(
              backgroundColor: Colors.cyan,
              child: new Text("M"),
            ),
            otherAccountsPictures: <Widget>[
              new CircleAvatar(
                backgroundColor: Colors.white,
                child: new Text("K"),
              )
            ],
          ),
          new ListTile(
              title: new Text("Campaign"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Campaign(),
                        ))
                    //Navigator.of(context).pop(),
                    //Navigator.of(context).pushNamed("/Campaign"),
                  }),
          new ListTile(
              title: new Text("Stats"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Stats(),
                        ))
                  }),
          new ListTile(
              title: new Text("Billing"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Billing(),
                        ))
                    //Navigator.pop(context)
                    //Navigator.pop(
                    // context,
                    // MaterialPageRoute(builder: (context) => Billing()),
                    // )
                    // Navigator.of(context).pop(),
                    // Navigator.of(context).pushNamed("/Billing"),
                  }),
          new ListTile(
              title: new Text("Logout"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Login(),
                        ))
                  }),
          new Divider(),
          new ListTile(
            title: new Text("close"),
            trailing: new Icon(Icons.close),
            onTap: () => Navigator.of(context).pop(),
          ),
        ])),
        body: Padding(
          //Container(
               
          padding: EdgeInsets.only(right: 0, left: 0, top: 0, bottom: 0),
          child: SingleChildScrollView(
          child: Column(
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                Container(
                  width: 420,
                  child: Text('B R A N D   N A M E',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.staatliches(
                          textStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 30,
                        fontWeight: FontWeight.w500,
                      ))),
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: Colors.black, width: 1.0))),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.11),
                Text('C A M P A I G N   S T A R T E D   O N   0 2 / 1 7 / 2 1',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.staatliches(
                          textStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                      ))),
                      SizedBox(height: 0),
                      
                  Container(
                  height: 220,
                  width: 150,
                  //depth: 12,
                  //spread: 12,
                  //borderRadius: 16,
                  child: Padding(
                    padding: const EdgeInsets.only(
                      left: 16,
                      right: 16,
                      top: 16,
                    ),
                    child: Column(
                        //crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.w900,
                                color: Colors.black),
                          ),
                          Text(
                            "",
                            style: TextStyle(
                                fontSize: 30,
                                fontWeight: FontWeight.w900,
                                color: Colors.black),
                          ),
                          SizedBox(height: 8),
                          Chart(
                            data: data,
                          ),
                        ]),
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                Row( children: [
                        SizedBox(width: 32),

                        RaisedButton(
                                child: Text("W E E K",
                                    style: GoogleFonts.staatliches(
                                    textStyle: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2),
                                  side: BorderSide(
                                    color: Colors.black,
                                    width: 0,
                                  ),
                                ),
                                onPressed: () => {},
                                color: Colors.green,
                                padding: EdgeInsets.fromLTRB(32, 12, 32, 12),
                                splashColor: Colors.grey,
                              ),
                              SizedBox(width: 20),

                        RaisedButton(
                                child: Text("M O N T H",
                                    style: GoogleFonts.staatliches(
                                    textStyle: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2),
                                  side: BorderSide(
                                    color: Colors.black,
                                    width: 0,
                                  ),
                                ),
                                onPressed: () => {},
                                color: Colors.green,
                                padding: EdgeInsets.fromLTRB(30, 12, 30, 12),
                                splashColor: Colors.grey,
                              ),
                              SizedBox(width: 20),

                        RaisedButton(
                                child: Text("Y E A R",
                                    style: GoogleFonts.staatliches(
                                    textStyle: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                    ))),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(2),
                                  side: BorderSide(
                                    color: Colors.black,
                                    width: 0,
                                  ),
                                ),
                                onPressed: () => {},
                                color: Colors.green,
                                padding: EdgeInsets.fromLTRB(32, 12, 32, 12),
                                splashColor: Colors.grey,
                              )
                      ]),
                SizedBox(height: MediaQuery.of(context).size.height * 0.062),
                Text('           S A L E S   M A D E :',
                      textAlign: TextAlign.left,
                      style: GoogleFonts.staatliches(
                          textStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                      ))),
                      SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                      Text('            T O T A L   D O L L A R S   E A R N E D :',
                      textAlign: TextAlign.left,
                      style: GoogleFonts.staatliches(
                          textStyle: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                      ))),

                      SizedBox(height: MediaQuery.of(context).size.height * 0.15),
                      Container(
                padding: EdgeInsets.only(top: 12),
                decoration: BoxDecoration(
                    /* gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Colors.blue[900],
                                  Colors.blue[500],
                                  Colors.blue[400],
                                  Colors.blue[500],
                                  Colors.blue[900],
                                ],
                              ), */
                    border: Border(
                        top: BorderSide(color: Colors.grey[700], width: 1.0))),
                child: Row(
                  children: [
                    SizedBox(width: MediaQuery.of(context).size.width * 0.02,),
                    FlatButton(
                      child: Icon(Icons.home, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ActiveCampaigns()),
                        );
                      },
                    ),
                    SizedBox(width: MediaQuery.of(context).size.width * 0.04,),
                    FlatButton(
                      child:
                          Icon(Icons.add_circle, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CampaignSearch()),
                        );
                      },
                    ),
                    SizedBox(width: MediaQuery.of(context).size.width * 0.04,),
                    FlatButton(
                      child: Icon(Icons.card_giftcard,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RedeemGifts()),
                        );
                      },
                    ),
                   SizedBox(width: MediaQuery.of(context).size.width * 0.04,),
                    FlatButton(
                      child: Icon(Icons.attach_money,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PayStubs()),
                        );
                      },
                    ),
                  ],
                ),
              )


                ]
                )
                )
                ))
                ;
                }
                }