import 'package:InfluencerApp/billing/billing.dart';
import 'package:InfluencerApp/billing/redeem_gifts.dart';
import 'package:InfluencerApp/campaigns/campaign.dart';
import 'package:InfluencerApp/screens/sign_in.dart';
import 'package:InfluencerApp/stats/stats.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

//import 'package:flutter/foundation.dart';
//import 'package:vinco/home_screen.dart';
//import './forgot_password.dart';

class BillingSetup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    return Scaffold(
        backgroundColor: Colors.grey[600],
        //appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
         // title: Text("SETUP BILLING"),
        //), //elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,
        drawer: Drawer(
            child: new ListView(children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: new Text("Mike Callaghan"),
            accountEmail: new Text("mqcallaghan9@gmail.com"),
            currentAccountPicture: new CircleAvatar(
              backgroundColor: Colors.cyan,
              child: new Text("M"),
            ),
            otherAccountsPictures: <Widget>[
              new CircleAvatar(
                backgroundColor: Colors.white,
                child: new Text("K"),
              )
            ],
          ),
          new ListTile(
              title: new Text("Campaign"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Campaign(),
                        ))
                    //Navigator.of(context).pop(),
                    //Navigator.of(context).pushNamed("/Campaign"),
                  }),
          new ListTile(
              title: new Text("Stats"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Stats(),
                        ))
                  }),
          new ListTile(
              title: new Text("Billing"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Billing(),
                        ))
                    //Navigator.pop(context)
                    //Navigator.pop(
                    // context,
                    // MaterialPageRoute(builder: (context) => Billing()),
                    // )
                    // Navigator.of(context).pop(),
                    // Navigator.of(context).pushNamed("/Billing"),
                  }),
          new ListTile(
              title: new Text("Logout"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Login(),
                        ))
                  }),
          new Divider(),
          new ListTile(
            title: new Text("close"),
            trailing: new Icon(Icons.close),
            onTap: () => Navigator.of(context).pop(),
          ),
        ])),
        body: Container(
          margin: EdgeInsets.all(0),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.grey[50],
                Colors.grey[50],
                Colors.grey[50],
                Colors.grey[50],
              ],
            ),
          ),
          child: Column(
            children: [
              SizedBox(height: 40),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    border: Border.all(width: 0, color: Colors.black),
                    color: Colors.white),
                margin:
                    EdgeInsets.only(right: 40, left: 40, top: 15, bottom: 5),
                child: Padding(
                    padding: const EdgeInsets.only(
                        top: 10, right: 0, left: 0, bottom: 0),
                    child: Column(
                      children: [
                        Align(
                            alignment: Alignment.center,
                            child: Container(
                              padding: EdgeInsets.only(
                                  left: 0, right: 0, top: 0, bottom: 0),
                              child: Text('S e t   U p   B i l l i n g',
                                  textAlign: TextAlign.right,
                                  style: GoogleFonts.staatliches(
                                  textStyle: TextStyle(
                                    color: Colors.black,
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                  ))),
                            )),
                        SizedBox(height: 5),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },

                              //onTap: () {
                              //FocusScope.of(context).requestFocus();
                              //},
                              decoration: InputDecoration(
                                hintText: 'PAYPAL USERNAME',
                                fillColor: Colors.grey[300],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(2, 2))),
                              ),
                            )),
                        SizedBox(height: 5),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'Confirm EMAIL',
                                fillColor: Colors.grey[300],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(2, 2))),
                              ),
                            )),
                        SizedBox(height: 20),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'PASSWORD',
                                fillColor: Colors.grey[300],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(2, 2))),
                              ),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'CONFIRM PASSWORD',
                                fillColor: Colors.grey[300],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(2, 2))),
                              ),
                            )),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'USERNAME',
                                fillColor: Colors.grey[300],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(2, 2))),
                              ),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'FULL NAME',
                                fillColor: Colors.grey[300],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(2, 2))),
                              ),
                            )),
                        SizedBox(
                          height: 5,
                        ),
                        Container(
                            width: 280,
                            child: TextField(
                              onChanged: (String str) {
                                print(str);
                              },
                              decoration: InputDecoration(
                                hintText: 'PHONE NUMBER',
                                fillColor: Colors.grey[300],

                                filled: true,

                                contentPadding: const EdgeInsets.all(16.0),

                                //child: padding(  EdgeInsets.only(

                                // left: 300, right: 300, bottom: 116),

                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                      width: 0,
                                    ),

                                    // borderRadius: BorderRadius.all(Radius.circular(30)),

                                    borderRadius: BorderRadius.all(
                                        Radius.elliptical(2, 2))),
                              ),
                            )),
                        SizedBox(height: 20),
                        Align(
                            alignment: Alignment.center,
                            child: RaisedButton(
                              child: Text("E n t e r",
                                  style: GoogleFonts.staatliches(
                                  textStyle: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ))),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(2),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 0,
                                ),
                              ),
                              onPressed: () => {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => RedeemGifts()),
                                ),
                              },
                              color: Colors.green,
                              padding: EdgeInsets.fromLTRB(110, 15, 110, 15),
                              splashColor: Colors.grey,
                            )),
                        SizedBox(height: 20),
                      ],

                      //SizedBox.expand(

                      //Expanded(

                      //child:

                      /* Flexible(
                          child: Expanded(
                              child: Container(
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight,
                                        colors: [
                                          Colors.blue[900],
                                          Colors.blue[500],
                                          Colors.blue[400],
                                          Colors.blue[500],
                                          Colors.blue[900],
                                        ],
                                      ),
                                      //color: Colors.lightBlue,
                                      border: Border(
                                          top: BorderSide(
                                              color: Colors.black, width: 20.0))),
                                  //color: Colors.black, width: (20)),

                                  //borderRadius: BorderRadius.circular(10.0)

                                  padding: const EdgeInsets.only(
                                      left: 300, right: 300, bottom: 116),
                                  margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                  alignment: Alignment.bottomCenter,
                                  child: Text(
                                    '',
                                  )))) */
                    )),
              ),
              SizedBox(height: 40),
              Flexible(
                  child: Expanded(
                      child: Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Colors.green,
                                  Colors.green,
                                  Colors.green,
                                  Colors.green,
                                  Colors.green,
                                ],
                              ),
                              //color: Colors.lightBlue,
                              border: Border(
                                  top: BorderSide(
                                      color: Colors.black, width: 0))),
                          //color: Colors.black, width: (20)),

                          //borderRadius: BorderRadius.circular(10.0)

                          padding: const EdgeInsets.only(
                              left: 300, right: 300, bottom: 116),
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          alignment: Alignment.bottomCenter,
                          child: Text(
                            '',
                          ))))
            ],
          ),
        ));
  }
}
