import 'package:InfluencerApp/billing/billing.dart';
import 'package:InfluencerApp/billing/pay_stubs.dart';
import 'package:InfluencerApp/billing/setup_billing.dart';
import 'package:InfluencerApp/campaigns/active_campaign.dart';
import 'package:InfluencerApp/campaigns/campaign.dart';
import 'package:InfluencerApp/campaigns/campaign_search.dart';
import 'package:InfluencerApp/campaigns/new_campaign.dart';
import 'package:InfluencerApp/campaigns/new_campaign2.dart';
import 'package:InfluencerApp/screens/sign_in.dart';
import 'package:InfluencerApp/stats/stats.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


//import 'package:flutter/foundation.dart';
//import 'package:vinco/home_screen.dart';
//import './forgot_password.dart';
GlobalKey _bottomNavigationKey = GlobalKey();

class RedeemGifts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var _blankFocusNode = newFocusNode();
    return Scaffold(
        backgroundColor: Colors.white,
        /* bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.blueAccent,
          currentIndex: 0, // this will be set when a new tab is tapped
          items: [
            BottomNavigationBarItem(
              icon: new Icon(
                Icons.home,
                color: Colors.black,
                size: 35,
              ),
              title: new Text(''),
            ),
            BottomNavigationBarItem(
              icon: new Icon(
                Icons.add_circle,
                color: Colors.black,
                size: 35,
              ),
              title: new Text(''),
            ),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.card_giftcard,
                  color: Colors.black,
                  size: 35,
                ),
                title: Text(''))
          ],
        ), */

        //elevation: defaultTargetPlatform == TargetPlatform.android ? 5.0 : 0.0,
        drawer: Drawer(
            child: new ListView(children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: new Text("Mike Callaghan"),
            accountEmail: new Text("mqcallaghan9@gmail.com"),
            currentAccountPicture: new CircleAvatar(
              backgroundColor: Colors.cyan,
              child: new Text("M"),
            ),
            otherAccountsPictures: <Widget>[
              new CircleAvatar(
                backgroundColor: Colors.white,
                child: new Text("K"),
              )
            ],
          ),
          new ListTile(
              title: new Text("Campaign"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Campaign(),
                        ))
                    //Navigator.of(context).pop(),
                    //Navigator.of(context).pushNamed("/Campaign"),
                  }),
          new ListTile(
              title: new Text("Stats"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Stats(),
                        ))
                  }),
          new ListTile(
              title: new Text("Billing"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Billing(),
                        ))
                    //Navigator.pop(context)
                    //Navigator.pop(
                    // context,
                    // MaterialPageRoute(builder: (context) => Billing()),
                    // )
                    // Navigator.of(context).pop(),
                    // Navigator.of(context).pushNamed("/Billing"),
                  }),
          new ListTile(
              title: new Text("Logout"),
              trailing: new Icon(Icons.arrow_upward),
              onTap: () => {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                          builder: (context) => Login(),
                        ))
                  }),
          new Divider(),
          new ListTile(
            title: new Text("close"),
            trailing: new Icon(Icons.close),
            onTap: () => Navigator.of(context).pop(),
          ),
        ])),
        body: Column(
          children: [
            SizedBox(height: 30),
            Container(
              width: 420,
              child: Text('G I F T S',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.staatliches(
                      textStyle: TextStyle(
                    color: Colors.black,
                    fontSize: 30,
                    fontWeight: FontWeight.w500,
                  ))),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(color: Colors.grey[700], width: 1.0))),
            ),
            SizedBox(height: 10),
            Padding(
              padding:
                  EdgeInsets.only(top: 50, bottom: 50, right: 60, left: 30),
              /* child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 5),
                    gradient: LinearGradient(colors: [
                      Colors.blue[900],
                      Colors.blue[500],
                      Colors.blue[400],
                      Colors.blue[500],
                      Colors.blue[900],
                    ]),
                    //Colors.lightBlue,
                    borderRadius: BorderRadius.circular(20)),

                // border: //Border(
                // Border.all(width: 5, color: Colors.black)),

                // top: BorderSide(color: Colors.black, width: 20.0))),
                //color: Colors.black, width: (20)),

                //borderRadius: BorderRadius.circular(10.0)

                padding: const EdgeInsets.only(
                    left: 20, right: 90, bottom: 40, top: 25),
                // margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                alignment: Alignment.center,
                child: Text("50 Dollars - CASH",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    )),

                //child: Text(
                // 'Description',
              ), */

              ////Align(
              //alignment: Alignment.centerRight,
            ),
            SizedBox(height: 10),
            RaisedButton(
              child: Text("5 0   D o l l a r s",
                  style: GoogleFonts.staatliches(
                  textStyle: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ))),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4),
                side: BorderSide(
                  color: Colors.black,
                  width: 0,
                ),
              ),
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => NewCampaign2()),
                ),
              },
              color: Colors.green,
              padding: EdgeInsets.fromLTRB(115, 70, 115, 70),
              splashColor: Colors.grey,
            ),
            SizedBox(
              height: 237,
            ),
            SizedBox(width: 30),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: Row(
                children: [
                  SizedBox(width: 10),
                  RaisedButton(
                    child: Text("P a y   S t u b s",
                        style: GoogleFonts.staatliches(
                        textStyle: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(2),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PayStubs()),
                      ),
                    },
                    color: Colors.green,
                    padding: EdgeInsets.fromLTRB(24, 20, 24, 20),
                    splashColor: Colors.grey,
                  ),
                  SizedBox(width: 50),
                  RaisedButton(
                    child: Text("W a l l e t",
                        style: GoogleFonts.staatliches(
                        textStyle: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ))),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(2),
                      side: BorderSide(
                        color: Colors.black,
                        width: 0,
                      ),
                    ),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => BillingSetup()),
                      ),
                    },
                    color: Colors.green,
                    padding: EdgeInsets.fromLTRB(38.6, 20, 38.6, 20),
                    splashColor: Colors.grey,
                  ),
                ],
              ),
            ),
            SizedBox(height: 55),
            Container(
              padding: EdgeInsets.only(top: 12),
              decoration: BoxDecoration(
                  /* gradient: LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment.centerRight,
                                  colors: [
                                    Colors.blue[900],
                                    Colors.blue[500],
                                    Colors.blue[400],
                                    Colors.blue[500],
                                    Colors.blue[900],
                                  ],
                                ), */
                  border:
                      Border(top: BorderSide(color: Colors.grey[700], width: 1.0))),
              child: Row(
                children: [
                  //SizedBox(height: 16),
                  SizedBox(width: 10),
                    FlatButton(
                      child: Icon(Icons.home, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ActiveCampaigns()),
                        );
                      },
                    ),
                    SizedBox(width: 16),
                    FlatButton(
                      child:
                          Icon(Icons.add_circle, size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CampaignSearch()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    FlatButton(
                      child: Icon(Icons.card_giftcard,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RedeemGifts()),
                        );
                      },
                    ),
                    SizedBox(
                      width: 12,
                    ),
                    FlatButton(
                      child: Icon(Icons.attach_money,
                          size: 35, color: Colors.grey[700]),
                      color: Colors.transparent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PayStubs()),
                        );
                      },
                    ),
                ],
              ),
            ),
          ],
        ));
  }
}