﻿using System;
using BCrypt.Net;

namespace VINCO.Common
{
    public class Encryption
    {
        public static string GetHash(string password, string salt)
        {
            return BCrypt.Net.BCrypt.HashPassword(password, salt);
        }

        public static string GenerateSalt()
        {
            return BCrypt.Net.BCrypt.GenerateSalt();
        }
    }
}
