﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VINCO.Common
{
    public static class ValidationMessages
    {
        public const string INVALID_EMAIL_FORMAT = "Email Address is invalid";
        public const string INVALID_PASSWORD = "Passwords must be at least 8 characters, with at least one number and one symbol.";
        public const string REGISTRATION_EMAIL_EXISTS = "The email provided is already in use.";
        public const string LOGIN_INVALID_CREDENTIALS = "The email or password provided is invalid";
        public const string CAMPAIGN_START_DATE_AFTER_END_DATE = "The Campaign start date must be before the Campaign end date.";
        public const string INFLUENCER_ALREADY_ADDED_TO_CAMPAIGN = "The Influencer had already been added to the Campaign.";
    }
}
