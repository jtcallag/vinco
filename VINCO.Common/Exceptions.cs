﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VINCO.Common
{
    public class VincoUnauthorizedException : Exception
    {
        public VincoUnauthorizedException(string message)
        {
            UnauthorizedMessage = message;
        }

        public string UnauthorizedMessage { get; set; }
    }

    public class VincoValidationException : Exception
    {
        public VincoValidationException(List<string> validationErrors)
        {
            ValidationErrors = validationErrors;
        }
        public List<string> ValidationErrors { get; set; }
    }

    public class VincoValidationResult
    {
        public VincoValidationResult()
        {
            IsValid = true;
            ValidationErrors = new List<string>();
        }
        public bool IsValid { get; set; }
        public List<string> ValidationErrors { get; set; }
    }
}
