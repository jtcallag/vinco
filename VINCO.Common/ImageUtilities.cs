﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace VINCO.Common
{
    public class ImageUtilities
    {
        public async Task<Stream> GetThumbnail(Stream source)
        {
            source.Position = 0;
            var img = Image.Load(source);
            var result = new MemoryStream();
            
            await Task.Run(async () =>
            {
                img = await Resize(img, ImageWidth.Thumb);
                await ConvertToPng(img, result);
            });

             return result;
        }

        public async Task<Dictionary<ImageWidth, Stream>> GetImageSet(Stream source, ImageType imageType)
        {
            var result = new Dictionary<ImageWidth, Stream>();
            using (source)
            {
                
                source.Position = 0;
                var img = Image.Load(source);
                var thumbStream = new MemoryStream();
                var smStream = new MemoryStream();
                var medStream = new MemoryStream();
                var lrgStream = new MemoryStream();
                var xlrgStream = new MemoryStream();

                await Task.Run(async () =>
                {
                    var includeXLarge = false;
                    var includeLarge = false;
                    var includeMedium = false;
                    var includeSmall = false;
                    var includeThumb = false;

                    switch(imageType)
                    {
                        case ImageType.AdvertiserLogo:
                        case ImageType.Campaign:
                            includeXLarge = true;
                            includeLarge = true;
                            includeMedium = true;
                            includeSmall = true;
                            includeThumb = true;
                            break;
                        case ImageType.Avatar:
                            includeSmall = true;
                            includeThumb = true;
                            break;
                    }
                    if (includeXLarge)
                    {
                        var xlrg = await Resize(img, ImageWidth.XLarge);
                        await ConvertToPng(xlrg, xlrgStream);
                        result.Add(ImageWidth.XLarge, xlrgStream);
                    }
                    if (includeLarge)
                    {
                        var lrg = await Resize(img, ImageWidth.Large);
                        await ConvertToPng(lrg, lrgStream);
                        result.Add(ImageWidth.Large, lrgStream);
                    }
                    if (includeMedium)
                    {
                        var med = await Resize(img, ImageWidth.Medium);
                        await ConvertToPng(med, medStream);
                        result.Add(ImageWidth.Medium, medStream);
                    }
                    if (includeSmall)
                    {
                        var sm = await Resize(img, ImageWidth.Small);
                        await ConvertToPng(sm, smStream);
                        result.Add(ImageWidth.Small, smStream);
                    }
                    if (includeThumb)
                    {
                        var thumb = await Resize(img, ImageWidth.Thumb);
                        await ConvertToPng(thumb, thumbStream);
                        result.Add(ImageWidth.Thumb, thumbStream);
                    }
                });
            }
            return result;

        }


        private async Task<Image> Resize(Image source, ImageWidth width)
        {
            await Task.Run(() =>
            {
                var options = new ResizeOptions
                {
                    Size = new SixLabors.ImageSharp.Size((int)width, 0),
                    Mode = ResizeMode.Max
                    
                };
                
                source.Mutate(x => x.Resize((int)width, 0, KnownResamplers.Lanczos3));
            });

            return source;

        }

        private async Task ConvertToPng(Image source, Stream output)
        {
            await Task.Run(async () =>
            {
                var encoder = new SixLabors.ImageSharp.Formats.Jpeg.JpegEncoder();
                encoder.Quality = 100;
                
                await source.SaveAsJpegAsync(output, encoder);
                //await source.SaveAsPngAsync(output);
            });

        }


    }

    public enum ImageWidth
    {
        Thumb = 48,
        Small = 96,
        Medium = 240,
        Large = 480,
        XLarge = 1024
    }

    public enum ImageType
    {
        Avatar,
        AdvertiserLogo,
        Campaign
    }


}
