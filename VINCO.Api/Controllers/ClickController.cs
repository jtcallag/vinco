﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VINCO.BLL.Interfaces;

namespace VINCO.Api.Controllers
{
    /// <summary>
    /// Provides operations for all Click-Thru-Action related activities.
    /// </summary>
    [Route("api/click")]
    [ApiController]
    public class ClickController : BaseController
    {
        private readonly IClickService _clickService;

        public ClickController(IClickService clickService)
        {
            _clickService = clickService;
        }




    
        /// <summary>
        /// Saves click event for the given CampaignInfluencer and redirects to Advertiser's product url.
        /// </summary>
        /// <param name="campaignInfluencerUniqID"></param>
        [HttpGet("{campaignInfluencerUniqID}")]
        public async Task<IActionResult> Get(string campaignInfluencerUniqID)
        {
            var result = await _clickService.SaveClick(campaignInfluencerUniqID);
            return Redirect(result);
        }

    }
}
