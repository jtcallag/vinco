﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VINCO.Model;
using VINCO.Model.ViewModel;
using VINCO.Common;
using VINCO.BLL.DAL;
using VINCO.BLL.Interfaces;

namespace VINCO.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected void SetCurrentUserContext(IBaseService service)
        {
            var claims = HttpContext.User?.Claims;
            var currentUser = new CurrentUser();
            if (claims != null)
            {
                currentUser.Email = claims.FirstOrDefault(c => c.Type == "Email")?.Value;
                currentUser.UserType = (Model.Enums.UserType)Enum.Parse(typeof(Model.Enums.UserType), claims.FirstOrDefault(c => c.Type == "UserType")?.Value);
                currentUser.UniqID = claims.FirstOrDefault(c => c.Type == "UniqID")?.Value;
                currentUser.ID = Int32.Parse(claims.FirstOrDefault(c => c.Type == "ID")?.Value ?? "0");
            }

            service.SetCurrentUser(currentUser);
        }

        protected RequestResult SetRequestResult(object model)
        {
            return new RequestResult()
            {
                IsSuccessful = true,
                Payload = model
            };
        }

        protected RequestResult ParseException(Exception e)
        {
            if (e is VincoValidationException)
            {
                return new RequestResult()
                {
                    IsSuccessful = false,
                    HasValidationErrors = true,
                    ValidationErrors = ((VincoValidationException)e).ValidationErrors
                };
            }
            else if (e is VincoUnauthorizedException)
            {
                return new RequestResult()
                {
                    IsSuccessful = false,
                    HasValidationErrors = false,
                    Message = ((VincoUnauthorizedException)e).UnauthorizedMessage
                };
            }
            else
            {
                return new RequestResult()
                {
                    IsSuccessful = false,
                    HasValidationErrors = false,
                    Message = e.Message
                };
            }
        }
    }
}
