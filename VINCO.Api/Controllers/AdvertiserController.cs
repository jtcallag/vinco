﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VINCO.BLL.Interfaces;
using VINCO.Common;
using VINCO.Model.ViewModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VINCO.Api.Controllers
{
    /// <summary>
    /// Provides operations for all Advertiser related activities.
    /// </summary>
    [Authorize]
    [Route("api/advertiser")]
    [ApiController]
    public class AdvertiserController : BaseController
    {
        private readonly IAdvertiserService _advertiserService;
        private readonly IMediaService _mediaService;

        public AdvertiserController(IAdvertiserService advertiserService, IMediaService mediaService)
        {
            _advertiserService = advertiserService;
            _mediaService = mediaService;
        }

        /// <summary>
        /// Registers a new Advertiser and creates a user account for the advertiser
        /// </summary>
        /// <param name="model">RegistrationViewModel.  defines both the advertiser and the person registering the advertiser</param>
        /// <returns>AdvertiserViewModel.</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public async Task<RequestResult> Register(RegistrationViewModel model)
        {
            try
            {
                var result = await _advertiserService.Register(model);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }

        /// <summary>
        /// Authenticates the Advertiser user account.  Sets a JWT Bearer Token in the response.
        /// </summary>
        /// <param name="model">LoginViewModel.  Sets the credentials to authenticate against.</param>
        /// <returns>bool indicating whether login was successful.</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<RequestResult> Login(LoginViewModel model)
        {
            try
            {
                var result = await _advertiserService.Login(model);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }




        // GET api/<AdvertiserController>/5
        /// <summary>
        /// Gets Advertiser info for the given Advertiser unique identifier.
        /// </summary>
        /// <param name="uniqID">string.  Unique Identifier for the advertiser.</param>
        /// <returns>AdvertiserViewModel</returns>
        [HttpGet("{uniqID}")]
        public async Task<RequestResult> Get(string uniqID)
        {
            SetCurrentUserContext(_advertiserService);

            try
            {
                var result = await _advertiserService.Get(uniqID);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }

        // GET api/<AdvertiserController>/5
        /// <summary>
        /// Gets Advertisers for the given search text.
        /// </summary>
        /// <param name="searchText">string.  Text used to wildcard search Advertisers by Advertiser name or alias.</param>
        /// <returns>AdvertiserViewModel</returns>
        [HttpGet("search/{searchText}")]
        public async Task<RequestResult> Search(string searchText)
        {
            SetCurrentUserContext(_advertiserService);
            try
            {
                var result = await _advertiserService.Search(searchText);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }

        // PUT api/<AdvertiserController>
        /// <summary>
        /// Updates an existing Advertiser
        /// </summary>
        /// <param name="advertiser"></param>
        [HttpPut]
        public async Task<RequestResult> Update([FromBody] AdvertiserViewModel advertiser)
        {
            SetCurrentUserContext(_advertiserService);

            try
            {
                var result = await _advertiserService.Update(advertiser);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }

        

        /// <summary>
        /// Gets a list of Campaigns for the given Advertiser unique identifier.
        /// </summary>
        /// <param name="uniqID">string.  Unique Identifier for the advertiser.</param>
        /// <returns>AdvertiserViewModel</returns>
        [HttpGet("{uniqID}/campaigns")]
        public async Task<RequestResult> GetCampaigns(string uniqID)
        {
            SetCurrentUserContext(_advertiserService);

            try
            {
                var result = await _advertiserService.GetCampaigns(uniqID);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }


        /// <summary>
        /// Uploads a logo file for the given Advertiser unique identifier.
        /// </summary>
        /// <param name="UniqID">string.  Unique Identifier for the advertiser.</param>
        /// <param name="logoFile">byte array.  The logo file. </param>
        /// <returns>bool</returns>
        [HttpPost]
        [Route("{UniqID}/logo")]
        public async Task<RequestResult> LogoUpload(string UniqID, IFormFile logoFile)
        {
            SetCurrentUserContext(_advertiserService);
            var result = true;

            try
            {
                var fileLogoStream = new MemoryStream();

                using (fileLogoStream)
                {
                    await logoFile.CopyToAsync(fileLogoStream);


                    result = await _mediaService.SaveLogo(UniqID, fileLogoStream);
                }
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }



    }
}
