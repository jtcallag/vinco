﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using VINCO.BLL.Interfaces;
using VINCO.Model.ViewModel;
using VINCO.Common;


namespace VINCO.Api.Controllers
{
    [Authorize]
    [Route("api/admin")]
    [ApiController]
    public class AdminController : BaseController
    {
        private readonly IAdminService _adminService;

        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
            //SetCurrentUserContext(_userService);
        }

        /// <summary>
        /// Registers a new Admin.
        /// </summary>
        /// <param name="model">RegistrationViewModel.  Ignore Advertiser and Influencer properties in the model.</param>
        /// <returns>AdminUserViewModel.</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public async Task<RequestResult> Register(RegistrationViewModel model)
        {
            try
            {
                var result = await _adminService.Register(model);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }

        /// <summary>
        /// Authenticates the Admin user account.  Sets a JWT Bearer Token in the response.
        /// </summary>
        /// <param name="model">LoginViewModel.  Sets the credentials to authenticate against.</param>
        /// <returns>bool indicating whether login was successful.</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<RequestResult> Login(LoginViewModel model)
        {
            try
            {
                var result = await _adminService.Login(model);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }


        /// <summary>
        /// Gets a list of application settings.
        /// </summary>
        /// <returns>List of ApplicationSettingsViewModel items.</returns>

        [HttpGet("settings")]
        public async Task<RequestResult> Get()
        {
            SetCurrentUserContext(_adminService);
       
            try
            {
                var result = await _adminService.GetApplicationSettings();
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }

        

        /// <summary>
        /// Updates an ApplicationSetting.
        /// </summary>
        /// <param name="model"></param>
        [HttpPut("setting")]
        public async Task<RequestResult> UpdateApplicationSetting(ApplicationSettingViewModel model)
        {
            SetCurrentUserContext(_adminService);
            try
            {
                var result = await _adminService.UpdateApplicationSetting(model);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }


    }
}
