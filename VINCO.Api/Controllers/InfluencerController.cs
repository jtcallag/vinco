﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VINCO.BLL.Interfaces;
using VINCO.Model.ViewModel;
using VINCO.Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace VINCO.Api.Controllers
{
    /// <summary>
    /// Provides operations for all Influencer related activities.
    /// </summary>
    [Authorize]
    [Route("api/influencer")]
    [ApiController]
    public class InfluencerController : BaseController
    {
        private readonly IInfluencerService _influencerService;
        private readonly IMediaService _mediaService;

        public InfluencerController(IInfluencerService influencerService, IMediaService mediaService)
        {
            _influencerService = influencerService;
            _mediaService = mediaService;
        
        }

        /// <summary>
        /// Registers a new Influencer.
        /// </summary>
        /// <param name="model">RegistrationViewModel.  Ignore Advertiser properties in the model.</param>
        /// <returns>InfluencerViewModel.</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("register")]   
        public async Task<RequestResult> Register(RegistrationViewModel model)
        {
            try
            {
                var result = await _influencerService.Register(model);
                return SetRequestResult(result);
            }
            catch(VincoValidationException v)
            {
                return ParseException(v);
            }
            catch(VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch(Exception e)
            {
                return ParseException(e);
            }

        }

        /// <summary>
        /// Authenticates the Influencer user account.  Sets a JWT Bearer Token in the response.
        /// </summary>
        /// <param name="model">LoginViewModel.  Sets the credentials to authenticate against.</param>
        /// <returns>bool indicating whether login was successful.</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<RequestResult> Login(LoginViewModel model)
        {
            try
            {
                var result = await _influencerService.Login(model);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }


        /// <summary>
        /// Gets Influencers for the given search text.
        /// </summary>
        /// <param name="searchText">string.  Text used to wildcard search Influencers by Influencer name, email, and/or mobile phone.</param>
        /// <returns>InfluencerViewModel</returns>
        [HttpGet("search/{searchText}")]
        public async Task<RequestResult> Search(string searchText)
        {
            SetCurrentUserContext(_influencerService);
            try
            {
                var result = await _influencerService.Search(searchText);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }

        /// <summary>
        /// Gets Influencer info for the given Influencer unique identifier.
        /// </summary>
        /// <param name="uniqID">string.  Unique Identifier for the Influencer.</param>
        /// <returns>InfluencerViewModel</returns>
        [HttpGet("{uniqID}")]   
        public async Task<RequestResult> Get(string uniqID)
        {
            try
            {
                var result = await _influencerService.Get(uniqID);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }

        /// <summary>
        /// Updates an existing Influencer
        /// </summary>
        /// <param name="influencer"></param>
        [HttpPut]
        public async Task<RequestResult> Update([FromBody] InfluencerViewModel influencer)
        {
            SetCurrentUserContext(_influencerService);

            try
            {
                var result = await _influencerService.Update(influencer);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }

        /// <summary>
        /// Uploads a logo file for the given Advertiser unique identifier.
        /// </summary>
        /// <param name="UniqID">string.  Unique Identifier for the advertiser.</param>
        /// <param name="avatarFile">byte array.  The avatar file. </param>
        /// <returns>bool</returns>
        [HttpPost]
        [Route("{UniqID}/avatar")]
        public async Task<RequestResult> AvatarUpload(string UniqID, IFormFile avatarFile)
        {
            SetCurrentUserContext(_influencerService);
            var result = true;

            try
            {
                var fileLogoStream = new MemoryStream();

                using (fileLogoStream)
                {
                    await avatarFile.CopyToAsync(fileLogoStream);


                    result = await _mediaService.SaveAvatar(UniqID, fileLogoStream);
                }
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }


        /// <summary>
        /// Enrolls an Influencer in a Campaign.
        /// </summary>
        /// <param name="uniqID">string. The Influencer's unique identifier</param>
        /// <param name="campaignUniqID">The Campaign's unique identifier</param>
        [HttpPost]
        [Route("{uniqID}/campaign/{campaignUniqID}")]   
        public async Task<RequestResult> AddCampaign(string uniqID, string campaignUniqID)
        {
            SetCurrentUserContext(_influencerService);

            try
            {
                var result = await _influencerService.AddCampaign(campaignUniqID, uniqID);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            { 
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }

        /// <summary>
        /// Updates an Influencer on a Campaign.
        /// </summary>
        /// <param name="uniqID">string. The Influencer's unique identifier</param>
        /// <param name="campaignUniqID">The Campaign's unique identifier</param>
        /// <param name="model">The Campaign Influencer view model</param>
        [HttpPut]
        [Route("{uniqID}/campaign/{campaignUniqID}")]
        public async Task<RequestResult> UpdateCampaign(string uniqID, string campaignUniqID, [FromBody] CampaignInfluencerViewModel model)
        {
            SetCurrentUserContext(_influencerService);

            try
            {
                var result = await _influencerService.UpdateCampaign(model);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }

        /// <summary>
        /// Gets a list of Campaigns for the given Influencer unique identifier.
        /// </summary>
        /// <param name="uniqID">string.  Unique Identifier for the influencer.</param>
        /// <returns>List of CampaignInfluencerViewModel</returns>
        [HttpGet("{uniqID}/campaigns")]
        public async Task<RequestResult> GetCampaigns(string uniqID)
        {
            try
            {
                var result = await _influencerService.GetCampaigns(uniqID);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }




        /// <summary>
        /// Gets the campaign for the given Influencer and campaign unique identifiers.
        /// </summary>
        /// <param name="uniqID">string.  Unique Identifier for the influencer.</param>
        /// <param name="campaignUniqID">string.  Unique Identifier for the campaign.</param>
        /// <returns>CampaignInfluencerViewModel</returns>
        [HttpGet("{uniqID}/campaign/{campaignUniqID}")]
        public async Task<RequestResult> GetCampaign(string uniqID, string campaignUniqID)
        {
            try
            {
                var result = await _influencerService.GetCampaign(campaignUniqID, uniqID);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }
    }
}
