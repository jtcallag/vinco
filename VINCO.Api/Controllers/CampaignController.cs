﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using VINCO.BLL.Interfaces;
using VINCO.Model.ViewModel;
using VINCO.Common;

namespace VINCO.Api.Controllers
{
    /// <summary>
    /// Provides operations for all autonomous Campaign related activities.
    /// </summary>
    [Authorize]
    [Route("api/campaign")]
    [ApiController]
    public class CampaignController : BaseController
    {
        private readonly ICampaignService _campaignService;
        private readonly IMediaService _mediaService;


        public CampaignController(ICampaignService campaignService, IMediaService mediaService)
        {
            _campaignService = campaignService;
            _mediaService = mediaService;
        }

        /// <summary>
        /// Search future or currently active Campaigns by keyword.  
        /// Advertiser name, product name, and product description will be wildcard searched.
        /// Includes Pagination.
        /// </summary>
        /// <param name="searchCriterion"></param>
        /// <returns>List of CampaignViewModel</returns>
        [HttpPost("search")]
        public async Task<RequestResult> SearchCampaigns([FromBody] CampaignSearchViewModel searchCriterion)
        {
            SetCurrentUserContext(_campaignService);
 
            try
            {
                var result = await _campaignService.Search(searchCriterion);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }


        /// <summary>
        /// Adds a Campaign.  
        /// </summary>
        /// <param name="model">Adds a campaign.</param>
        /// <returns>CampaignViewModel</returns>
        [HttpPost]
        public async Task<RequestResult> Add(CampaignViewModel model)
        {
            SetCurrentUserContext(_campaignService);

            try
            {
                var result = await _campaignService.Add(model);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }


        /// <summary>
        /// Updates a Campaign.  
        /// </summary>
        /// <param name="model">Adds a campaign.</param>
        /// <returns>CampaignViewModel</returns>
        [HttpPut]
        public async Task<RequestResult> Update(CampaignViewModel model)
        {
            SetCurrentUserContext(_campaignService);

            try
            {
                var result = await _campaignService.Update(model);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }


        /// <summary>
        /// Get Campaigns by uniqID.  
        /// </summary>
        /// <param name="uniqID"></param>
        /// <returns>CampaignViewModel</returns>
        [HttpGet("{uniqID}")]
        public async Task<RequestResult> Get(string uniqID)
        {
            SetCurrentUserContext(_campaignService);

            try
            {
                var result = await _campaignService.Get(uniqID);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }


        /// <summary>
        /// Get Campaign Influencers by campaignUniqID.  
        /// </summary>
        /// <param name="campaignUniqID"></param>
        /// <returns>CampaignInfluencerLiteViewModel</returns>
        [HttpGet("{campaignUniqID}/influencers")]
        public async Task<RequestResult> GetInfluencers(string campaignUniqID)
        {
            SetCurrentUserContext(_campaignService);
 
            try
            {
                var result = await _campaignService.GetInfluencers(campaignUniqID);
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }
        }

        /// <summary>
        /// Uploads an image file for the given Campaign unique identifier.
        /// </summary>
        /// <param name="UniqID">string.  Unique Identifier for the campaign.</param>
        /// <param name="ImageNum">string.  The number of the Image for the campaign.</param>
        /// <param name="imageFile">byte array.  The image file. </param>
        /// <returns>bool</returns>
        [HttpPost]
        [Route("{UniqID}/image/{ImageNum}")]
        public async Task<RequestResult> ImageUpload(string UniqID, string ImageNum, IFormFile imageFile)
        {
            SetCurrentUserContext(_campaignService);
            var result = true;

            try
            {
                var fileImageStream = new MemoryStream();

                using (fileImageStream)
                {
                    await imageFile.CopyToAsync(fileImageStream);

                    result = await _mediaService.SaveImage(UniqID, ImageNum, fileImageStream, ImageType.Campaign);
                }
                return SetRequestResult(result);
            }
            catch (VincoValidationException v)
            {
                return ParseException(v);
            }
            catch (VincoUnauthorizedException u)
            {
                return ParseException(u);
            }
            catch (Exception e)
            {
                return ParseException(e);
            }

        }



    }
}
