using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MySql.Data.EntityFrameworkCore.Infrastructure;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using VINCO.BLL;
using VINCO.BLL.Interfaces;
using VINCO.BLL.Storage;
using VINCO.Common;
using Microsoft.IdentityModel.Tokens;

namespace VINCO.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<VINCO.BLL.DAL.VincoDbContext>(
                    options => options.UseMySQL(Configuration.GetConnectionString("VINCO_DEV")
            ));

            services.AddScoped<IAdvertiserService, AdvertiserService>();
            services.AddScoped<IInfluencerService, InfluencerService>();
            services.AddScoped<ICampaignService, CampaignService>();
            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<IClickService, ClickService>();
            services.AddScoped<IMediaService, MediaService>();
            services.AddScoped<IStorageService, S3StorageService>();

            //services.AddCors();
            services.AddCors(options =>
            {
                options.AddPolicy("VincoCORS",
                                  builder =>
                                  {
                                      builder.WithOrigins("http://localhost:8080",
                                                          "http://api.vinco.com",
                                                          "https://api.vinco.com",
                                                          "http://admin.vinco.com",
                                                          "https://admin.vinco.com",
                                                          "http://advertiser.vinco.com",
                                                          "https://advertiser.vinco.com",
                                                          "http://influencer.vinco.com",
                                                          "https://influencer.vinco.com",
                                                          "http://influencerapp.vinco.com",
                                                          "https://influencerapp.vinco.com"
                                                          ).AllowAnyHeader()
                                                           .AllowAnyMethod()
                                                           .AllowCredentials();
                                  });
            });

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "VINCO API",
                    Description = "The VINCO Affiliate Marketing API.",
                    TermsOfService = null,
                    Contact = new OpenApiContact
                    { Name = "Joe Callaghan", Email = "dad@vinco.com", Url = null }
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Scheme = "bearer",
                    Description = "Please insert JWT token into field"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });
            });
                var key = Encoding.ASCII.GetBytes(Globals.TokenSecret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseHttpsRedirection();

            app.UseCors("VincoCORS");
            //app.UseCors(x => x
            //    .WithOrigins("http://admin.vinco.com")
            //    .AllowAnyMethod()
            //    .AllowAnyHeader()
            //    //.SetIsOriginAllowed(origin => true) // allow any origin
            //    .AllowCredentials()); // allow credentials
            app.UseSwagger();



            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "VINCO API V1");
            });

            

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
