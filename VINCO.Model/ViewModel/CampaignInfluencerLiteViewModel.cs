﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VINCO.Model.ViewModel
{
    public class CampaignInfluencerLiteViewModel
    {
        public string UniqID { get; set; }
        public string CampaignInfluencerClickUrl { get; set; }
        public bool IsEnabled { get; set; }
        public int ClickCount { get; set; }
        public string InfluencerUniqID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get { return $"{FirstName} {LastName}";  }
        }
        public DateTime DateOfBirth { get; set; }
        public int Age
        {
            get { return (DateTime.Today.Year - DateOfBirth.Year);  }
        }

        
        public void From(CampaignInfluencer campaignInfluencer)
        {
            this.IsEnabled = campaignInfluencer.IsEnabled;
            this.FirstName = campaignInfluencer.Influencer.User.FirstName;
            this.LastName = campaignInfluencer.Influencer.User.LastName;
            this.DateOfBirth = campaignInfluencer.Influencer.DateOfBirth;
            this.InfluencerUniqID = campaignInfluencer.Influencer.UniqID;
            this.ClickCount = 0;
            this.CampaignInfluencerClickUrl = campaignInfluencer.CampaignInfluencerClickUrl;
            this.UniqID = campaignInfluencer.UniqID;
        
        }


    }
}
