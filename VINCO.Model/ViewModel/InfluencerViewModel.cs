﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VINCO.Model;

namespace VINCO.Model.ViewModel
{
    public class InfluencerViewModel : BaseViewModel
    {

        public string UniqID { get; set; }
        public string NameAlias { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get { return $"{FirstName} {LastName}"; }
        }

        public int Age { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public bool IsRegistrationComplete { get; set; }
        public bool IsActive { get; set; }
        public DateTime? LastAttemptedLogin { get; set; }
        public DateTime? LastSuccessfulLogin { get; set; }

        [NotMapped]
        public string AuthenticationToken { get; set; }

        public string CreatedByName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedByName { get; set; }
        public DateTime ModifiedDate { get; set; }


        public Influencer To()
        {
            return new Influencer()
            {
                UniqID = this.UniqID,
                NameAlias = this.NameAlias,
                DateOfBirth = this.DateOfBirth,
                
            };
        }

        public void From(Influencer influencer)
        {
            this.UniqID = influencer.UniqID;
            this.NameAlias = influencer.NameAlias;
            this.DateOfBirth = influencer.DateOfBirth;
            this.FirstName = influencer.User.FirstName;
            this.LastName = influencer.User.LastName;
            this.Email = influencer.User.Email;
            this.MobilePhone = influencer.User.MobilePhone;
            this.LastAttemptedLogin = influencer.User.LastAttemptedLogin;
            this.LastSuccessfulLogin = influencer.User.LastSuccessfulLogin;
            this.IsRegistrationComplete = influencer.User.IsRegistrationComplete;
            this.IsActive = influencer.User.IsActive;
            var age = DateTime.Today.Year - influencer.DateOfBirth.Year;
            if (influencer.DateOfBirth.Date > DateTime.Today.AddYears(-age))
            {
                age--;
            }
            this.Age = age;
            this.CreatedByName = $"{influencer.CreatedByUser.FirstName} {influencer.CreatedByUser.LastName}";
            this.CreatedDate = influencer.CreatedDate;
            this.ModifiedByName = $"{influencer.ModifiedByUser.FirstName} {influencer.ModifiedByUser.LastName}";
            this.ModifiedDate = influencer.ModifiedDate;

        }


    }
}
