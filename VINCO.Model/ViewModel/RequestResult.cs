﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VINCO.Model.ViewModel
{
    public class RequestResult
    {
        public bool IsSuccessful { get; set; }
        public bool HasValidationErrors { get; set; }
        public string Message { get; set; }
        public object Payload { get; set; }

        public List<string> ValidationErrors { get; set; }
    }
}
