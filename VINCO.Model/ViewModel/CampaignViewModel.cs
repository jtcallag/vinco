﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VINCO.Model.ViewModel
{
    public class CampaignViewModel
    {
        public string UniqID { get; set; }
        public string AdvertiserUniqID { get; set; }
        public string AdvertiserName { get; set; }
        public string CampaignName { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string ProductUrl { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsEnabled { get; set; }

        public string CreatedByName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedByName { get; set; }
        public DateTime ModifiedDate { get; set; }

        //public List<CampaignInfluencer> CampaignInfluencers { get; set; }

        public Campaign To()
        {
            var campaign = new Campaign()
            {   
                 UniqID = this.UniqID,
                 CampaignName = this.CampaignName,
                 ProductName = this.ProductName,
                 ProductDescription = this.ProductDescription,
                 ProductUrl = this.ProductUrl,
                 StartDate = this.StartDate,
                 EndDate = this.EndDate,
                 IsEnabled = this.IsEnabled,
            };

            return campaign;
        }

        public void From(Campaign campaign)
        {
            UniqID = campaign.UniqID;
            CampaignName = campaign.CampaignName;
            ProductName = campaign.ProductName;
            ProductDescription = campaign.ProductDescription;
            ProductUrl = campaign.ProductUrl;
            StartDate = campaign.StartDate;
            EndDate = campaign.EndDate;
            IsEnabled = campaign.IsEnabled;
            AdvertiserUniqID = campaign.Advertiser.UniqID;
            AdvertiserName = campaign.Advertiser.Name;
            CreatedByName = $"{campaign.CreatedByUser.FirstName} {campaign.CreatedByUser.LastName}";
            CreatedDate = campaign.CreatedDate;
            ModifiedByName = $"{campaign.ModifiedByUser.FirstName} {campaign.ModifiedByUser.LastName}";
            ModifiedDate = campaign.ModifiedDate;
        }
    }
}
