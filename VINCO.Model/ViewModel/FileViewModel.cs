﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace VINCO.Model.ViewModel
{
    public class FileViewModel
    {
        public Stream FileStream { get; set; }
        public string MimeType { get; set; }
    }
}
