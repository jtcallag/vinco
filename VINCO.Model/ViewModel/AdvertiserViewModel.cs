﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VINCO.Model.ViewModel
{
    public class AdvertiserViewModel : BaseViewModel
    {

        public string UniqID { get; set; }
        public string Name { get; set; }
        public string NameAlias { get; set; }
        public string Description { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public bool IsEnabled { get; set; }
        public string CreatedByName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedByName { get; set; }
        public DateTime ModifiedDate { get; set; }

        public string LogoFileName { get; set; }

        [NotMapped]
        public string AuthenticationToken { get; set; }

        //public List<CampaignViewModel> Campaigns { get; set; }


        public Advertiser To()
        {
            var advertiser = new Advertiser()
            {
                UniqID = this.UniqID,
                Name = this.Name,
                NameAlias = this.NameAlias,
                Description = this.Description,
                StreetAddress1 = this.StreetAddress1,
                StreetAddress2 = this.StreetAddress2,
                City = this.City,
                State = this.State,
                PostalCode = this.PostalCode,
                IsEnabled = this.IsEnabled
            };
            
            return advertiser;
        }

        public void From(Advertiser advertiser)
        {
            this.UniqID = advertiser.UniqID;
            this.Name = advertiser.Name;
            this.NameAlias = advertiser.NameAlias;
            this.Description = advertiser.Description;
            this.StreetAddress1 = advertiser.StreetAddress1;
            this.StreetAddress2 = advertiser.StreetAddress2;
            this.City = advertiser.City;
            this.State = advertiser.State;
            this.PostalCode = advertiser.PostalCode;
            this.IsEnabled = advertiser.IsEnabled;
            this.CreatedByName = $"{advertiser.CreatedByUser.FirstName} {advertiser.CreatedByUser.LastName}";
            this.CreatedDate = advertiser.CreatedDate;
            this.ModifiedByName = $"{advertiser.ModifiedByUser.FirstName} {advertiser.ModifiedByUser.LastName}";
            this.ModifiedDate = advertiser.ModifiedDate;
        }

        public void LiteFrom(Advertiser advertiser)
        {
            this.UniqID = advertiser.UniqID;
            this.Name = advertiser.Name;
            this.NameAlias = advertiser.NameAlias;
            this.Description = advertiser.Description;
            this.StreetAddress1 = advertiser.StreetAddress1;
            this.StreetAddress2 = advertiser.StreetAddress2;
            this.City = advertiser.City;
            this.State = advertiser.State;
            this.PostalCode = advertiser.PostalCode;
            this.IsEnabled = advertiser.IsEnabled;
        }

    }
}
