﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VINCO.Model.ViewModel
{
    public class CampaignSearchViewModel
    {
        public string Keyword { get; set; }
        public string Category { get; set; }

        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
