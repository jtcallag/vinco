﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace VINCO.Model.ViewModel
{
    public class AdminUserViewModel
    {
        public string UniqID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public string MobilePhone { get; set; }
        //[JsonIgnore]
        //public string Password { get; set; }
        //[JsonIgnore]
        //public string PasswordSalt { get; set; }
        public bool IsRegistrationComplete { get; set; }
        public bool IsActive { get; set; }

        public DateTime? LastAttemptedLogin { get; set; }
        public DateTime? LastSuccessfulLogin { get; set; }
        public int UserTypeID { get; set; }
        
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

        [NotMapped]
        public string AuthenticationToken { get; set; }

        public User To()
        {
            return new User()
            {
                UniqID = this.UniqID,
                FirstName = this.FirstName,
                LastName = this.LastName,
                Email = this.Email,

                MobilePhone = this.MobilePhone,
                //Password = this.Password,
                //PasswordSalt = this.PasswordSalt,
                IsRegistrationComplete = this.IsRegistrationComplete,
                IsActive = this.IsActive,

                LastAttemptedLogin = this.LastAttemptedLogin,
                LastSuccessfulLogin = this.LastSuccessfulLogin,
                UserTypeID = this.UserTypeID,

                CreatedBy = this.CreatedBy,
                CreatedDate = this.CreatedDate,
                ModifiedBy = this.ModifiedBy,
                ModifiedDate = this.ModifiedDate

            };
        }

        public void From(User user)
        {
            this.UniqID = user.UniqID;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Email = user.Email;

            this.MobilePhone = user.MobilePhone;
            //this.Password = user.Password;
            //this.PasswordSalt = user.PasswordSalt;
            this.IsRegistrationComplete = user.IsRegistrationComplete;
            this.IsActive = user.IsActive;

            this.LastAttemptedLogin = user.LastAttemptedLogin;
            this.LastSuccessfulLogin = user.LastSuccessfulLogin;
            this.UserTypeID = user.UserTypeID;

            this.CreatedBy = user.CreatedBy;
            this.CreatedDate = user.CreatedDate;
            this.ModifiedBy = user.ModifiedBy;
            this.ModifiedDate = user.ModifiedDate;

        }
    }
}
