﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VINCO.Model.ViewModel
{
    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
