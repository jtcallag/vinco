﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VINCO.Model.ViewModel
{
    public class CampaignInfluencerViewModel : BaseViewModel
    {
        public string UniqID { get; set; }
        public string CampaignInfluencerClickUrl { get; set; }
        public bool IsEnabled { get; set; }


        //campaign
        public string CampaignUniqID { get; set; }
        public string AdvertiserUniqID { get; set; }
        public string AdvertiserName { get; set; }
        public string CampaignName { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string ProductUrl { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool CampaignIsEnabled { get; set; }

        //influencer
        public string InfluencerUniqID { get; set; }
        public string InfluencerNameAlias { get; set; }
        public DateTime InfluencerDateOfBirth { get; set; }

        
        public CampaignInfluencer To()
        {
            return new CampaignInfluencer()
            {
                UniqID = this.UniqID,
                CampaignInfluencerClickUrl = this.CampaignInfluencerClickUrl,
                IsEnabled = this.IsEnabled
            };
        }

        public void From(CampaignInfluencer campaignInfluencer)
        {
            this.UniqID = campaignInfluencer.UniqID;
            this.CampaignInfluencerClickUrl = campaignInfluencer.CampaignInfluencerClickUrl;
            this.IsEnabled = campaignInfluencer.IsEnabled;
            this.AdvertiserUniqID = campaignInfluencer.Campaign.Advertiser.UniqID;
            this.AdvertiserName = campaignInfluencer.Campaign.Advertiser.Name;
            this.CampaignUniqID = campaignInfluencer.Campaign.UniqID;
            this.CampaignName = campaignInfluencer.Campaign.CampaignName;
            this.ProductName = campaignInfluencer.Campaign.ProductName;
            this.ProductDescription = campaignInfluencer.Campaign.ProductName;
            this.ProductUrl = campaignInfluencer.Campaign.ProductUrl;
            this.StartDate = campaignInfluencer.Campaign.StartDate;
            this.EndDate = campaignInfluencer.Campaign.EndDate;
            this.CampaignIsEnabled = campaignInfluencer.Campaign.IsEnabled;
            this.InfluencerUniqID = campaignInfluencer.Influencer.UniqID;
            this.InfluencerNameAlias = campaignInfluencer.Influencer.NameAlias;
            this.InfluencerDateOfBirth = campaignInfluencer.Influencer.DateOfBirth;


    }
}
}
