﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VINCO.Model.ViewModel
{
    public class RegistrationViewModel : BaseViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public int UserTypeID { get; set; }
        public string NameAlias { get; set; }

        #region Advertiser specific info

        public string CompanyName { get; set; }
        public string Description { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        #endregion

        #region Influencer specific into

        public DateTime DateOfBirth { get; set; }


        #endregion


        public Advertiser ToAdvertiser()
        {
            return new Advertiser()
            {
                Name = this.CompanyName,
                NameAlias = this.NameAlias,
                Description = this.Description,
                StreetAddress1 = this.StreetAddress1,
                StreetAddress2 = this.StreetAddress2,
                City = this.City,
                State = this.State,
                PostalCode = this.PostalCode
            };
        }

        public Influencer ToInfluencer()
        {
            return new Influencer()
            {
                DateOfBirth = this.DateOfBirth,
                NameAlias = this.NameAlias
             };
        }

        public User ToUser()
        {
            return new User()
            {
                Email = this.Email,
                FirstName = this.FirstName,
                LastName = this.LastName,
                MobilePhone = this.Phone,
                UserTypeID = this.UserTypeID
            };
        }
    }
}
