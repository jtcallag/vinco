﻿using System;
using System.Collections.Generic;
using System.Text;
using VINCO.Model;

namespace VINCO.Model.ViewModel
{
    public class ApplicationSettingViewModel
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public string CreatedByName { get; set; }
        public DateTime CreatedDate { get; set; }

        public string ModifiedByName { get; set; }
        public DateTime ModifiedDate { get; set; }

        public void From(ApplicationSetting setting)
        {
            this.Name = setting.Name;
            this.Value = setting.Value;
            this.CreatedByName = $"{setting.CreatedByUser.FirstName} {setting.CreatedByUser.LastName}";
            this.CreatedDate = setting.CreatedDate;
            this.ModifiedByName = $"{setting.ModifiedByUser.FirstName} {setting.ModifiedByUser.LastName}";
            this.ModifiedDate = setting.ModifiedDate;
        }

        public ApplicationSetting To()
        {
            var setting = new ApplicationSetting();
            setting.Name = this.Name;
            setting.Value = this.Value;
            return setting;
        }
    }
}
