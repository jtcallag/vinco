﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VINCO.Model
{
    public class UserType : BaseModel
    {
        [Key]
        public int? ID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

 
        [InverseProperty("UserType")]
        public ICollection<User> Users { get; set; }
    }
}
