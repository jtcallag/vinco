﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VINCO.Model.Enums
{
    public enum UserType
    {
        VincoAdmin = 1,
        Advertiser = 2,
        Influencer = 3
    }

    public enum ApplicationSettingKey
    {
        S3ApiKey,
        S3ApiSecret,
        S3BucketName,
        S3Url,
        MediaRootUrl
        
    }

}
