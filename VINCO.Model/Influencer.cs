﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using VINCO.Model.Interfaces;

namespace VINCO.Model
{
    public class Influencer : BaseModel, IVincoModel
    {

        [Key]
        public int? ID { get; set; }

        [Required]
        [MaxLength(255)]
        public string UniqID { get; set; }

        [MaxLength(50)]
        public string NameAlias { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }

        public int CreatedBy { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }
        public DateTime CreatedDate { get; set; }

        public int ModifiedBy { get; set; }
        [ForeignKey("ModifiedBy")]
        public User ModifiedByUser { get; set; }
        public DateTime ModifiedDate { get; set; }


        public ICollection<CampaignInfluencer> CampaignInfluencers { get; set; }



        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public User User { get; set; }

        

    }
}
