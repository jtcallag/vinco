﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VINCO.Model
{
    public class ClickAction : BaseModel
    {
        [Key]
        public int? ID { get; set; }
        [Required]
        [MaxLength(50)]
        public string CampaignUniqID { get; set; }
        [Required]
        [MaxLength(50)]
        public string InfluencerUniqID { get; set; }
        [Required]
        public DateTime ClickDateTime { get; set; }

        [DefaultValue(false)]
        public bool ResultedInPurchase { get; set; }

        [MaxLength(20)]
        public string OrigIPAddress { get; set; }

        [MaxLength(50)]
        public string HttpReferrer { get; set; }
        public int CampaignInfluencerID { get; set; }
        [ForeignKey("CampaignInfluencerID")]
        public CampaignInfluencer CampaignInfluencer { get; set; }
    }
}
