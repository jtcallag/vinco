﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VINCO.Model
{
    public class PaymentTransaction : BaseModel
    {
        public int? ID { get; set; }
        public string UniqID { get; set; }

        public string TransactionCode { get; set; }

        public decimal? TransactionAmount { get; set; }

        [MaxLength(500)]
        public string ResponseMessage { get; set; }

        public int PaymentTransactionStatusID { get; set; }

        [ForeignKey("PaymentTransactionStatusID")]
        public PaymentTransactionStatus Status { get; set; }

        public int PaymentProfileID { get; set; }

        [ForeignKey("PaymentProfileID")]
        public PaymentProfile Profile { get; set; }
    }
}
