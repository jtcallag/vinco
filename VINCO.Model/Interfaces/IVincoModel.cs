﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VINCO.Model.Interfaces
{
    public interface IVincoModel
    {
        int CreatedBy { get; set; }
        DateTime CreatedDate { get; set; }
        int ModifiedBy { get; set; }
        DateTime ModifiedDate { get; set; }

    }
}
