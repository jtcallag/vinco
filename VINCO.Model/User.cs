﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using VINCO.Model.Interfaces;

namespace VINCO.Model
{
    public class User : BaseModel, IVincoModel
    {
        [Key]
        public int? ID { get; set; }
        
        
        [Required]
        [MaxLength(255)]
        public string UniqID { get; set; }


        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        [Required]
        [MaxLength(100)]
        public string Email { get; set; }

        public string MobilePhone { get; set; }

        [Required]
        [MaxLength(255)]
        public string Password { get; set; }

        [Required]
        [MaxLength(255)]
        public string PasswordSalt { get; set; }


        public bool IsRegistrationComplete { get; set; }
        public bool IsActive { get; set; }

        public DateTime? LastAttemptedLogin { get; set; }
        public DateTime? LastSuccessfulLogin { get; set; }


        [ForeignKey("UserType")]
        public int UserTypeID { get; set; }
        public UserType UserType { get; set; }

        public int CreatedBy { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }
        public DateTime CreatedDate { get; set; }

        public int ModifiedBy { get; set; }
        [ForeignKey("ModifiedBy")]
        public User ModifiedByUser { get; set; }
        public DateTime ModifiedDate { get; set; }






    }
}
