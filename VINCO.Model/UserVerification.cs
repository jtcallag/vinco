﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VINCO.Model
{
    public class UserVerification : BaseModel
    {

        public int? ID { get; set; }
        [Required]
        [MaxLength(20)]
        public string Passcode { get; set; }

        [Required]
        public DateTime ExpiryDate { get; set; }

        [Required]
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public User User { get; set; }
    }
}
