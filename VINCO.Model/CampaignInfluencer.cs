﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VINCO.Model
{
    public class CampaignInfluencer : BaseModel
    {
        [Key]
        public int? ID { get; set; }

        public string UniqID { get; set; }

        public int CampaignID { get; set; }
        [ForeignKey("CampaignID")]
        public Campaign Campaign { get; set; }

        public int InfluencerID { get; set; }
        [ForeignKey("InfluencerID")]
        public Influencer Influencer { get; set; }

        public string CampaignInfluencerClickUrl { get; set; }
        public bool IsEnabled { get; set; }
        public ICollection<ClickAction> ClickActions { get; set; }
    }
}
