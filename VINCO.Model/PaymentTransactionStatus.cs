﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VINCO.Model
{
    public class PaymentTransactionStatus : BaseModel
    {
        [Key]
        public int? ID { get; set; }

        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        public bool IsActive { get; set; }

 

    }
}
