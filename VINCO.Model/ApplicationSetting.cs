﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VINCO.Model.Interfaces;

namespace VINCO.Model
{
    public class ApplicationSetting : BaseModel, IVincoModel
    {
        [Key]
        public int? ID { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        public string Value { get; set; }

        public int CreatedBy { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }
        public DateTime CreatedDate { get; set; }

        public int ModifiedBy { get; set; }
        [ForeignKey("ModifiedBy")]
        public User ModifiedByUser { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
