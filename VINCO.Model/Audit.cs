﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VINCO.Model
{
    [NotMapped]
    public class Audit : BaseModel
    {
        public string UniqID { get; set; }

        public string Message { get; set; }
        public string Detail { get; set; }

        public DateTime AuditDate { get; set; }
    }

    
    public enum AuditType
    {
        Campaign,
        Advertisor,
        Influencer,
        Internal
    }
}
