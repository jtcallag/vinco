﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VINCO.Model.Interfaces;

namespace VINCO.Model
{
    public class Advertiser : BaseModel, IVincoModel
    {
        [Key]
        public int? ID { get; set; }

        [Required]
        [MaxLength(50)]
        public string UniqID { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(50)]
        public string NameAlias { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }

        [Required]
        [MaxLength(100)]
        public string StreetAddress1 { get; set; }
        
        [MaxLength(100)]
        public string StreetAddress2 { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string City { get; set; }

        [MaxLength(10)]
        public string State { get; set; }

        [Required]
        [MaxLength(20)]
        public string PostalCode { get; set; }

        public bool IsEnabled { get; set; }

        public int CreatedBy { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }
        public DateTime CreatedDate { get; set; }

        public int ModifiedBy { get; set; }
        [ForeignKey("ModifiedBy")]
        public User ModifiedByUser { get; set; }
        public DateTime ModifiedDate { get; set; }

        public string LogoFileName { get; set; }

        public ICollection<Campaign> Campaigns { get; set; }

        public ICollection<AdvertiserUser> AdvertiserUsers { get; set; }


        

    }
}
