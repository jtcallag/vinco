﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using VINCO.Model.Interfaces;

namespace VINCO.Model
{
    public class Campaign : BaseModel, IVincoModel
    {
        [Key]
        public int? ID { get; set; }
        [Required]
        [MaxLength(50)]
        public string UniqID { get; set; }

        [Required]
        [MaxLength(50)]
        public string CampaignName { get; set; }

        [Required]
        [MaxLength(50)]
        public string ProductName { get; set; }

        [Required]
        [MaxLength(1000)]
        public string ProductDescription { get; set; }

        [Required]
        [MaxLength(500)]
        public string ProductUrl { get; set; }

        //[Required]
        //[MaxLength(500)]
        //public string ProductSmallImageUrl { get; set; }

        //[Required]
        //[MaxLength(500)]
        //public string ProductMediumImageUrl { get; set; }

        //[Required]
        //[MaxLength(500)]
        //public string ProductLargeImageUrl { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public bool IsEnabled { get; set; }

        public int CreatedBy { get; set; }
        [ForeignKey("CreatedBy")]
        public User CreatedByUser { get; set; }
        public DateTime CreatedDate { get; set; }

        public int ModifiedBy { get; set; }
        [ForeignKey("ModifiedBy")]
        public User ModifiedByUser { get; set; }
        public DateTime ModifiedDate { get; set; }

        public int AdvertiserID { get; set; }
        
        [ForeignKey("AdvertiserID")]
        public Advertiser Advertiser { get; set; }

        public ICollection<CampaignInfluencer> CampaignInfluencers { get; set; }

    }
}
