﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace VINCO.Model
{
    public class MediaRequest
    {
        public Stream ImageStream { get; set; }
        public string FileName { get; set; }

        #region S3 (DigitalOcean Spaces)

        public string S3Key { get; set; }
        public string S3Secret { get; set; }
        public string S3Url { get; set; }
        public string S3Bucket { get; set; }

        #endregion

    }
}
