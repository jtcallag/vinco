﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VINCO.Model
{
    public class AdvertiserUser
    {
        public int? ID { get; set; }
        public string UniqID { get; set; }

        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public User User { get; set; }

        public int AdvertiserID { get; set; }
        [ForeignKey("AdvertiserID")]
        public Advertiser Advertiser { get; set; }
     }
}
